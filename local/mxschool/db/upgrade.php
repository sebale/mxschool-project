<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_mxschool_upgrade($oldversion) {
    global $CFG, $DB, $SESSION;

    $dbman = $DB->get_manager();

    
    /* local_mxschool_advisors */
    
	// Define table local_mxschool_advisors to be created.
	$table = new xmldb_table('local_mxschool_advisors');

	// Adding fields to table local_mxschool_advisors.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('studentid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('currentadvisor', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('keep_current_advisor', XMLDB_TYPE_CHAR, '60', null, null, null, 'yes');
    $table->add_field('advisor1', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('advisor2', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('advisor3', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('advisor4', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('advisor5', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('finaladvisor', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('comment', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
	$table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_advisors.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_advisors.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_announcements */
    
    // Define table local_mxschool_announcements to be created.
	$table = new xmldb_table('local_mxschool_announcements');

	// Adding fields to table local_mxschool_announcements.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('description', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('descriptionformat', XMLDB_TYPE_INTEGER, '4', null, XMLDB_NOTNULL, null, '1');
    $table->add_field('announcementdate', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('startdate', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('enddate', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
	$table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
	$table->add_field('state', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    
	// Adding keys to table local_mxschool_announcements.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_announcements.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_dates */
    
    // Define table local_mxschool_dates to be created.
	$table = new xmldb_table('local_mxschool_dates');

	// Adding fields to table local_mxschool_dates.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('date', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('weekendtype', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    
	// Adding keys to table local_mxschool_dates.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_dates.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_dorms */
    
    // Define table local_mxschool_dorms to be created.
	$table = new xmldb_table('local_mxschool_dorms');

	// Adding fields to table local_mxschool_dorms.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('abbreviation', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('gender', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, null, null, 'boarding');
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    
	// Adding keys to table local_mxschool_dorms.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_dorms.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_notifications */
    
    // Define table local_mxschool_notifications to be created.
	$table = new xmldb_table('local_mxschool_notifications');

	// Adding fields to table local_mxschool_notifications.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('description', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('tags', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('subject', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('body', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('sender', XMLDB_TYPE_CHAR, '255', null, null, null, 'supportuser');
    $table->add_field('sendto', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    $table->add_field('role', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('category', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '5', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_notifications.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_notifications.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_parents */
    
    // Define table local_mxschool_parents to be created.
	$table = new xmldb_table('local_mxschool_parents');

	// Adding fields to table local_mxschool_parents.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('childid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('address', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('homephone', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('cellphone', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('workphone', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('email', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_parents.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_parents.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_pickup_sites */
    
    // Define table local_mxschool_pickup_sites to be created.
	$table = new xmldb_table('local_mxschool_pickup_sites');

	// Adding fields to table local_mxschool_pickup_sites.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('site', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('type', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    $table->add_field('sitetype', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    
	// Adding keys to table local_mxschool_pickup_sites.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_pickup_sites.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_pickup_times */
    
    // Define table local_mxschool_pickup_times to be created.
	$table = new xmldb_table('local_mxschool_pickup_times');

	// Adding fields to table local_mxschool_pickup_times.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('datetime', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('type', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    
	// Adding keys to table local_mxschool_pickup_times.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_pickup_times.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_students */
    
    // Define table local_mxschool_students to be created.
	$table = new xmldb_table('local_mxschool_students');

	// Adding fields to table local_mxschool_students.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('middle', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('preferred', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('grade', XMLDB_TYPE_CHAR, '10', null, null, null, null);
    $table->add_field('yearofgraduation', XMLDB_TYPE_CHAR, '5', null, null, null, null);
    $table->add_field('gender', XMLDB_TYPE_CHAR, '10', null, null, null, null);
    $table->add_field('dorm', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('room', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('birthdate', XMLDB_TYPE_CHAR, '20', null, null, null, null);
    $table->add_field('overnight', XMLDB_TYPE_CHAR, '40', null, null, null, null);
    $table->add_field('driving', XMLDB_TYPE_CHAR, '40', null, null, null, null);
    $table->add_field('comment', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('boston', XMLDB_TYPE_CHAR, '40', null, null, null, null);
    $table->add_field('advisor', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('dormphone', XMLDB_TYPE_CHAR, '20', null, null, null, null);
    $table->add_field('studentcell', XMLDB_TYPE_CHAR, '20', null, null, null, null);
    $table->add_field('maydrive', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('maygiverides', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('swimcompetent', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('swimallowed', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('boatallowed', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    
	// Adding keys to table local_mxschool_students.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_students.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_transport */
    
    // Define table local_mxschool_transport to be created.
	$table = new xmldb_table('local_mxschool_transport');

	// Adding fields to table local_mxschool_transport.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('studentid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('destination', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('stu_cellphone', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('depart_date_time', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_transportation', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('depart_transport_type', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_driver', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('depart_airport', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_carrier', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('depart_flight', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('depart_customs', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_train', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_train_other', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('depart_bus', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_bus_other', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('depart_nyc_stops', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_date_time', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_transportation', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('return_transport_type', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_driver', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('return_airport', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_carrier', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('return_flight', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('return_customs', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_train', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_train_other', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('return_bus', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_bus_other', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('return_nyc_stops', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('trans_type_depart', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_site_depart', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_time_depart', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_time_depart_other', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('confirmed_depart', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('trans_type_return', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_site_return', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_time_return', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_time_return_other', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('confirmed_return', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('email_sendable_depart', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('email_sent_depart', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('email_sendable_return', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('email_sent_return', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_transport.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_transport.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_rooming */
    
	// Define table local_mxschool_rooming to be created.
	$table = new xmldb_table('local_mxschool_rooming');

	// Adding fields to table local_mxschool_rooming.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('studentid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('dormaterequest', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('dorm', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('roomtype', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('haslivedindouble', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('preferreddoubleroommate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('status', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timecreate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timeupdate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_rooming.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_rooming.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    /* local_mxschool_driving */
    
	// Define table local_mxschool_driving to be created.
	$table = new xmldb_table('local_mxschool_driving');

	// Adding fields to table local_mxschool_driving.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('student', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('date', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('date_time', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('data', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('timecreate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_driving.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_driving.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    
    /* local_mxschool_tutors */
    
	// Define table local_mxschool_tutors to be created.
	$table = new xmldb_table('local_mxschool_tutors');

	// Adding fields to table local_mxschool_tutors.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('data', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    
	// Adding keys to table local_mxschool_tutors.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_tutors.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    
    /* local_mxschool_tutors_cat */
    
	// Define table local_mxschool_tutors_cat to be created.
	$table = new xmldb_table('local_mxschool_tutors_cat');

	// Adding fields to table local_mxschool_tutors_cat.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('timecreate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_tutors_cat.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_tutors_cat.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    
    /* local_mxschool_tutors_course */
    
	// Define table local_mxschool_tutors_course to be created.
	$table = new xmldb_table('local_mxschool_tutors_course');

	// Adding fields to table local_mxschool_tutors_course.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('category', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timecreate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_tutors_course.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_tutors_course.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_tutors_sess */
    
	// Define table local_mxschool_tutors_sess to be created.
	$table = new xmldb_table('local_mxschool_tutors_sess');

	// Adding fields to table local_mxschool_tutors_sess.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('date', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('time', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('tutor_id', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('student_id', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('subject', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('course', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('topic', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('type_request', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('type_requested_other', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('effectiveness', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('notes', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('timecreate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_tutors_sess.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_tutors_sess.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_weekend */
    
	// Define table local_mxschool_weekend to be created.
	$table = new xmldb_table('local_mxschool_weekend');

	// Adding fields to table local_mxschool_weekend.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('student', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('departure_time', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_time', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('destination', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('phone', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('timecreate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('parent', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('invite', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('ok', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_weekend.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_weekend.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    
    /* local_mxschool_faculty */
    
	// Define table local_mxschool_faculty to be created.
	$table = new xmldb_table('local_mxschool_faculty');

	// Adding fields to table local_mxschool_faculty.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('dorm', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('type', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('available', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('userid', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_faculty.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_faculty.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    
    
    /* local_mxschool_epassenger */
    
	// Define table local_mxschool_epassenger to be created.
	$table = new xmldb_table('local_mxschool_epassenger');

	// Adding fields to table local_mxschool_epassenger.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('passenger', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('driver', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('driver_other', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('departure_time', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('destination', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('return_time', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('granded_from', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('permission', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('call_received', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timecreate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_epassenger.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_epassenger.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    
    
    /* local_mxschool_edriver */
    
	// Define table local_mxschool_edriver to be created.
	$table = new xmldb_table('local_mxschool_edriver');

	// Adding fields to table local_mxschool_edriver.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('driver', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('passenger', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('departure_time', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('destination', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('return_time', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('granded', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('granded_from', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timectreate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_edriver.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_edriver.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    $field = new xmldb_field('timectreate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    if(!$dbman->field_exists($table, $field)){
        $dbman->add_field($table, $field);    
    }

	if($oldversion < 2016072601){
        $table = new xmldb_table('local_mxschool_weekend');
        $field = new xmldb_field('transportation', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $dbman->add_field($table, $field);

        $table = new xmldb_table('local_mxschool_dates');
        $field = new xmldb_field('time', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->add_field($table, $field);

        $table = new xmldb_table('local_mxschool_driving');
        $field = new xmldb_field('make_of_car', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('license_plate', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('color', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('model_of_car', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $dbman->add_field($table, $field);

    }

	if($oldversion < 2016080900){
        $table = new xmldb_table('local_mxschool_epassenger');
        $field = new xmldb_field('granded', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->add_field($table, $field);

        unset_user_preference('flextable_weekday_checkin_table');
        unset($SESSION->flextable);
    }

	if($oldversion < 2016110100){
        global $DB;
        $record = new stdClass();
        $record->id = 9;
        $record->type = 'custom';
        $record->name = 'Driver form results';
        $record->description = 'Sanding then drivers record was created or updated';
        $record->tags = 'a:6:{i:0;s:15:"passenger_names";i:1;s:11:"driver_name";i:2;s:9:"departure";i:3;s:11:"destination";i:4;s:6:"return";i:5;s:14:"permision_from";}';
        $record->subject = 'Middlesex Driver form results for [driver_name]';
        $record->body = "<p><font face=\"Verdana\" size=\"2\" color=\"#5E8CB2\">Middlesex Driver Form Results:</font></p> 
              <table id=\"yui_3_13_0_2_1477984245304_178\" height=\"170\" width=\"340\">
	              <tbody id=\"yui_3_13_0_2_1477984245304_177\"><tr id=\"yui_3_13_0_2_1477984245304_176\">
		              <td id=\"yui_3_13_0_2_1477984245304_191\" bgcolor=\"#CCCCC\"><font id=\"yui_3_13_0_2_1477984245304_190\" face=\"Verdana\" size=\"1\">Passenger Name(s): </font></td>
		              <td id=\"yui_3_13_0_2_1477984245304_175\">[passenger_names]</td>
	              </tr>
	              <tr id=\"yui_3_13_0_2_1477984245304_189\">
		              <td id=\"yui_3_13_0_2_1477984245304_188\" bgcolor=\"#CCCCC\"><font face=\"Verdana\" size=\"1\">Driver Name: </font></td>
                  <td>[driver_name]</td>
	              </tr>
	              <tr>
		              <td bgcolor=\"#CCCCC\"><font face=\"Verdana\" size=\"1\">Departure Time: </font></td>
		              <td>[departure]</td>
	              </tr>
	              <tr> 
		              <td bgcolor=\"#CCCCC\"><font face=\"Verdana\" size=\"1\">Destination: </font></td>
		              <td>[destination]</td>
	              </tr>
	              <tr>
		              <td bgcolor=\"#CCCCC\"><font face=\"Verdana\" size=\"1\">Return Time: </font></td>
		              <td>[return]</td>
	              </tr>
	              <tr>
                  <td bgcolor=\"#CCCCC\"><font face=\"Verdana\" size=\"1\">Face to Face Permission Granted By: </font></td>
                  <td>[permision_from]</td>
                </tr>
				 
          </tbody></table>";
        $record->sender = 'supportuser';
        $record->status = 1;
        $record->category = 'Vacation and Travel';
        $record->sortorder = 9;

        $DB->insert_record('local_mxschool_notifications',$record);
    }

    return true;
}
