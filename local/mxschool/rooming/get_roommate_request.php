<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');

$stuid = required_param('stuid',PARAM_INT);

$dorm = $DB->get_record_sql('SELECT d.name
                                    FROM {local_mxschool_students} as ms                                     
                                      LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=ms.dorm
                                    WHERE ms.id=:student_id
                                ',array('student_id'=>$stuid));

$roommate = $DB->get_records_sql('SELECT st.id, u.lastname, u.firstname, st.middle
                                    FROM {local_mxschool_students} as ms
                                      LEFT JOIN {local_mxschool_students} as st ON st.gender=ms.gender AND st.grade=ms.grade AND st.id!=ms.id
                                      LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=st.dorm
                                      LEFT JOIN {user} u ON u.id=st.userid
                                    WHERE ms.id=:student_id AND d.type="boarding"
                                    ORDER BY u.lastname, u.firstname, st.middle
                                ',array('student_id'=>$stuid));
$options_grade = '';
foreach ($roommate as $item) {
    $options_grade .= '<option value="'.$item->id.'">'.$item->lastname.' '.$item->firstname.' '.$item->middle.'</option>';
}
$roommate = $DB->get_records_sql('SELECT st.id, u.lastname, u.firstname, st.middle
                                    FROM {local_mxschool_students} as ms
                                      LEFT JOIN {local_mxschool_students} as st ON st.gender=ms.gender AND st.grade != ms.grade AND st.id!=ms.id
                                      LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=st.dorm
                                      LEFT JOIN {user} u ON u.id=st.userid
                                    WHERE ms.id=:student_id AND d.type="boarding"
                                    ORDER BY u.lastname, u.firstname, st.middle
                                ',array('student_id'=>$stuid));
$options_all = '';
foreach ($roommate as $item) {
    $options_all .= '<option value="'.$item->id.'">'.$item->lastname.' '.$item->firstname.' '.$item->middle.'</option>';
}
$options_array = array('options_grade' => $options_grade, 'options_all' => $options_all, 'dorm'=> $dorm->name);
$json_options = json_encode($options_array);
die($json_options);

