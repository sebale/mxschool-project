<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class enter_rooming_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE, $DB, $USER;

        $mform          = $this->_form;
        $id             = $this->_customdata['id'];
        if($id>0){
            $instant = $DB->get_record_sql('SELECT ro.studentid, d.name as dorm, ro.roomtype,ro.dormaterequest, ro.haslivedindouble, ro.preferreddoubleroommate
                                                  FROM {local_mxschool_rooming}  ro
                                                    LEFT JOIN {local_mxschool_students}  ms ON ms.id=ro.studentid
                                                    LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=ms.dorm
                                                  WHERE ro.id=:id',
                array('id'=>$id));
            $mform->addElement('hidden', 'id', $id);
            $mform->setType('id', PARAM_INT);
        }

        $current_student = $DB->get_record_sql('SELECT ms.id,d.name as dorm 
                                                  FROM {local_mxschool_students}  ms
                                                    LEFT JOIN {user} u ON u.id=ms.userid
                                                    LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=ms.dorm
                                                  WHERE u.email = :email AND u.firstname = :first AND u.lastname = :last',
                                                array('email'=>$USER->email,'first'=>$USER->firstname,'last'=>$USER->lastname));

        if(isset($current_student->id)){
            $instant = $DB->get_record_sql('SELECT ro.studentid, d.name as dorm, ro.roomtype,ro.dormaterequest, ro.haslivedindouble, ro.preferreddoubleroommate
                                                  FROM {local_mxschool_rooming}  ro
                                                    LEFT JOIN {local_mxschool_students}  ms ON ms.id=ro.studentid
                                                    LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=ms.dorm
                                                  WHERE ro.studentid=:id',
                array('id'=>$current_student->id));
            if(!isset($instant->studentid))
                unset($instant);
        }

        $response_obj = $DB->get_records_sql('SELECT ms.id,ms.middle,u.lastname,u.firstname
                                                  FROM {local_mxschool_students}  ms
                                                    LEFT JOIN {user} u ON u.id=ms.userid');
        $all_students = array(''=>get_string('name'));
        foreach($response_obj as $item){
            $all_students[$item->id] = $item->lastname.' '.$item->firstname.' '.$item->middle;
        }

        $response_obj = $DB->get_records_sql('SELECT ms.id, u.firstname, ms.middle, u.lastname, ms.dorm, d.type 
                                                  FROM {local_mxschool_students} ms
                                                    LEFT JOIN {user} u ON u.id=ms.userid
                                                    LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=ms.dorm
                                                  WHERE d.type = "boarding" AND ms.grade <= 11
                                                  ORDER BY u.lastname, u.firstname, ms.middle ASC');
        $students = array(''=>get_string('name'));
        foreach($response_obj as $item){
            $students[$item->id] = $item->lastname.' '.$item->firstname.' '.$item->middle;
        }

        $response_obj = $DB->get_records_sql('SELECT id, abbreviation, name, gender, type
                                        FROM {local_mxschool_dorms}
                                        WHERE type="boarding"
                                        ORDER BY name ASC');
        $dorms = array(''=>get_string('current_dorm','local_mxschool'));
        foreach($response_obj as $item){
            $dorms[$item->abbreviation] = $item->name;
        }

        $request_room = array(''=>get_string('room_type','local_mxschool'),
                                'single'=>get_string('single','local_mxschool'),
                                'double'=>get_string('double','local_mxschool'),
                                'triple'=>get_string('triple','local_mxschool'),
                                'quad'=>get_string('quad','local_mxschool'),
                            );

        if (isset($current_student->id) && !isset($instant)){
            $mform->addElement('static', 'studentname', get_string('name'), $students[$current_student->id]);
            $mform->addElement('hidden', 'name', $current_student->id);
            $mform->setType('name', PARAM_INT);
        }elseif(isset($instant)){
            $mform->addElement('static', 'studentname', get_string('name'), $students[$instant->studentid]);
            $mform->addElement('hidden', 'name', $instant->studentid);
            $mform->setType('name', PARAM_INT);
        }else{
            $mform->addElement('select', 'name', get_string('name'), $students, array('id'=>'stu_name'));
            $mform->setType('name', PARAM_RAW);
            $mform->addRule('name', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
            $mform->setDefault('name', '');
        }

        if (isset($current_student->id) && !isset($instant)){
            $dorm = $current_student->dorm;
        }elseif(isset($instant)){
            $dorm = $instant->dorm;
        }else{
            $dorm = '-----';
        }
        $mform->addElement('static', 'current_dorm', get_string('current_dorm','local_mxschool'), $dorm);

        $mform->addElement('select', 'room_type', get_string('request_room_type','local_mxschool'), $request_room, array('id'=>'room_t'));
        $mform->setType('room_type', PARAM_RAW);
        $mform->addRule('room_type', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($instant)){
            $mform->setDefault('room_type', $instant->roomtype);
        }

        if(isset($instant)){
            $dormates = explode(',', $instant->dormaterequest);
        }


        $mform->addElement('html', html_writer::start_div('rdormmates'));
            $mform->addElement('select', 'dormate_select1', get_string('dormate_from_grade','local_mxschool'), $all_students, array('id'=>'dormate1','class'=>'first_dormate_requested'));
            $mform->setType('dormate_select1', PARAM_RAW);
            $mform->addRule('dormate_select1', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
            if(isset($instant)){
                $mform->setDefault('dormate_select1', $dormates[0]);
            }

            $mform->addElement('html', html_writer::start_div('hide-label second_dormate_requested',array('style'=>'display:none;')));
                $mform->addElement('select', 'dormate_select2', '', $all_students, array('id'=>'dormate2'));
                $mform->setType('dormate_select2', PARAM_RAW);
                $mform->addRule('dormate_select2', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
                if(isset($instant)){
                    $mform->setDefault('dormate_select2', $dormates[1]);
                }
            $mform->addElement('html', html_writer::end_div());

            $mform->addElement('html', html_writer::start_div('hide-label third_dormate_requested',array('style'=>'display:none;')));
                $mform->addElement('select', 'dormate_select3', '', $all_students, array('id'=>'dormate3'));
                $mform->setType('dormate_select3', PARAM_RAW);
                $mform->addRule('dormate_select3', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
                if(isset($instant)){
                    $mform->setDefault('dormate_select3', $dormates[2]);
                }
            $mform->addElement('html', html_writer::end_div());

            $mform->addElement('html', html_writer::start_div('req_dorms_any',array('style'=>'display:none;')));
                $mform->addElement('select', 'dormate_select4', get_string('dormate_from_any_grade','local_mxschool'), $all_students, array('id'=>'dormate4','class'=>'fourth_dormate_requested'));
                $mform->setType('dormate_select4', PARAM_RAW);
                $mform->addRule('dormate_select4', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
                if(isset($instant)){
                    $mform->setDefault('dormate_select4', $dormates[3]);
                }

                $mform->addElement('html', html_writer::start_div('hide-label fifth_dormate_requested',array('style'=>'display:none;')));
                    $mform->addElement('select', 'dormate_select5', '', $all_students, array('id'=>'dormate5'));
                    $mform->setType('dormate_select5', PARAM_RAW);
                    $mform->addRule('dormate_select5', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
                    if(isset($instant)){
                        $mform->setDefault('dormate_select5', $dormates[4]);
                    }
                $mform->addElement('html', html_writer::end_div());

                $mform->addElement('html', html_writer::start_div('hide-label sixth_dormate_requested',array('style'=>'display:none;')));
                    $mform->addElement('select', 'dormate_select6', '', $all_students, array('id'=>'dormate6'));
                    $mform->setType('dormate_select6', PARAM_RAW);
                    $mform->addRule('dormate_select6', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
                    if(isset($instant)){
                        $mform->setDefault('dormate_select6', $dormates[5]);
                    }
                $mform->addElement('html', html_writer::end_div());
            $mform->addElement('html', html_writer::end_div());

        $mform->addElement('html', html_writer::end_div());

        $mform->addElement('advcheckbox', 'past_one_rm_db', '', get_string('dormate_check', 'local_mxschool'), array('group'=>1), array(0, 1));
        if(isset($instant)){
            $mform->setDefault('past_one_rm_db', $instant->haslivedindouble);
        }
        $mform->addElement('html', html_writer::tag('p',get_string('dormate_info','local_mxschool')));

        $mform->addElement('select', 'preferred_roommate', get_string('preferred_roommate','local_mxschool'), $all_students, array('id'=>'preferred_roommate'));
        $mform->setType('preferred_roommate', PARAM_RAW);
        $mform->addRule('preferred_roommate', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($instant)){
            $mform->setDefault('preferred_roommate', $instant->preferreddoubleroommate);
        }

        $this->add_action_buttons(get_string('cancel'), get_string('save', 'local_mxschool'));
    }
}


