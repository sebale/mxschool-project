<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class roomings_table extends table_sql {
    
    function __construct($uniqueid, $filter_dorm, $search) {
		global $CFG, $USER, $DB;

        parent::__construct($uniqueid);

        $columns = array('user', 'grade', 'gender', 'dorm', 'roomtype', 'dormaterequest', 'preferreddoubleroommate', 'haslivedindouble', 'lastmod', 'actions');
        $header = array('Name', 'Grade', 'Gender', 'Current Dorm ', 'Requested Room Type', 'Requested Dormates', 'Preferred Double Roommate', 'Has Lived in Double', 'Last Edited', 'Actions');
        
        $this->define_columns($columns);
        $this->define_headers($header);

        
        $fields = "ro.id, CONCAT(u.firstname,', ',u.lastname,' ',ms.middle) as user, ms.grade, ms.gender, d.name as dorm, ro.roomtype, ro.dormaterequest, CONCAT(u_pre.firstname,' ',u_pre.lastname,' ',ms_pre.middle) as preferreddoubleroommate, ro.haslivedindouble, ro.timecreate, ro.timeupdate";
        $from = "{local_mxschool_rooming} ro
                    LEFT JOIN {local_mxschool_students} ms ON ms.id=ro.studentid
                    LEFT JOIN {user} u ON u.id = ms.userid
                    LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=ms.dorm
                    LEFT JOIN {local_mxschool_students} ms_pre ON ms_pre.id=ro.preferreddoubleroommate
                    LEFT JOIN {user} u_pre ON u_pre.id = ms_pre.userid
                ";
        $where = 'ro.status=1';
        if(!empty($filter_dorm)){
            $where .= " AND ms.dorm='$filter_dorm'";
        }

        if(!empty($search)){
            //LIKE '%$search%'

            $where .= " AND ( CONCAT(u.firstname,' ',u.lastname,' ',ms.middle) LIKE '%$search%' OR ms.grade LIKE '%$search%' OR ms.gender LIKE '%$search%' OR d.name LIKE '%$search%' OR ro.roomtype LIKE '%$search%' OR CONCAT(u_pre.firstname,' ',u_pre.lastname,' ',ms_pre.middle) LIKE '%$search%')";
        }

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }
    
    function col_lastmod($values) {
      return  ($values->timeupdate>0)?date('m/d/Y h:i A', $values->timeupdate):date('m/d/Y h:i A', $values->timecreate);
    }
    function col_dormaterequest($values) {
        global $DB;
        $users = $DB->get_records_sql("SELECT ms.id, CONCAT(u.firstname, ', ', u.lastname,' ', ms.middle) as name
                                        FROM {local_mxschool_students} ms
                                          LEFT JOIN {user} u ON u.id=ms.userid
                                        WHERE ms.id IN(".$values->dormaterequest.")");
        if ($this->is_downloading()){
            $html = array();
            foreach($users as $user){
                $html[] = $user->name;
            }
            $html = implode(', ', $html);
        }else{
            $html = '<ol>';
            foreach($users as $user){
                $html .= "<li>$user->name</li>";
            }
            $html .= '</ol>';
        }

        return  $html;
    }
    function col_haslivedindouble($values) {
        return  ($values->haslivedindouble)?get_string('yes'):get_string('no');
    }
    function col_actions($values) {
        global $OUTPUT, $PAGE;

        if ($this->is_downloading()){
            return '';
        }

        $strdelete  = get_string('delete');
        $stredit  = get_string('edit');
        $strpreview  = get_string('preview', 'local_mxschool');

        $edit = array();

        $aurl = new moodle_url('/local/mxschool/rooming/rooming-form.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/rooming/rooming-requests.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));


        return implode('', $edit);
    }
}
