<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/formslib.php');
require_once('../lib.php');

class weekend_checkin_table extends table_sql {
    private $times = null;

    function __construct($uniqueid, $data) {
        global $CFG;

        parent::__construct($uniqueid);
        $original_data = $data;

        $data->day = array_keys($data->day);
        $first_day = min($data->day);
        $last_day = max($data->day);

        $this->times = array();
        $this->times['before_early'] = strtotime($data->date.' 7:00pm '.$first_day.' day ');
        $this->times['before_late'] = strtotime($data->date.' 11:00pm '.$first_day.' day ');
        $this->times['weekend_firstday'] = strtotime($data->date.' 12:00am '.$first_day.' day ');

        $this->times['after_early'] = strtotime($data->date.' 7:00pm '.$last_day.' day +30 minutes');
        $this->times['after_late'] = strtotime($data->date.' 11:00pm '.$last_day.' day +3 hours');
        $this->times['weekend_lastday'] = strtotime($data->date.' 12:00am '.$last_day.' day ');

        $columns = array('name','room', 'grade');
        $header = array('Name','Room #', 'Grade');
        $i = 1;
        $select = '';
        foreach($data->day as $day){
            $columns[] = 'early'.$i;
            $columns[] = 'late'.$i;
            $header[] = 'Early';
            $header[] = 'Late';
            $select .= ",'' as early$i,'' as late$i";
            $i++;
        }

        $columns = array_merge($columns,array('clean','parent','invite','ok','destination','transportation','phone','departure_time','return_time'));
        $header = array_merge($header,array('Room Clean?','Parent','Invite','OK','Destination','Transportation','Phone#','Departure Time','Return Time'));


        $this->define_columns($columns);
        $this->define_headers($header);
        $this->sort_default_column = 'name';


        $fields = "st.id, (st.room * 1) as room, (st.grade * 1) as grade, CONCAT(u.lastname,' ',u.firstname) as name, '' as clean, w.parent, w.invite, w.ok, w.destination, w.transportation, w.phone, w.id as weekend_id, w.departure_time, w.return_time".$select;
        $from = "{local_mxschool_students} st
                    LEFT JOIN {user} u ON st.userid=u.id
                    LEFT JOIN {local_mxschool_weekend} w ON w.student=st.id AND w.return_time >= :return AND departure_time <= :depart";

        $where = "st.id>0";
        if($data->dorm == 'day' || $data->dorm == 'boarding'){
            $from .= ' LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=st.dorm';
            $where .= " AND d.type='$data->dorm'";
        }elseif($data->dorm != 'all'){
            $where .= " AND st.dorm='$data->dorm'";
        }

        $this->set_sql($fields, $from, $where, array('return'=>$this->times['weekend_firstday'],'depart'=>$this->times['weekend_lastday']));
        $this->define_baseurl(new moodle_url("/local/mxschool/check-in/create-weekend-checkin.php", array('data'=>json_encode($original_data))));
    }

    function col_parent($values){
        $attr = array('type' => 'checkbox','value' => $values->weekend_id,'name'=>'parent');
        if($values->parent == 1)
            $attr['checked'] = 'checked';

        return (isset($values->weekend_id) && $values->weekend_id > 0)?html_writer::empty_tag('input', $attr):'';
    }
    function col_invite($values){
        $attr = array('type' => 'checkbox','value' => $values->weekend_id,'name'=>'invite');
        if($values->invite == 1)
            $attr['checked'] = 'checked';

        return (isset($values->weekend_id) && $values->weekend_id > 0)?html_writer::empty_tag('input', $attr):'';
    }
    function col_ok($values){
        $attr = array('type' => 'checkbox','value' => $values->weekend_id,'name'=>'ok');
        if($values->ok == 1)
            $attr['checked'] = 'checked';

        $button = html_writer::tag('button','Send email',array('value'=>$values->weekend_id));

        return (isset($values->weekend_id) && $values->weekend_id > 0)?html_writer::empty_tag('input', $attr).$button:'';
    }
    function col_departure_time($values){
        return  ($values->departure_time>0)?date('m/d/Y h:i A', $values->departure_time):'';
    }
    function col_return_time($values){
        return  ($values->departure_time>0)?date('m/d/Y h:i A', $values->return_time):'';
    }
    /*function other_cols($column, $row) {
        if(strpos($column,'early') !== false){
            return ($row->departure_time < $this->times['before_early'] && $row->return_time > $this->times['before_early']) ? 'XXX' : '';
        }elseif(strpos($column,'late') !== false){
            return ($row->departure_time < $this->times['after_late'] && $row->return_time > $this->times['after_late']) ? 'XXX' : '';
        }else
            return NULL;
    }*/
}

class weekend_checkin_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB;
        $days = array('-3'=>'Wednesday','-2'=>'Thursday','-1'=>'Friday','0'=>'Saturday','+1'=>'Sunday','+2'=>'Monday','+3'=>'Tuesday');

        $mform          = $this->_form;
        
        $mform->addElement('select', 'dorm', get_string('dorm','local_mxschool'),array_merge(get_dorms_list(),array('boarding'=>get_string('all_dorm_boarding','local_mxschool'),'day'=>get_string('all_dorm_day','local_mxschool'),'all'=>get_string('all_dorm_all','local_mxschool'))));
        $mform->setType('dorm', PARAM_RAW);
        $mform->addRule('dorm', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setDefault('dorm','all');

        $dates = array();
        for($date = strtotime('saturday', get_config('local_mxschool', 'dorms_open')); $date <= get_config('local_mxschool', 'dorms_close'); $date = strtotime("+1 week", $date)){
            $str_date = date('m/d/Y',$date);
            $dates[$str_date] = $str_date;
        }
        $mform->addElement('select', 'date', get_string('weekend','local_mxschool'),$dates);
        $mform->setType('date', PARAM_RAW);
        $mform->addRule('date', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $elements_array=array();
        foreach($days as $key=>$day){
            $elements_array[] = $mform->createElement('checkbox', "day[$key]", $day, $day);
        }
        $mform->addGroup($elements_array, 'day', get_string('long_weekend_days', 'local_mxschool'), array(' '), false);
        $mform->setDefault("day[0]",1);
        $mform->setDefault("day[+1]",1);
        $mform->addRule('day', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $this->add_action_buttons(null, get_string('generate_table', 'local_mxschool'));
    }
}
