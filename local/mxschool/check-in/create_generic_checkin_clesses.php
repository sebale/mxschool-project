<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/formslib.php');
require_once('../lib.php');

class generic_checkin_table extends table_sql {

    function __construct($uniqueid, $dorm) {
        global $CFG;

        parent::__construct($uniqueid);

        $columns = array('name','room', 'grade', 'empty');
        $header = array('Name','Room #', 'Grade', '');

        $this->define_columns($columns);
        $this->define_headers($header);


        $fields = "st.id, (st.room * 1) as room, (st.grade * 1) as grade, CONCAT(u.lastname,' ',u.firstname) as name,'' as empty";
        $from = "{local_mxschool_students} st
                    LEFT JOIN {user} u ON st.userid=u.id";

        $where = "st.id>0";
        if(!empty($dorm)){
            $where .= " AND st.dorm='$dorm'";
        }

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl(new moodle_url("/local/mxschool/check-in/create-generic-checkin.php", array('dorm'=>$dorm)));
    }
}

class generic_checkin_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB;

        $mform          = $this->_form;
        
        $mform->addElement('select', 'dorm', get_string('dorm','local_mxschool'),get_dorms_list());
        $mform->setType('dorm', PARAM_RAW);
        $mform->addRule('dorm', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $this->add_action_buttons(null, get_string('generate_table', 'local_mxschool'));
    }
}
