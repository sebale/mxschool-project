<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/formslib.php');
require_once('../lib.php');

class weekday_checkin_table extends table_sql {

    function __construct($uniqueid, $dorm) {
        global $CFG;

        parent::__construct($uniqueid);

        $columns = array('name','room', 'grade', 'early1','late1', 'early2','late2', 'early3','late3', 'early4','late4', 'early5','late5');
        $header = array('Name','Room #', 'Grade', 'Early','Late', 'Early','Late', 'Early','Late', 'Early','Late', 'Early','Late');

        $this->define_columns($columns);
        $this->define_headers($header);
        $this->sort_default_column = 'name';
        


        $fields = "st.id, (st.room * 1) as room, (st.grade * 1) as grade, CONCAT(u.lastname,' ',u.firstname) as name,'' as early1,'' as late1,'' as early2,'' as late2,'' as early3,'' as late3,'' as early4,'' as late4,'' as early5,'' as late5";
        $from = "{local_mxschool_students} st
                    LEFT JOIN {user} u ON st.userid=u.id";

        $where = "st.id>0";
        if(!empty($dorm)){
            $where .= " AND st.dorm='$dorm'";
        }

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl(new moodle_url("/local/mxschool/check-in/create-weekday-checkin.php", array('dorm'=>$dorm)));
    }
    
    function col_grade($values) {
      return (int)$values->grade;
    }

    function setup() {
        global $SESSION,$CFG;
        if($CFG->version < 2015052301){
            return parent::setup();
        }

        if (empty($this->columns) || empty($this->uniqueid)) {
            return false;
        }

        // Load any existing user preferences.
        if ($this->persistent) {
            $this->prefs = json_decode(get_user_preferences('flextable_' . $this->uniqueid), true);
        } else if (isset($SESSION->flextable[$this->uniqueid])) {
            $this->prefs = $SESSION->flextable[$this->uniqueid];
        }
        $this->prefs = (array)$this->prefs;

        // Set up default preferences if needed.
        if (!$this->prefs or optional_param($this->request[TABLE_VAR_RESET], false, PARAM_BOOL)) {
            $this->prefs = array(
                'collapse' => array(),
                'sortby'   => array(),
                'i_first'  => '',
                'i_last'   => '',
                'textsort' => $this->column_textsort,
            );
        }
        $this->prefs = (array)$this->prefs;
        $oldprefs = $this->prefs;

        if (($showcol = optional_param($this->request[TABLE_VAR_SHOW], '', PARAM_ALPHANUMEXT)) &&
            isset($this->columns[$showcol])) {
            $this->prefs['collapse'][$showcol] = false;

        } else if (($hidecol = optional_param($this->request[TABLE_VAR_HIDE], '', PARAM_ALPHANUMEXT)) &&
            isset($this->columns[$hidecol])) {
            $this->prefs['collapse'][$hidecol] = true;
            if (array_key_exists($hidecol, $this->prefs['sortby'])) {
                unset($this->prefs['sortby'][$hidecol]);
            }
        }

        // Now, update the column attributes for collapsed columns
        foreach (array_keys($this->columns) as $column) {
            if (!empty($this->prefs['collapse'][$column])) {
                $this->column_style[$column]['width'] = '10px';
            }
        }

        if (($sortcol = optional_param($this->request[TABLE_VAR_SORT], '', PARAM_ALPHANUMEXT)) &&
            $this->is_sortable($sortcol) && empty($this->prefs['collapse'][$sortcol]) &&
            (isset($this->columns[$sortcol]) || in_array($sortcol, get_all_user_name_fields())
                && isset($this->columns['fullname']))) {

            if (array_key_exists($sortcol, $this->prefs['sortby'])) {
                // This key already exists somewhere. Change its sortorder and bring it to the top.
                $sortorder = $this->prefs['sortby'][$sortcol] == SORT_ASC ? SORT_DESC : SORT_ASC;
                unset($this->prefs['sortby'][$sortcol]);
                $this->prefs['sortby'] = array_merge(array($sortcol => $sortorder), $this->prefs['sortby']);
            } else {
                // Key doesn't exist, so just add it to the beginning of the array, ascending order
                $this->prefs['sortby'] = array_merge(array($sortcol => SORT_ASC), $this->prefs['sortby']);
            }

            // Finally, make sure that no more than $this->maxsortkeys are present into the array
            $this->prefs['sortby'] = array_slice($this->prefs['sortby'], 0, $this->maxsortkeys);
        }
        
        // MDL-35375 - If a default order is defined and it is not in the current list of order by columns, add it at the end.
        // This prevents results from being returned in a random order if the only order by column contains equal values.
        if (!empty($this->sort_default_column))  {
            if (!array_key_exists($this->sort_default_column, $this->prefs['sortby'])) {
                $defaultsort = array($this->sort_default_column => $this->sort_default_order);
                $this->prefs['sortby'] = array_merge($this->prefs['sortby'], $defaultsort);
            }
        }

        $ilast = optional_param($this->request[TABLE_VAR_ILAST], null, PARAM_RAW);
        if (!is_null($ilast) && ($ilast ==='' || strpos(get_string('alphabet', 'langconfig'), $ilast) !== false)) {
            $this->prefs['i_last'] = $ilast;
        }

        $ifirst = optional_param($this->request[TABLE_VAR_IFIRST], null, PARAM_RAW);
        if (!is_null($ifirst) && ($ifirst === '' || strpos(get_string('alphabet', 'langconfig'), $ifirst) !== false)) {
            $this->prefs['i_first'] = $ifirst;
        }

        // Save user preferences if they have changed.
        if ($this->prefs != $oldprefs) {
            if ($this->persistent) {
                set_user_preference('flextable_' . $this->uniqueid, json_encode($this->prefs));
            } else {
                $SESSION->flextable[$this->uniqueid] = $this->prefs;
            }
        }
        unset($oldprefs);

        if (empty($this->baseurl)) {
            debugging('You should set baseurl when using flexible_table.');
            global $PAGE;
            $this->baseurl = $PAGE->url;
        }

        $this->currpage = optional_param($this->request[TABLE_VAR_PAGE], 0, PARAM_INT);
        $this->setup = true;

        // Always introduce the "flexible" class for the table if not specified
        if (empty($this->attributes)) {
            $this->attributes['class'] = 'flexible';
        } else if (!isset($this->attributes['class'])) {
            $this->attributes['class'] = 'flexible';
        } else if (!in_array('flexible', explode(' ', $this->attributes['class']))) {
            $this->attributes['class'] = trim('flexible ' . $this->attributes['class']);
        }
    }
}

class weekday_checkin_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB;

        $mform          = $this->_form;

        $mform->addElement('select', 'dorm', get_string('dorm','local_mxschool'),get_dorms_list());
        $mform->setType('dorm', PARAM_RAW);
        $mform->addRule('dorm', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $this->add_action_buttons(null, get_string('generate_table', 'local_mxschool'));
    }
}
