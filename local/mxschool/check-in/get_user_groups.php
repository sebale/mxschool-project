<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');

$number_of_groups = optional_param('number_of_groups', '', PARAM_INT);

require_login();

$users = $DB->get_records_sql('SELECT u.id, u.lastname 
                                FROM {local_mxschool_students} s
                                    LEFT JOIN {user} u ON s.userid=u.id
                                ORDER BY u.lastname');
$users_last = array();
foreach($users as $user){
    $users_last[] = $user->lastname;
}

$count_students = count($users_last);

$extra = $count_students % $number_of_groups;
$peoplePerGroup = ($count_students-$extra)/$number_of_groups;

$mod = 0;
$options = '';
for($i = 0; $i < $number_of_groups; $i++){
    $first_user = $users_last[($i*$peoplePerGroup)+$mod];
    if($extra > $mod)
        $mod++;
    $second_user = $users_last[(($i+1)*$peoplePerGroup)-1+$mod];

    $options .= '<option value="'.$i.'">'.$first_user.'-'.$second_user.'</option>';
}

die(json_encode(array('options_text' => $options)));