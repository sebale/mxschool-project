<?php

// This file is part of the ecampus module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles viewing a ecampus
 *
 * @package    mod
 * @subpackage ecampus
 * @copyright  Sebale <info@sebale.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../../config.php");
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$id = optional_param('id', 0, PARAM_INT);
$status = optional_param('status', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);

require_login();
require_capability('local/mxschool:notifications_manage', context_system::instance());

$admin_notifications = $DB->get_records("local_mxschool_notifications");

$title = "Notifications Settings";

$PAGE->set_url(new moodle_url("/local/mxschool/notifications/settings.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/script.js', true);
$PAGE->requires->js('/local/mxschool/assets/js/jquery.classyedit.js', true);
$PAGE->requires->css('/local/mxschool/assets/css/jquery.classyedit.css', true);
$PAGE->set_title($title);
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);

echo $OUTPUT->header();
?>
<div class="notifications-settings">
	<?php echo $OUTPUT->heading($title); ?>
	<?php $types = array('course'=>'C', 'user'=>'S, C', 'module'=>'C', 'system'=>'S'); ?>
	<div class="mx-jtabs-box">
		<div class="mx-jtabs-header">
			<ul class="tabs-list">
				<li class="active"><a>Notifications Settings</a></li>
				<li><a>Support User Settings</a></li>
			</ul>
		</div>
		<div class="mx-jtabs">
			<div class="mx-jtabs-item visible">
				<ul class="not-list">
				<?php foreach($admin_notifications as $item): ?>
					<li value="<?php echo $item->id; ?>" id="not_item_<?php echo $item->id; ?>" class="clearfix <?php echo ($item->status == 1) ? 'active' : ''; ?>">
						<div class="not-box clearfix">
							<div class="desc-box">
								<div class="name"><?php echo $item->subject; ?></div>
							</div>
							<div class="actions-box">
								<div class="actions">
									<span title="Settings" class="btn btn-xs load" <?php echo ($item->status == 0) ? 'disabled="disabled"' : ''; ?>><i class="fa fa-cog"></i></span>
									<span class="btn btn-xs trigger <?php echo ($item->status == 1) ? 'btn-success' : ''; ?>"><?php echo ($item->status == 1) ? 'on' : 'off'; ?></span>
								</div>
							</div>
						</div>
						<div class="not-form-box"></div>
					</li>
				<?php endforeach; ?>
				</ul>
			</div>
			<div class="mx-jtabs-item">
				<div class="not-form-inner">
                    <?php $support_user = $DB->get_record('config', array('name'=>'supportname')); ?>
                    <?php $support_email = $DB->get_record('config', array('name'=>'supportemail')); ?>
                    <?php $noreply_email = $DB->get_record('config', array('name'=>'noreplyaddress')); ?>
                    <form name="support-form" class="not-form clearfix" id="support_form" method="POST" action="<?php echo $CFG->wwwroot;?>/local/mxschool/notifications/ajax.php">
                        <div class="not-form-items-f">
                            <div class="not-form-item">
                                <label>Support User name:</label>
                                <input type="text" value="<?php echo $support_user->value; ?>" name="form[name]" />
                            </div>
                            <div class="not-form-item">
                                <label>Support Email:</label>
                                <input type="text" value="<?php echo $support_email->value; ?>" name="form[email]" />
                            </div>
                            <div class="not-form-item">
                                <label>Support noreply Email:</label>
                                <input type="text" value="<?php echo $noreply_email->value; ?>" name="form[noreply]" />
                            </div>
                            <div class="not-form-item">
                                <input type="hidden" name="action" value="save-support" />
                                <button style="width:80px;" class="send btn btn-success" type="button">Update</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function(){
		jQuery('#support_form .send').click(function(){
			var button = jQuery(this);
			jQuery.ajax({
				url: jQuery('#support_form').attr('action'),
				type: "POST",
				data: jQuery('#support_form').serialize(),
				beforeSend: function(){
					jQuery(button).html('<i class="fa fa-spin fa-spinner"></i>');
				}
			}).done(function( data ) {
				jQuery(button).html('Update');
			});
		});
		
		jQuery('.not-list .btn.trigger').click(function(){
			jQuery(this).toggleClass('btn-success');
			jQuery(this).parent().parent().parent().parent().toggleClass('active');
			var id = jQuery(this).parent().parent().parent().parent().val();
			if(jQuery(this).hasClass('btn-success')){
				jQuery(this).html('on');
				var state = 1;
				jQuery('#not_item_'+id+' .load').removeAttr('disabled');
			}else{
				jQuery(this).html('off');
				var state = 0;
				jQuery('#not_item_'+id+' .load').attr('disabled', true);
			}
			var button = jQuery(this);
			var button_text = jQuery(this).html();
			jQuery.ajax({
				url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/notifications/ajax.php",
				type: "POST",
				data: 'action=adminnot-set-status&id='+id+'&status='+state,
				dataType: "json",
				beforeSend: function(){
					jQuery(button).html('<i class="fa fa-spinner fa-spin"></i>');
				}
			}).done(function( data ) {
				jQuery(button).html(button_text);
			});
		});
		
		jQuery('.not-list .btn.load').click(function(){
			if (jQuery(this).attr('disabled')){
			} else {
				var id = jQuery(this).parent().parent().parent().parent().val();
				jQuery('#not_item_'+id+' .not-form-box').slideToggle();
				jQuery(this).toggleClass('btn-warning');
				jQuery('#not_item_'+id+' .not-form-box').toggleClass('active');
				if (jQuery('#not_item_'+id+' .not-form-box').hasClass('active')){
					jQuery('#not_item_'+id+' .not-form-box').html('<i class="fa fa-spin fa-spinner"></i>');
					jQuery('#not_item_'+id+' .not-form-box').load("<?php echo $CFG->wwwroot;?>/local/mxschool/notifications/ajax.php?action=adminnot-load-form&id="+id, function(){
						jQuery('#not_item_'+id+' .not-form-box .fa-spin').remove();
					});
				} else {
					jQuery('#not_item_'+id+' .not-form-box').empty();
				}
			}
		});
	});
</script>

<?php
echo $OUTPUT->footer();