
<link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/mxschool/classes/alerts/css/lobibox.css"/>
<link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/mxschool/classes/alerts/css/font-awesome.min.css"/>
<script src="<?php echo $CFG->wwwroot; ?>/local/mxschool/classes/alerts/js/jquery.1.11.min.js"></script>
<script src="<?php echo $CFG->wwwroot; ?>/local/mxschool/classes/alerts/js/lobibox.js"></script>

<?php 

class jAlerts {
    protected $_type;
    protected $_text;
    protected $_params;
    
    function __construct() {
        $this->display();
    }
    
    function display($type = 'info', $text = '') {
        if (isset($_SESSION['jalerts'])){
            
            $output = '<script>';
            $output .= 'jQuery(document).ready(function(){';
            $output .= 'Lobibox.notify("'.((isset($_SESSION['jalerts']['type'])) ? $_SESSION['jalerts']['type'] : 'info').'", {';
            $output .= 'sound: false,';
            $output .= "showClass: 'zoomIn',";
            $output .= "hideClass: 'zoomOut',";
            $output .= "position: 'top center',";
            $output .= "iconSource: 'fontAwesome',";
            $output .= "delay: 3000,";
            $output .= "msg: '".((isset($_SESSION['jalerts']['type'])) ? $_SESSION['jalerts']['text'] : '')."'";
            $output .= '});';
            $output .= '});';
            $output .= '</script>';
            echo $output;
            
            $this->destroy();
        }
    }
    
    function create($params = array()) {
        $alert = array();
        $alert['type'] = (isset($params['type'])) ? $params['type'] : 'info';
        $alert['text'] = (isset($params['text'])) ? $params['text'] : '';
        
        $_SESSION['jalerts'] = $alert;
    }
    
    function destroy() {
        unset($_SESSION['jalerts']);
    }
    
}

$jAlert = new jAlerts();

?>