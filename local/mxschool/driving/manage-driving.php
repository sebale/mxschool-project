<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('manage_driving_table.php');

$download = optional_param('download', '', PARAM_ALPHA);
$search = optional_param('search', '', PARAM_RAW);
$action = optional_param('action', '', PARAM_ALPHA);
$id = optional_param('id', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:settings', $systemcontext);

if($action == 'delete' && $id>0){
    $DB->delete_records('local_mxschool_driving',array('student'=>$id));
}

$table = new manage_driving_table('manage_driving_table', $search);
$table->is_collapsible = false;
$table->is_downloading($download, 'driving', 'Driving');
if ($table->is_downloading()){
    $table->out(20, true);
    die();
}

require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$title = get_string('manage_driving','local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/driving/manage-driving.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('driving_name', 'local_mxschool'), new moodle_url('/local/mxschool/driving/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'mxschool-search-form'));
echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search').' ...', 'value' => $search));
echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('search')));
echo html_writer::end_tag("label");
echo html_writer::end_tag("form");

echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
$table->out(20, true);
echo html_writer::end_tag("div");
echo html_writer::link(new moodle_url('/local/mxschool/driving/enter-driving.php'), get_string('create_driving_table', 'local_mxschool'),array('class' => 'btn after-table'));

echo $OUTPUT->footer();
