<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('select_form.php');

$systemcontext   = context_system::instance();
require_login();
require_capability('local/mxschool:advisor_selection_manage', context_system::instance());
$title = get_string('advisor_selection_form', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/advisor_selection/preferences.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('advisor_selection', 'local_mxschool'), new moodle_url('/local/mxschool/advisor_selection/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/script.js', true);
$PAGE->requires->js('/local/mxschool/advisor_selection/select_script.js', true);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title($title);
$PAGE->set_heading($title);

$settings = array();
$settings['second_semester_starts'] = get_config('local_mxschool', 'second_semester_starts');
$settings['dorms_close'] = get_config('local_mxschool', 'dorms_close');
$settings['advisor_form_enable'] = get_config('local_mxschool', 'advisor_form_enable');
$settings['advisor_form_time_prior'] = get_config('local_mxschool', 'advisor_form_time_prior');
$settings['advisor_form_message'] = get_config('local_mxschool', 'advisor_form_message');

$advisors = $DB->get_records_sql("SELECT f.*, CONCAT(u.firstname, ' ', u.lastname) as username, u.email 
                                    FROM {local_mxschool_faculty} f 
                                        LEFT JOIN {user} u ON u.id = f.userid 
                                        WHERE u.id > 0 AND f.available > 0
                                    ORDER BY u.firstname, u.lastname");

if ($settings['advisor_form_enable'] == 'second_semester_starts') { 
    $current_year = date("Y"); $last_year = $current_year - 1;
    
    $students = $DB->get_records_sql("SELECT s.id, s.userid, u.firstname, u.lastname
                         FROM {local_mxschool_students} s
                            LEFT JOIN {user} u ON u.id = s.userid
                                WHERE s.yearofgraduation = ? AND u.id > 0
                         ORDER BY u.lastname, u.firstname, s.middle ASC", array($last_year));
} elseif ($settings['advisor_form_enable'] == 'dorms_close') {
    $students = $DB->get_records_sql("SELECT s.id, s.userid, u.firstname, u.lastname
                         FROM {local_mxschool_students} s
                            LEFT JOIN {user} u ON u.id = s.userid
                                WHERE s.grade < 12 AND u.id > 0
                         ORDER BY u.lastname, u.firstname, s.middle ASC");
} else {
    $students = $DB->get_records_sql("SELECT s.id, s.userid, u.firstname, u.lastname
                         FROM {local_mxschool_students} s
                            LEFT JOIN {user} u ON u.id = s.userid
                                WHERE u.id > 0
                         ORDER BY u.lastname, u.firstname, s.middle ASC");
} 

$editform = new edit_form(null, array('settings'=>$settings, 'students'=>$students, 'advisors'=>$advisors));

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/mxschool/advisor_selection/index.php'));
} else if ($data = $editform->get_data()) {
    
    $new_selection = new stdClass();
    $new_selection->studentid = $data->student;
    $new_selection->currentadvisor = $data->currentadvisor_id;
    $new_selection->keep_current_advisor = $data->keep_current_advisor;
    if ($data->keep_current_advisor == 'no'){
        $new_selection->advisor1 = $data->advisor1;    
        $new_selection->advisor2 = $data->advisor2;    
        $new_selection->advisor3 = $data->advisor3;    
        $new_selection->advisor4 = $data->advisor4;    
        $new_selection->advisor5 = $data->advisor5;    
    }
    $new_selection->finaladvisor = 0;    
    $new_selection->status = 0;    
    $new_selection->comment = $data->comment;  
    $new_selection->timecreated = time();    
    $new_selection->timemodified = time();    
    
    $new_selection->id = $DB->insert_record('local_mxschool_advisors', $new_selection);
    
    redirect(new moodle_url('/local/mxschool/advisor_selection/index.php'));
}

// Print the form.

$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mx-adviser-select-form'));
$editform->display();
echo html_writer::end_tag('div');

echo $OUTPUT->footer();

