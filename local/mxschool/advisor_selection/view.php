<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('view_table.php');

$search     = optional_param('search', '', PARAM_RAW);
$download   = optional_param('download', '', PARAM_ALPHA);
$filter     = optional_param('filter', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);

require_login();
require_capability('local/mxschool:advisor_selection_manage', context_system::instance());

$title = get_string('view_and_edit', 'local_mxschool');

if ($action == 'delete' and $id) {
    $DB->delete_records('local_mxschool_advisors', array('id'=>$id));
    redirect(new moodle_url("/local/mxschool/advisor_selection/view.php"));
}

$PAGE->set_url(new moodle_url("/local/mxschool/advisor_selection/view.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('advisor_selection', 'local_mxschool'), new moodle_url('/local/mxschool/advisor_selection/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new view_table('view_table', $search, $filter, $download);
$table->is_collapsible = false;
$table->is_downloading($download, 'Advisor_selection_'.date('m_d_Y'));

if (!$table->is_downloading()) {
    echo $OUTPUT->header();
    echo $OUTPUT->heading($title);
    echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'mxschool-search-form'));
    echo html_writer::start_tag("label",  array());
    echo html_writer::start_tag('select', array('name' => 'filter'));
        echo html_writer::tag('option', 'Show all', array('value' => ''));
        $options = array('value' => 1); if($filter == 1) $options['selected'] = 'selected';
        echo html_writer::tag('option', 'Pending', $options);
        $options = array('value' => 2); if($filter == 2) $options['selected'] = 'selected';
        echo html_writer::tag('option', 'Choosen', $options);
        $options = array('value' => 3); if($filter == 3) $options['selected'] = 'selected';
        echo html_writer::tag('option', 'Finalized', $options);
    echo html_writer::end_tag("select");
    echo html_writer::end_tag("label");
    echo html_writer::start_tag("label",  array());
    echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search').' ...', 'value' => $search));
    echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('search')));
    echo html_writer::end_tag("label");
    echo html_writer::end_tag("form");
    
    echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
}

$table->out(20, true);

if (!$table->is_downloading()) {
    echo html_writer::end_tag("div");
    echo $OUTPUT->footer();
}
