<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $settings       = $this->_customdata['settings'];
        $record         = $this->_customdata['record'];
        $advisors       = $this->_customdata['advisors'];
        $student        = $this->_customdata['student'];
        $currentadvisor = $this->_customdata['currentadvisor'];
        
        $systemcontext   = context_system::instance();
        $this->context = $systemcontext;
        
        $advisors_options = array('0'=>'----------');
        if (count($advisors)){
            foreach ($advisors as $advisor){
                $advisors_options[$advisor->id] = $advisor->username;
            }
        }
        
        $mform->addElement('hidden', 'id', $record->id);
        $mform->addElement('hidden', 'currentadvisor_id', $record->currentadvisor_id, array('class'=>'currentadvisor_id'));
        $mform->addElement('hidden', 'studentid', $record->currentadvisor);
        
        $mform->addElement('text', 'student', get_string('studentname', 'local_mxschool'), array('class="student"', 'disabled'=>'disabled'));
        $mform->setDefault('student', $student->lastname.', '.$student->firstname);
        
        $mform->addElement('text', 'currentadvisor', get_string('currentadvisor', 'local_mxschool'), array("class"=>"current-advisor", 'disabled'=>'disabled'));
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'keep_current_advisor', '', get_string('yes'), 'yes', array('class'=>'keep-advisor'));
        $radioarray[] = $mform->createElement('radio', 'keep_current_advisor', '', get_string('no'), 'no', array('class'=>'keep-advisor'));
        $mform->addGroup($radioarray, 'radioar', get_string('keep_current_advisor', 'local_mxschool'), array(''), false);
        $mform->addRule('radioar', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('html', '<div class="advisor-text">Please rank you top five advisor choices in descending order.</div>');
        
        $mform->addElement('select', 'advisor1', 'First Choice', $advisors_options, array("class"=>"advisor-selection"));
        $mform->addRule('advisor1', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('select', 'advisor2', 'Second Choice', $advisors_options, array("class"=>"advisor-selection"));
        $mform->addRule('advisor2', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('select', 'advisor3', 'Third Choice', $advisors_options, array("class"=>"advisor-selection"));
        $mform->addRule('advisor3', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('select', 'advisor4', 'Fourth Choice', $advisors_options, array("class"=>"advisor-selection"));
        $mform->addRule('advisor4', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
       
        $mform->addElement('select', 'advisor5', 'Fifth Choice', $advisors_options, array("class"=>"advisor-selection"));
        $mform->addRule('advisor5', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('select', 'finaladvisor', 'Chosen Advisor', $advisors_options, array("class"=>"advisor-selection"));
        
        $mform->addElement('textarea', 'comment', 'Comments', array("class"=>"advisor-selection", "placeholder"=>"If there is additional information you'd like the deans to know, please use this space.", "style"=>"width:50%; min-height: 80px;"));
        
        $this->add_action_buttons(get_string('cancel'), get_string('save', 'local_mxschool'));
        
        // Finally set the current form data
        $this->set_data($record);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        return $errors;
    }
}

