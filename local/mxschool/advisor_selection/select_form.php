<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $settings       = $this->_customdata['settings'];
        $advisors       = $this->_customdata['advisors'];
        $students       = $this->_customdata['students'];
        
        $systemcontext   = context_system::instance();
        $this->context = $systemcontext;
        
        $advisors_options = array('0'=>'----------');
        if (count($advisors)){
            foreach ($advisors as $advisor){
                $advisors_options[$advisor->id] = $advisor->username;
            }
        }
        
        $students_options = array(''=>'----------');
        if (count($students)){
            foreach ($students as $student){
                $students_options[$student->id] = $student->lastname.', '.$student->firstname;
            }
        }
        
        $mform->addElement('hidden', 'currentadvisor_id', 0, array('class'=>'currentadvisor_id'));
        
        $mform->addElement('select', 'student', get_string('studentname', 'local_mxschool'), $students_options, array('class="student"'));
        $mform->addRule('student', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('text', 'currentadvisor', get_string('currentadvisor', 'local_mxschool'), array("class"=>"current-advisor", "disabled"=>"disabled"));
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'keep_current_advisor', '', get_string('yes'), 'yes', array('class'=>'keep-advisor'));
        $radioarray[] = $mform->createElement('radio', 'keep_current_advisor', '', get_string('no'), 'no', array('class'=>'keep-advisor'));
        $mform->addGroup($radioarray, 'radioar', get_string('keep_current_advisor', 'local_mxschool'), array(''), false);
        $mform->addRule('radioar', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('html', '<div class="advisor-text">Please rank you top five advisor choices in descending order.</div>');
        
        $mform->addElement('select', 'advisor1', 'First Choice', $advisors_options, array("class"=>"advisor-selection"));
        $mform->addRule('advisor1', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('select', 'advisor2', 'Second Choice', $advisors_options, array("class"=>"advisor-selection"));
        $mform->addRule('advisor2', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('select', 'advisor3', 'Third Choice', $advisors_options, array("class"=>"advisor-selection"));
        $mform->addRule('advisor3', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('select', 'advisor4', 'Fourth Choice', $advisors_options, array("class"=>"advisor-selection"));
        $mform->addRule('advisor4', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
       
        $mform->addElement('select', 'advisor5', 'Fifth Choice', $advisors_options, array("class"=>"advisor-selection"));
        $mform->addRule('advisor5', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('textarea', 'comment', 'Comments', array("class"=>"advisor-selection hidden", "placeholder"=>"If there is additional information you'd like the deans to know, please use this space.", "style"=>"width:50%;min-height: 80px;"));
        
        $this->add_action_buttons(get_string('cancel'), get_string('save', 'local_mxschool'));
        
        $mform->addElement('html', '<h3 class="form-label-h3">'.get_string('advisors', 'local_mxschool').'</h3>');
        
        $mform->addElement('html', '<div class="advisors-box clearfix">');
        if (count($advisors) > 0){
            foreach ($advisors as $advisor){
                $mform->addElement('checkbox', 'advisor_'.$advisor->id, $advisor->username, '', array('class'=>'onoff-switcher'));
                if ($advisor->available > 0){
                    $mform->setDefault('advisor_'.$advisor->id, true);
                }
            }
        }
        $mform->addElement('html', '</div>');
        
        // Finally set the current form data
        $this->set_data($settings);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        return $errors;
    }
}

