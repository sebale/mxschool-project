<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class manage_table extends table_sql {
    
    public $_active_advisors = array();
    public $_all_advisors = array();
    
    function __construct($uniqueid, $search, $filter) {
		global $CFG, $USER, $DB;

        parent::__construct($uniqueid);
        
        $this->set_advisers();
        
        $columns = array('student', 'grade', 'current_advisor', 'keep_current_advisor', 'advisorchoice', 'status');
        $headers = array(
            get_string('student', 'local_mxschool'),
            get_string('grade'),
            get_string('currentadvisor', 'local_mxschool'),
            get_string('keep_current_advisor', 'local_mxschool'),
            get_string('advisorchoice', 'local_mxschool'),
            'Finalized'
        );

        $this->define_columns($columns);
        $this->define_headers($headers);
        
        $sql_search = ($search) ? " AND (s.student LIKE '%$search%' OR ca.current_advisor LIKE '%$search%' OR a1.advisor_1 LIKE '%$search%' OR a2.advisor_2 LIKE '%$search%' OR a3.advisor_3 LIKE '%$search%' OR a4.advisor_4 LIKE '%$search%' OR a5.advisor_5 LIKE '%$search%' OR fa.finaladvisor_name LIKE '%$search%')" : "";
        if ($filter){
            if ($filter == '1') {
                $sql_search .= " AND mas.keep_current_advisor = 'no'";
            } elseif ($filter == '2'){
                $sql_search .= " AND mas.keep_current_advisor = 'yes'";
            } elseif ($filter == '3'){
                $sql_search .= " AND mas.finaladvisor = 0";
            } elseif ($filter == '4'){
                $sql_search .= " AND mas.finaladvisor > 0 AND mas.status = 0";
            } elseif ($filter == '5'){
                $sql_search .= " AND mas.finaladvisor > 0 AND mas.status > 0";
            } 
        }
        
        $fields = "mas.id, mas.keep_current_advisor, mas.comment, mas.currentadvisor, mas.finaladvisor, s.student, s.grade, ca.current_advisor, mas.advisor1, mas.advisor2, mas.advisor3, mas.advisor4, mas.advisor5, fa.finaladvisor_name, mas.status ";
        $from = "{local_mxschool_advisors} mas
                    LEFT JOIN (SELECT s.id, CONCAT(u.lastname, ', ', u.firstname) as student, s.grade FROM {local_mxschool_students} s LEFT JOIN {user} u ON u.id = s.userid WHERE u.id > 0) s ON s.id = mas.studentid
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as current_advisor FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) ca ON ca.id = mas.currentadvisor
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_1 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a1 ON a1.id = mas.advisor1
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_2 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a2 ON a2.id = mas.advisor2
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_3 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a3 ON a3.id = mas.advisor3
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_4 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a4 ON a4.id = mas.advisor4
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_5 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a5 ON a5.id = mas.advisor5
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as finaladvisor_name FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) fa ON fa.id = mas.finaladvisor
                ";
        $where = 'mas.id > 0'.$sql_search;
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/mxschool/advisor_selection/manage.php?search=".$search."&filter=".$filter);
    }
    
    function col_keep_current_advisor($values) {
      return ucfirst($values->keep_current_advisor);
    }
    
    function col_status($values) {
      return ($values->status > 0) ? 'Yes' : 'No';
    }
    
    function col_advisorchoice($values) {
        global $OUTPUT, $PAGE;    
        
        $advisors = $this->_advisors;
            
        $output = '';
        if ($this->is_downloading()){
            $output .= ((isset($this->_all_advisors[$values->currentadvisor])) ? $this->_all_advisors[$values->currentadvisor] . ';' : '')
                        . ((isset($this->_all_advisors[$values->advisor1])) ? $this->_all_advisors[$values->advisor1] . ';' : '')
                        . ((isset($this->_all_advisors[$values->advisor2])) ? $this->_all_advisors[$values->advisor2] . ';' : '')
                        . ((isset($this->_all_advisors[$values->advisor3])) ? $this->_all_advisors[$values->advisor3] . ';' : '')
                        . ((isset($this->_all_advisors[$values->advisor4])) ? $this->_all_advisors[$values->advisor4] . ';' : '')
                        . ((isset($this->_all_advisors[$values->advisor5])) ? $this->_all_advisors[$values->advisor5] . ';' : '');
        } else {
            $output .= html_writer::start_tag('div', array('class' => 'adv-select-box', 'id' => 'final-adv-'.$values->id,));
            if ($values->keep_current_advisor == 'yes'){
                $output .= html_writer::start_tag('label', array('class'=>'adv-select-div'));
                $options = array('type'=>'radio', 'value'=>$values->currentadvisor, 'name'=>'final-adv-'.$values->id, 'class'=>'adv-select'); 
                if($values->finaladvisor == $values->currentadvisor) $options['checked'] = 'checked';
                $output .= html_writer::tag('input', $values->current_advisor, $options);
                $output .= html_writer::end_tag('label');
            } else {
                if ($values->advisor1 > 0){
                    if (isset($this->_all_advisors[$values->advisor1])){
                        $output .= html_writer::start_tag('label', array('class'=>'adv-select-div'));
                        $options = array('type'=>'radio', 'value'=>$values->advisor1, 'name'=>'final-adv-'.$values->id, 'class'=>'adv-select'); 
                        if($values->finaladvisor == $values->advisor1) $options['checked'] = 'checked';
                        $output .= html_writer::tag('input', $this->_all_advisors[$values->advisor1], $options);
                        $output .= html_writer::end_tag('label');
                    }
                }
                if ($values->advisor2 > 0){
                    if (isset($this->_all_advisors[$values->advisor2])){
                        $output .= html_writer::start_tag('label', array('class'=>'adv-select-div'));
                        $options = array('type'=>'radio', 'value'=>$values->advisor2, 'name'=>'final-adv-'.$values->id, 'class'=>'adv-select'); 
                        if($values->finaladvisor == $values->advisor2) $options['checked'] = 'checked';
                        $output .= html_writer::tag('input', $this->_all_advisors[$values->advisor2], $options);
                        $output .= html_writer::end_tag('label');
                    }
                }
                if ($values->advisor3 > 0){
                    if (isset($this->_all_advisors[$values->advisor3])){
                        $output .= html_writer::start_tag('label', array('class'=>'adv-select-div'));
                        $options = array('type'=>'radio', 'value'=>$values->advisor3, 'name'=>'final-adv-'.$values->id, 'class'=>'adv-select'); 
                        if($values->finaladvisor == $values->advisor3) $options['checked'] = 'checked';
                        $output .= html_writer::tag('input', $this->_all_advisors[$values->advisor3], $options);
                        $output .= html_writer::end_tag('label');
                    }
                }
                if ($values->advisor4 > 0){
                    if (isset($this->_all_advisors[$values->advisor4])){
                        $output .= html_writer::start_tag('label', array('class'=>'adv-select-div'));
                        $options = array('type'=>'radio', 'value'=>$values->advisor4, 'name'=>'final-adv-'.$values->id, 'class'=>'adv-select'); 
                        if($values->finaladvisor == $values->advisor4) $options['checked'] = 'checked';
                        $output .= html_writer::tag('input', $this->_all_advisors[$values->advisor4], $options);
                        $output .= html_writer::end_tag('label');
                    }
                }
                if ($values->advisor5 > 0){
                    if (isset($this->_all_advisors[$values->advisor5])){
                        $output .= html_writer::start_tag('label', array('class'=>'adv-select-div'));
                        $options = array('type'=>'radio', 'value'=>$values->advisor5, 'name'=>'final-adv-'.$values->id, 'class'=>'adv-select'); 
                        if($values->finaladvisor == $values->advisor5) $options['checked'] = 'checked';
                        $output .= html_writer::tag('input', $this->_all_advisors[$values->advisor5], $options);
                        $output .= html_writer::end_tag('label');
                    }
                }

                $output .= html_writer::start_tag('label', array('class'=>'adv-select-div'));
                $output .= html_writer::tag('input', 'Other', array('type'=>'radio', 'value'=>'0', 'name'=>'final-adv-'.$values->id.'[]', 'id' => 'final-adv-'.$values->id, 'class'=>'adv-select-other'));
                $output .= html_writer::end_tag('label');
            }
            $output .= $this->print_advisors_select($values);
            $output .= html_writer::end_tag("div");
        }
        
      return $output;
    }
    
    function set_advisers(){
        global $DB;
        
        $advisors = $DB->get_records_sql("SELECT f.*, CONCAT(u.firstname, ' ', u.lastname) as username, u.email 
                                    FROM {local_mxschool_faculty} f 
                                        LEFT JOIN {user} u ON u.id = f.userid 
                                        WHERE u.id > 0 
                                    ORDER BY u.firstname, u.lastname");
        
        $all_advisors = array(); $active_advisors = array();
        foreach ($advisors as $advisor){
            $all_advisors[$advisor->id] = $advisor->username;
            
            if ($advisor->available > 0){
                $active_advisors[$advisor->id] = $advisor->username;   
            }
        }
        
        $this->_all_advisors = $all_advisors;
        $this->_active_advisors = $active_advisors;
    }
    
    function print_advisors_select($values){
        global $DB, $CFG;
        $output = '';

        $output = '';
        $output .= html_writer::start_tag('div', array('class'=>'clone-select-box', 'style'=>'display:none;'));
        $output .= html_writer::start_tag('select', array('name' => 'other-adv-select-'.$values->id, 'id'=>'other-adv-select-'.$values->id, 'data-id'=>$values->id, 'class'=>'other-adv-select'));
        $output .= html_writer::tag('option', '', array('value' => ''));
            foreach ($this->_active_advisors as $advisorid=>$advisorname){
                if ($advisorid == $values->currentadvisor or 
                    $advisorid == $values->advisor1 or 
                    $advisorid == $values->advisor2 or 
                    $advisorid == $values->advisor3 or 
                    $advisorid == $values->advisor4 or 
                    $advisorid == $values->advisor5
                   ) continue;
                $output .= html_writer::tag('option', $advisorname, array('value' => $advisorid));   
            }
        $output .= html_writer::end_tag("select");
        $output .= html_writer::end_tag("div");

        return $output;
    }
}
