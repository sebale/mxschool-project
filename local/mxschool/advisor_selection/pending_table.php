<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class pending_table extends table_sql {
    function __construct($uniqueid, $search, $download) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        
        $columns = array('student', 'grade', 'email');
        $headers = array(
            get_string('student', 'local_mxschool'),
            get_string('grade'),
            get_string('email')
        );
        
        $this->define_columns($columns);
        $this->define_headers($headers);
        
        $sql_search = ($search) ? " AND (u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%')" : "";
        
        $fields = "s.id, s.userid, s.grade, CONCAT(u.lastname, ', ', u.firstname) as student, u.email";
        $from = " {local_mxschool_students} s 
                    LEFT JOIN {user} u ON u.id = s.userid";
        $where = 's.id NOT IN (
                        SELECT mas.studentid FROM {local_mxschool_advisors} mas
                    )'.$sql_search;
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/mxschool/advisor_selection/pending.php?search=".$search);
    }
    
   
}
