<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool capabilities.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class events_handler{
    public static function role_assigned($role){
        global $DB;
        $user_roles = get_config('local_mxschool', 'user_roles');
        $user_roles = (empty($user_roles))?new stdClass():json_decode($user_roles);

        if(isset($user_roles->student_role) && $user_roles->student_role == $role->roleid && $role->contextid == 1){
            $user = $DB->get_record('user',array('id'=>$role->userid));
            
            $new_record = new stdClass();
            $new_record->userid = $user->id;
            $new_record->preferred = $user->lastname;
            $DB->insert_record('local_mxschool_students',$new_record,false);

        }elseif(isset($user_roles->faculty_member_role) && $user_roles->faculty_member_role == $role->roleid && $role->contextid == 1){
            $new_record = new stdClass();
            $new_record->userid = $role->userid;
            $DB->insert_record('local_mxschool_faculty',$new_record,false);

        }elseif(isset($user_roles->peer_tutors_role) && $user_roles->peer_tutors_role == $role->roleid && $role->contextid == 1){
            $new_record = new stdClass();
            $new_record->userid = $role->userid;
            $DB->insert_record('local_mxschool_tutors',$new_record,false);
        }
        return true;
    }
    public static function role_unassigned($role){
        global $DB;
        $user_roles = get_config('local_mxschool', 'user_roles');
        $user_roles = (empty($user_roles))?new stdClass():json_decode($user_roles);
        
        if(isset($user_roles->student_role) && $user_roles->student_role == $role->roleid && $role->contextid == 1){
            $student  = $DB->get_record('local_mxschool_students', array('userid'=>$role->userid));
            if ($student){
                $DB->delete_records('local_mxschool_parents',array('childid'=>$student->id));    
                $DB->delete_records('local_mxschool_advisors',array('studentid'=>$student->id));    
                $DB->delete_records('local_mxschool_transport',array('studentid'=>$student->id));
            }
            $DB->delete_records('local_mxschool_students',array('userid'=>$role->userid));
        }elseif(isset($user_roles->faculty_member_role) && $user_roles->faculty_member_role == $role->roleid && $role->contextid == 1){
            $DB->delete_records('local_mxschool_faculty',array('userid'=>$role->userid));
        }elseif(isset($user_roles->peer_tutors_role) && $user_roles->peer_tutors_role == $role->roleid && $role->contextid == 1){
            $DB->delete_records('local_mxschool_tutors',array('userid'=>$role->userid));
        }
        return true;
    }
}
