<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');

$id = required_param('id',PARAM_INT);
$action = required_param('action',PARAM_RAW);

require_login();
$systemcontext   = context_system::instance();

$PAGE->set_context($systemcontext);

if($action == 'check'){
    //require_capability('local/mxschool:edriving_settings', $systemcontext);
    $record = $DB->get_record('local_mxschool_epassenger',array('id'=>$id));
    $record->granded = 1;
    $DB->update_record('local_mxschool_epassenger',$record);
    //$edit = $OUTPUT->action_icon('#', new pix_icon('t/block', 'Unchecked Back In', 'core', array('class' => 'iconsmall')), null, array('data_id'=>$record->id, 'onclick'=>"unchecked_back_in(this);"));

    $edit = html_writer::empty_tag('input',array('type'=>'checkbox','class'=>'onoff-switcher','checked'=>'checked', 'onchange'=>"unchecked_back_in(this);", 'data_id'=>$record->id));
    die(json_encode(array('string'=>get_string('yes'),'link'=>$edit)));
}elseif($action == 'uncheck'){
    //require_capability('local/mxschool:edriving_settings', $systemcontext);
    $record = $DB->get_record('local_mxschool_epassenger',array('id'=>$id));
    $record->granded = 0;
    $DB->update_record('local_mxschool_epassenger',$record);

    $edit = html_writer::empty_tag('input',array('type'=>'checkbox','class'=>'onoff-switcher', 'onchange'=>"checked_back_in(this);", 'data_id'=>$record->id));
    //$edit = $OUTPUT->action_icon('#', new pix_icon('t/check', 'Checked Back In', 'core', array('class' => 'iconsmall')), null, array('data_id'=>$record->id, 'onclick'=>"checked_back_in(this);"));
    die(json_encode(array('string'=>get_string('no'),'link'=>$edit)));
}
