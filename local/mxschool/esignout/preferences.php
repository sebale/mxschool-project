<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('preferences_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$systemcontext   = context_system::instance();
require_login();
require_capability('local/mxschool:edriving_settings', context_system::instance());
$title = get_string('preferences', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/esignout/preferences.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('driver_name', 'local_mxschool'), new moodle_url('/local/mxschool/esignout/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/script.js', true);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title($title);
$PAGE->set_heading($title);

$settings = array();
$settings['driver_grades'] = get_config('local_mxschool', 'driver_grades');
$settings['permissions_required_text'] = get_config('local_mxschool', 'permissions_required_text');

$editform = new edit_form(null, array('settings'=>$settings));

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/mxschool/rooming/index.php'));
} else if ($data = $editform->get_data()) {
    set_config('driver_grades', implode(',',$data->driver_grades), 'local_mxschool');
    set_config('permissions_required_text', $data->permissions_required_text, 'local_mxschool');

    $jAlert->create(array('type'=>'success', 'text'=>'Preferences was successfully saved'));
    redirect(new moodle_url('/local/mxschool/esignout/index.php'));
}

// Print the form.

$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mx-adviser-settings-form'));
$editform->display();
echo html_writer::end_tag('div');

echo $OUTPUT->footer();
