<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/formslib.php');

class manage_driver_table extends table_sql {

    function __construct($uniqueid, $search) {
        global $CFG,$DB,$USER;

        parent::__construct($uniqueid);

        $student = $DB->get_record('local_mxschool_students',array('userid'=>$USER->id));

        $columns = array('driver', 'passenger', 'departure_time', 'destination', 'return_time', 'granded_from', 'granded', 'actions');
        $header = array('Driver`s Name', 'Passenger Name(s)', 'Departure Time', 'Destination', 'Return Time', 'Faculty Permission','Driver Checked In?', 'Sign In');

        $this->define_columns($columns);
        $this->define_headers($header);

        $stu = (isset($student->id))?1:0;
        $fields = "d.*, CONCAT (u.firstname,' ',u.lastname) as driver, CONCAT (uf.firstname,' ',uf.lastname) as granded_from, $stu as student";
        $from = "{local_mxschool_edriver} d 
                    LEFT JOIN {local_mxschool_students} s ON s.id=d.driver
                    LEFT JOIN {user} u ON u.id=s.userid
                    LEFT JOIN {local_mxschool_faculty} f ON f.id=d.granded_from
                    LEFT JOIN {user} uf ON uf.id=f.userid
                    ";

        $where = "d.id>0";
        if(!empty($search)){
            //LIKE '%$search%'

            $where .= " AND (CONCAT (u.firstname,' ',u.lastname) LIKE '%$search%' OR CONCAT (uf.firstname,' ',uf.lastname) LIKE '%$search%' OR d.destination LIKE '%$search%' )";
        }
        if(isset($student->id)){
            $where .= " AND d.driver=" . $student->id;
            $today_start  = strtotime("today 00:01");
            $today_end  = strtotime("today 23:59");

            $where .= " AND d.timectreate > $today_start AND d.timectreate < $today_end";
        }

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }

    function col_departure_time($values) {
        return date('m/d/Y h:i A',$values->departure_time);
    }
    function col_return_time($values) {
        return date('m/d/Y h:i A',$values->return_time);
    }
    function col_passenger($values) {
        global $DB;
        $values->passenger = ltrim($values->passenger,',');
        $records = $DB->get_records_sql("SELECT CONCAT (u.firstname,' ',u.lastname) as name
                                         FROM {local_mxschool_students} s 
                                            LEFT JOIN {user} u ON u.id=s.userid
                                         WHERE s.id IN ($values->passenger)");
        $names = array();
        foreach($records as $record){
            $names[] = $record->name;
        }
        return implode(', ',$names);
    }
    function col_granded($values) {
        return ($values->granded)?get_string('yes'):get_string('no');
    }

    function col_actions($values) {
        global $OUTPUT;

        if ($this->is_downloading()){
            return '';
        }

        $strdelete  = get_string('delete');
        $stredit  = get_string('edit');

        $edit = array();

        if($values->granded){
            //$edit[] = $OUTPUT->action_icon('#', new pix_icon('t/block', 'Unchecked Back In', 'core', array('class' => 'iconsmall')), null, array('data_id'=>$values->id, 'onclick'=>"unchecked_back_in(this);"));
            $edit[] = html_writer::empty_tag('input',array('type'=>'checkbox','class'=>'onoff-switcher','checked'=>'checked', 'onchange'=>"unchecked_back_in(this);", 'data_id'=>$values->id));
        }else{
            //$edit[] = $OUTPUT->action_icon('#', new pix_icon('t/check', 'Checked Back In', 'core', array('class' => 'iconsmall')), null, array('data_id'=>$values->id, 'onclick'=>"checked_back_in(this);"));
            $edit[] = html_writer::empty_tag('input',array('type'=>'checkbox','class'=>'onoff-switcher', 'onchange'=>"checked_back_in(this);", 'data_id'=>$values->id));
        }

        if($values->student != 1){
            $aurl = new moodle_url('/local/mxschool/esignout/enter-driving.php', array('id' => $values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

            $aurl = new moodle_url('/local/mxschool/esignout/drivings.php', array('action' => 'delete', 'id' => $values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick' => "if (!confirm('Are you sure want to delete this redord?')) return false;"));
        }

        return implode('', $edit);
    }
}

class enter_driver_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB,$USER;

        $mform          = $this->_form;
        $record_id     = $this->_customdata['id'];
        $driver_grades  = get_config('local_mxschool', 'driver_grades');
        $student = $DB->get_record('local_mxschool_students',array('userid'=>$USER->id));
        
        $record = $DB->get_record('local_mxschool_edriver',array('id'=>$record_id));
        if(!empty($driver_grades)){
            $drivers_obj = $DB->get_records_sql("SELECT s.id, CONCAT (u.firstname,' ',u.lastname) as name
                                            FROM {local_mxschool_students} s
                                              LEFT JOIN {user} u ON u.id=s.userid
                                              LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation = s.dorm
                                            WHERE s.grade IN($driver_grades) AND d.type='day' AND (s.maydrive = 1 OR s.maygiverides = 1)
                                              ");
        }else{
            $drivers_obj = array();
        }
        $drivers = array(''=>'Driver\'s Name');
        foreach($drivers_obj as $driver){
            $drivers[$driver->id] = $driver->name;
        }

        $passengers_obj = $DB->get_records_sql("SELECT s.id, CONCAT (u.firstname,' ',u.lastname) as name
                                            FROM {local_mxschool_students} s
                                              LEFT JOIN {user} u ON u.id=s.userid
                                            WHERE s.dorm NOT LIKE 'wd'
                                              ");
        $passengers = array(''=>'No Passenger');
        foreach($passengers_obj as $passender){
            $passengers[$passender->id] = $passender->name;
        }


        $faculty_obj = $DB->get_records_sql("SELECT f.id, CONCAT (u.firstname,' ',u.lastname) as name
                                            FROM {local_mxschool_faculty} f
                                              LEFT JOIN {user} u ON u.id=f.userid
                                              ");
        $faculties = array(''=>'Faculty Name');
        foreach($faculty_obj as $faculty){
            $faculties[$faculty->id] = $faculty->name;
        }

        if(isset($student->id) AND !isset($drivers[$student->id])){
            redirect('/my/');
        }elseif(isset($student->id)){
            $mform->addElement('hidden', 'driver', $student->id);
            $mform->setType('driver', PARAM_INT);
        }else{
            $mform->addElement('select', 'driver', get_string('driver_name', 'local_mxschool'), $drivers);
            $mform->setType('driver', PARAM_INT);
            $mform->addRule('driver', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
            if(isset($record->id))
                $mform->setDefault('driver', $record->driver);
        }

        $mform->addElement('select', 'passengers', get_string('passengers_name', 'local_mxschool'), $passengers);
        $mform->setType('passengers', PARAM_RAW);
        $mform->getElement('passengers')->setMultiple(true);
        $mform->addRule('passengers', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->getElement('passengers')->setSelected(explode(',',$record->passenger));

        $times = array('startyear' => date('Y',strtotime('-6 month',get_config('local_mxschool','second_semester_starts'))),'stopyear'  => date('Y',get_config('local_mxschool','second_semester_starts')));
        /*$mform->addElement('date_time_selector', 'departure_time', get_string('departure_date', 'local_mxschool'), $times);
        $mform->setType('departure_time', PARAM_INT);
        $mform->addRule('departure_time', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('departure_time', $record->departure_time);*/
        
        $mform->addElement('text', 'departure_time', get_string('departure_date','local_mxschool'));
        $mform->setType('departure_time', PARAM_RAW);
        $mform->addRule('departure_time', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id)){
            $mform->setDefault('departure_time', date('m/d/Y h:i A', $record->departure_time));
        }

        $mform->addElement('text', 'destination', get_string('destination', 'local_mxschool'));
        $mform->setType('destination', PARAM_RAW);
        $mform->addRule('destination', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('destination', $record->destination);

        /*$mform->addElement('date_time_selector', 'return_time', get_string('estimated_return_time', 'local_mxschool'), $times);
        $mform->setType('return_time', PARAM_INT);
        $mform->addRule('return_time', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('return_time', $record->return_time);*/
        
        $mform->addElement('text', 'return_time', get_string('estimated_return_time','local_mxschool'));
        $mform->setType('return_time', PARAM_RAW);
        $mform->addRule('return_time', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id)){
            $mform->setDefault('return_time', date('m/d/Y h:i A', $record->return_time));
        }

        $mform->addElement('select', 'granted_by', get_string('permission_granted_by', 'local_mxschool'), $faculties);
        $mform->setType('granted_by', PARAM_INT);
        $mform->addRule('granted_by', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('granted_by', $record->granded_from);

        $mform->addElement('hidden', 'id', $record_id);
        $mform->setType('id', PARAM_INT);

        $this->add_action_buttons(get_string('cancel'), get_string('submit_form', 'local_mxschool'));
    }
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        if(strtotime($data['return_time']) <= strtotime($data['departure_time'])){
            $errors['departure_time'] = 'Estimated Return Time cannot be BEFORE Departure Time';
        }

        return $errors;
    }
}
