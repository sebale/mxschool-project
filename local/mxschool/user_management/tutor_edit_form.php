<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $id             = $this->_customdata['id'];
        $user           = $this->_customdata['user'];
        
        
        $mform->addElement('hidden', 'id', $id);
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('hidden', 'userid', $user->id);
        $mform->setType('userid', PARAM_INT);

        $mform->addElement('text', 'firstname', 'First name', 'maxlength="254"  size="50"');
        $mform->addRule('firstname', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('firstname', PARAM_TEXT);
        if (isset($user->firstname)){
            $mform->setDefault('firstname', $user->firstname);
        }
        
        $mform->addElement('text', 'lastname', 'Last name', 'maxlength="254"  size="50"');
        $mform->addRule('lastname', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('lastname', PARAM_TEXT);
        if (isset($user->lastname)){
            $mform->setDefault('lastname', $user->lastname);
        }
        
        $this->add_action_buttons(get_string('cancel'), get_string('update'));

    }

}

