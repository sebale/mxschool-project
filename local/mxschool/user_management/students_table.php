<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class students_table extends table_sql {

    
    function __construct($uniqueid, $search, $filter) {
		global $CFG, $USER, $DB;

        parent::__construct($uniqueid);

        $filter->type = (empty($filter->type))?'default':$filter->type;

        $headers["Permissions"] = array("Overnight", "Driving", "To Boston?", "Can Drive to Town?", "Can Give Rides?", "Competent Swimmer?","Swimming","Boating","Comment");
        $headers["Birthdays"] = array("Date");
        $headers["Parents"] = array("Parent Name","Address","Home Phone","Cell Phone","Work Phone","Email");

        $datafields["Permissions"] = array("Overnight", "Driving", "Boston", "MayDriveToTown","MayGiveRides","SwimCompetent","SwimAllowed","BoatAllowed", "Comment");
        $datafields["Birthdays"] = array("BirthDate");
        $datafields["Parents"] = array("Name","Address","HomePhone","CellPhone","WorkPhone","Email");


        
        $columns = $headers = array();

        $columns['default'] = array('student', 'grade', 'gender', 'yearofgraduation', 'advisorname', 'dormname', 'room', 'actions');
        $columns['phones'] = array('student', 'studentcell', 'dormphone', 'actions');
        $columns['permissions'] = array('student', 'overnight', 'driving', 'boston', 'maydrive', 'maygiverides', 'swimcompetent', 'swimallowed', 'boatallowed', 'comment', 'actions');
        $columns['birthdays'] = array('student', 'birthdate', 'actions');
        $columns['parents'] = array('student', 'name', 'address','homephone', 'cellphone', 'workphone', 'email', 'actions');

        foreach($columns[$filter->type] as $item){
            $header[] = get_string($item, 'local_mxschool');
        }

        $this->define_columns($columns[$filter->type]);
        $this->define_headers($header);

        if($filter->type == 'parents'){
            $fields = "p.id as dcid, ms.id, CONCAT(u.lastname, ' ', u.firstname) as student";
            $srch = array();
            foreach($columns[$filter->type] as $item){
                if($item == 'student' || $item == 'actions')
                    continue;
                else{
                    $fields .= ", p.$item as $item";
                    $srch[] = " p.$item LIKE '%$search%' ";
                }
            }

            $sql_search = ($search)?" AND (u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%' OR ms.preferred LIKE '%$search%' OR ".implode('OR',$srch).") ":'';

            $from = "{local_mxschool_students} ms
                    LEFT JOIN {user} u ON u.id = ms.userid
                    LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation = ms.dorm
                    LEFT JOIN {local_mxschool_parents} p ON p.childid=ms.id
                ";
        }else{
            $fields = "ms.id, CONCAT(u.lastname, ' ', u.firstname) as student";
            $srch = array();
            foreach($columns[$filter->type] as $item){
                if($item == 'student' || $item == 'actions')
                    continue;
                elseif($item == 'dormname'){
                    $fields .= ", d.name as $item";
                    $srch[] = " d.name LIKE '%$search%' ";
                }
                elseif($item == 'advisorname'){
                    $fields .= ", a.advisorname as $item";
                    $srch[] = " a.advisorname LIKE '%$search%' ";
                }
                else{
                    $fields .= ", ms.$item as $item";
                    $srch[] = " ms.$item LIKE '%$search%' ";
                }
            }
            $sql_search = ($search)?" AND (u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%' OR ms.preferred LIKE '%$search%' OR ".implode('OR',$srch).") ":'';

            $from = "{local_mxschool_students} ms
                    LEFT JOIN {user} u ON u.id = ms.userid
                    LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation = ms.dorm
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisorname FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a ON a.id = ms.advisor
                ";
        }

        $where_dorm = "";
        if(!empty($filter->dorm)){
            $where_dorm = "ms.dorm='$filter->dorm' AND ";    
        }
        $where = $where_dorm.' u.id > 0 AND u.deleted = 0'.$sql_search;
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }
    
    function col_boatallowed($values) {
      return ($values->boatallowed)?get_string('yes'):get_string('no');
    }
    function col_swimallowed($values) {
      return ($values->swimallowed)?get_string('yes'):get_string('no');
    }
    function col_swimcompetent($values) {
      return ($values->swimcompetent)?get_string('yes'):get_string('no');
    }
    function col_maygiverides($values) {
      return ($values->maygiverides)?get_string('yes'):get_string('no');
    }
    function col_maydrive($values) {
      return ($values->maydrive)?get_string('yes'):get_string('no');
    }
    function col_actions($values) {
      global $OUTPUT, $PAGE;

        if ($this->is_downloading()){
            return '';
        }

      $strdelete  = get_string('delete');
      $stredit  = get_string('edit');
      $strpreview  = get_string('preview', 'local_mxschool');

        $edit = array();

        $aurl = new moodle_url('/local/mxschool/user_management/student_edit.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/user_management/students.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));

      return implode('', $edit);
    }
}
