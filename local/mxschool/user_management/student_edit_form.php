<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $id             = $this->_customdata['id'];
        $student        = $this->_customdata['student'];
        $user           = $this->_customdata['user'];
        $dorms          = $this->_customdata['dorms'];
        $advisors       = $this->_customdata['advisors'];
        
        $systemcontext   = context_system::instance();
        
        $this->id  = $id;
        $this->context = $systemcontext;
        
        $mform->addElement('hidden', 'id', $id);
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('hidden', 'userid', $user->id);
        $mform->setType('userid', PARAM_INT);

        $mform->addElement('text', 'firstname', 'First name', 'maxlength="254"  size="50"');
        $mform->addRule('firstname', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('firstname', PARAM_TEXT);
        if (isset($user->firstname)){
            $mform->setDefault('firstname', $user->firstname);
        }
        
        $mform->addElement('text', 'lastname', 'Last name', 'maxlength="254"  size="50"');
        $mform->addRule('lastname', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('lastname', PARAM_TEXT);
        if (isset($user->lastname)){
            $mform->setDefault('lastname', $user->lastname);
        }
        
        $mform->addElement('text', 'middle', 'Middle name', 'maxlength="254"  size="50"');
        $mform->setType('middle', PARAM_TEXT);
        
        $mform->addElement('text', 'email', 'Email', 'maxlength="254"  size="50"');
        $mform->addRule('email', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('email', PARAM_TEXT);
        if (isset($user->email)){
            $mform->setDefault('email', $user->email);
        }
        
        $mform->addElement('text', 'preferred', 'Preferred', 'maxlength="254"  size="50"');
        $mform->setType('preferred', PARAM_TEXT);
        
        $mform->addElement('select', 'grade', 'Grade', array(9=>9, 10=>10, 11=>11, 12=>12));
        $mform->setType('grade', PARAM_INT);
        
        $mform->addElement('text', 'yearofgraduation', 'Year of Graduation', 'maxlength="254"  size="5"');
        $mform->setType('yearofgraduation', PARAM_TEXT);
        
        $mform->addElement('select', 'gender', 'Gender', array('M'=>'M', 'F'=>'F'));
        $mform->setType('gender', PARAM_RAW);
        
        $mform->addElement('select', 'dorm', 'Dorm', $dorms);
        $mform->setType('dorm', PARAM_RAW);
        
        $mform->addElement('text', 'room', 'Room', 'maxlength="254"  size="5"');
        $mform->setType('room', PARAM_TEXT);
        
        $mform->addElement('date_selector', 'birthdate', 'BirthDate', 'maxlength="254"  size="10"');
        $mform->setType('birthdate', PARAM_TEXT);
        if ($student->birthdate and $student->birthdate != ''){
            $mform->setDefault('birthdate', strtotime($student->birthdate));
            unset($student->birthdate);
        }
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'overnight', '', 'Parent', 'Parent');
        $radioarray[] = $mform->createElement('radio', 'overnight', '', 'Host', 'Host');
        $mform->addGroup($radioarray, 'overnightarray', 'Overnight', array(' '), false);
        if ($student->overnight != ''){
            $mform->setDefault('overnight', $student->overnight);
        }
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'driving', '', 'Parent permission', 'Parent permission');
        $radioarray[] = $mform->createElement('radio', 'driving', '', 'Over 21 driver', 'Over 21 driver');
        $radioarray[] = $mform->createElement('radio', 'driving', '', 'Any driver', 'Any driver');
        $radioarray[] = $mform->createElement('radio', 'driving', '', 'Specific drivers (Enter in comments)', 'Specific drivers (Enter in comments)');
        $mform->addGroup($radioarray, 'drivingarray', 'Driving', array(' '), false);
        if ($student->driving != ''){
            $mform->setDefault('driving', $student->driving);
        }
        
        $mform->addElement('textarea', 'comment', 'Comment', 'cols="50"');
        $mform->setType('comment', PARAM_TEXT);
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'boston', '', 'Yes', 'Yes');
        $radioarray[] = $mform->createElement('radio', 'boston', '', 'Parent', 'Parent');
        $radioarray[] = $mform->createElement('radio', 'boston', '', 'No', 'No');
        $mform->addGroup($radioarray, 'bostonarray', 'Boston', array(' '), false);
        if ($student->boston != ''){
            $mform->setDefault('boston', $student->boston);
        }
        
        $mform->addElement('select', 'advisor', 'Advisor', $advisors);
        $mform->setType('advisor', PARAM_INT);
        if ($student->advisor > 0){
            $mform->setDefault('advisor', $student->advisor);
        }
        
        $mform->addElement('text', 'dormphone', 'DormPhone', 'maxlength="254"  size="50"');
        $mform->setType('dormphone', PARAM_TEXT);
        
        $mform->addElement('text', 'studentcell', 'StudentCell', 'maxlength="254"  size="50"');
        $mform->setType('studentcell', PARAM_TEXT);
        
        $mform->addElement('selectyesno', 'maydrive', 'May Drive to Town');
        $mform->addElement('selectyesno', 'maygiverides', 'May Give Rides');
        $mform->addElement('selectyesno', 'swimcompetent', 'Swim Competent');
        $mform->addElement('selectyesno', 'swimallowed', 'Swim Competent');
        $mform->addElement('selectyesno', 'swimallowed', 'Swim Allowed');
        $mform->addElement('selectyesno', 'boatallowed', 'Boat Allowed ');
        
        $this->add_action_buttons(get_string('cancel'), get_string('update'));

        // Finally set the current form data
        $this->set_data($student);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB, $CFG;
        
        $errors = parent::validation($data, $files);

        $data    = (object)$data;
        $user    = $DB->get_record('user', array('id'=>$data->userid));

        // validate email
        if (!validate_email($data->email)) {
            $errors['email'] = get_string('invalidemail');
        } else if (($data->email !== $user->email) and $DB->record_exists('user', array('email'=>$data->email, 'mnethostid'=>$CFG->mnet_localhost_id))) {
            $errors['email'] = get_string('emailexists');
        }

        return $errors;
    }
}

