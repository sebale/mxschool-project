<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;

    /**
     * Form definition.
     */
    function definition() {
        $mform          = $this->_form;
        $id             = $this->_customdata['id'];
        $faculty        = $this->_customdata['faculty'];
        $user           = $this->_customdata['user'];
        $dorms          = $this->_customdata['dorms'];

        $systemcontext   = context_system::instance();

        $this->id  = $id;
        $this->context = $systemcontext;

        $mform->addElement('hidden', 'id', $id);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'userid', $user->id);
        $mform->setType('userid', PARAM_INT);

        $mform->addElement('text', 'firstname', 'First name', 'maxlength="254"  size="50"');
        $mform->addRule('firstname', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('firstname', PARAM_TEXT);
        if (isset($user->firstname)){
            $mform->setDefault('firstname', $user->firstname);
        }

        $mform->addElement('text', 'lastname', 'Last name', 'maxlength="254"  size="50"');
        $mform->addRule('lastname', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('lastname', PARAM_TEXT);
        if (isset($user->lastname)){
            $mform->setDefault('lastname', $user->lastname);
        }


        $mform->addElement('text', 'email', 'Email', 'maxlength="254"  size="50"');
        $mform->addRule('email', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('email', PARAM_TEXT);
        if (isset($user->email)){
            $mform->setDefault('email', $user->email);
        }

        $mform->addElement('text', 'phone1', 'Phone', 'maxlength="254"  size="50"');
        $mform->setType('phone1', PARAM_TEXT);
        if (isset($user->phone1)){
            $mform->setDefault('phone1', $user->phone1);
        }

        $mform->addElement('select', 'dorm', 'Dorm', $dorms);
        $mform->setType('dorm', PARAM_RAW);



        $facultytypes=array();
        $facultytypes[0]="";
        $facultytypes[1]="Head of House";
        $facultytypes[2]="Lives In Dorm";
        $facultytypes[3]="Lives Out Of Dorm";

        $mform->addElement('select', 'type', 'Type', $facultytypes);
        $mform->setType('type', PARAM_RAW);

        $this->add_action_buttons(get_string('cancel'), get_string('update'));

        // Finally set the current form data
        $this->set_data($faculty);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB, $CFG;

        $errors = parent::validation($data, $files);

        $data    = (object)$data;
        $user    = $DB->get_record('user', array('id'=>$data->userid));

        // validate email
        if (!validate_email($data->email)) {
            $errors['email'] = get_string('invalidemail');
        } else if (($data->email !== $user->email) and $DB->record_exists('user', array('email'=>$data->email, 'mnethostid'=>$CFG->mnet_localhost_id))) {
            $errors['email'] = get_string('emailexists');
        }

        return $errors;
    }
}

