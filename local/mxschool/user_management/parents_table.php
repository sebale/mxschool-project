<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class parents_table extends table_sql {

    
    function __construct($uniqueid, $search, $filter) {
		global $CFG, $USER, $DB;

        parent::__construct($uniqueid);
        
        $columns = array('student', 'name', 'email', 'address', 'homephone', 'cellphone', 'workphone', 'actions');
        $headers = array(
            get_string('student', 'local_mxschool'),
            get_string('parent_name', 'local_mxschool'),
            get_string('email'),
            get_string('address'),
            get_string('homephone', 'local_mxschool'),
            get_string('cellphone', 'local_mxschool'),
            get_string('workphone', 'local_mxschool'),
            get_string('actions', 'local_mxschool')
        );

        $this->define_columns($columns);
        $this->define_headers($headers);
        
        $sql_search = ($search) ? " AND (mp.name LIKE '%$search%' OR mp.email LIKE '%$search%' OR mp.address LIKE '%$search%' OR mp.homephone LIKE '%$search%' OR mp.cellphone LIKE '%$search%' OR mp.workphone LIKE '%$search%' OR u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%')" : "";
        
        $fields = "mp.id, mp.userid, mp.name, mp.email, mp.address, mp.homephone, mp.cellphone, mp.workphone,  CONCAT(u.firstname, ' ', u.lastname) as student ";
        $from = "{local_mxschool_parents} mp
                    LEFT JOIN {local_mxschool_students} ms ON ms.id = mp.childid
                    LEFT JOIN {user} u ON u.id = ms.userid
                ";
        $where = 'u.id > 0 AND u.deleted = 0'.$sql_search;
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/mxschool/user_management/parents.php?search=".$search);
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
      $strdelete  = get_string('delete');
      $stredit  = get_string('edit');
      $strpreview  = get_string('preview', 'local_mxschool');
      
        $edit = array();
        
        $aurl = new moodle_url('/local/mxschool/user_management/parent_edit.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/user_management/parents.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));
        
      return implode('', $edit);
    }
}
