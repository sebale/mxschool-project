<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Kaltura version file.
 *
 * @package    local_kaltura_announcements
 * @author     KALTURA
 * @copyright  2016 KALTURA, kaltura.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('dorm_edit_form.php');
require_once('../lib.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:manage_users', $systemcontext);

$id = optional_param('id', 0, PARAM_INT); // Dorm id.

$dorm = null;
if ($id){
    $dorm = $DB->get_record('local_mxschool_dorms', array('id'=>$id));
}

$title = ($id > 0) ? get_string('edit_dorm', 'local_mxschool').' '.$dorm->name : get_string('create_dorm', 'local_mxschool');
$PAGE->set_url(new moodle_url("/local/mxschool/user_management/dorm_edit.php", array('id'=>$id)));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('user_management', 'local_mxschool'), new moodle_url('/local/mxschool/user_management/index.php'));
$PAGE->navbar->add(get_string('manage_dorms', 'local_mxschool'), new moodle_url('/local/mxschool/user_management/dorms.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$args = array(
    'id'         => $id,
    'dorm'       => $dorm
);
$editform = new edit_form(null, $args);

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/mxschool/user_management/dorms.php'));
} else if ($data = $editform->get_data()) {
    // Process data if submitted.
    if ($data->id > 0){
        $DB->update_record('local_mxschool_dorms', $data);
    } else {
        $newDorm = new stdClass();
        $newDorm->name = $data->name;
        $newDorm->abbreviation = $data->abbreviation;
        $newDorm->gender = $data->gender;
        $newDorm->type = $data->type;
        $DB->insert_record('local_mxschool_dorms', $newDorm);
    }
    
    $jAlert->create(array('type'=>'success', 'text'=>'Dorm was successfully saved'));
    redirect(new moodle_url('/local/mxschool/user_management/dorms.php'));
}


echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$editform->display();

echo $OUTPUT->footer();
