<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Kaltura version file.
 *
 * @package    local_kaltura_announcements
 * @author     KALTURA
 * @copyright  2016 KALTURA, kaltura.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('student_edit_form.php');
require_once('../lib.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:manage_users', $systemcontext);

$id = required_param('id', PARAM_INT); // Student id.

$user = null;
$student = $DB->get_record('local_mxschool_students', array('id'=>$id));
if ($student){
    $user = $DB->get_record('user', array('id'=>$student->userid));
}

$title = get_string('edit_student', 'local_mxschool').' '.fullname($user);
$PAGE->set_url(new moodle_url("/local/mxschool/user_management/student_edit.php", array('id'=>$id)));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('user_management', 'local_mxschool'), new moodle_url('/local/mxschool/user_management/index.php'));
$PAGE->navbar->add(get_string('manage_students', 'local_mxschool'), new moodle_url('/local/mxschool/user_management/students.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$args = array(
    'id'         => $id,
    'student'    => $student,
    'user'       => $user,
    'dorms'      => get_dorms_list(),
    'advisors'   => get_advisors_list()
);
$editform = new edit_form(null, $args);

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/mxschool/user_management/students.php'));
} else if ($data = $editform->get_data()) {
    // Process data if submitted.
    $student = $data;
    $student->birthdate = ($student->birthdate > 0) ? date('m/d/Y', $student->birthdate) : '';
    
    $DB->update_record('local_mxschool_students', $student);
    
    $user = $DB->get_record('user', array('id'=>$data->userid));
    if ($user){
        $user->firstname = $data->firstname;
        $user->lastname = $data->lastname;
        $user->email = $data->email;
        $DB->update_record('user', $user);
    }
    
    $select_advisor = $DB->get_record('local_mxschool_advisors', array('studentid'=>$data->id));
    if ($select_advisor and $select_advisor->currentadvisor != $data->advisor){
        $select_advisor->currentadvisor = $data->advisor;
        $DB->update_record('local_mxschool_advisors', $select_advisor);
    }
    
    $jAlert->create(array('type'=>'success', 'text'=>'Student was successfully updated'));
    redirect(new moodle_url('/local/mxschool/user_management/students.php'));
}


echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$editform->display();

echo $OUTPUT->footer();
