<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('students_table.php');
require ('../lib.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$search     = optional_param('search', '', PARAM_RAW);
$filter     = (object)optional_param_array('filter', '', PARAM_RAW);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:manage_users', $systemcontext);

$title = get_string('manage_students', 'local_mxschool');

if ($action == 'delete' and $id) {
    $student = $DB->get_record('local_mxschool_students', array('id'=>$id));
    if ($student){
        $user = $DB->get_record('user', array('id'=>$student->userid));
        if ($user){
            delete_user($user);       
        }
    }    
    $DB->delete_records('local_mxschool_advisors', array('studentid'=>$student->id));
    $DB->delete_records('local_mxschool_parents', array('childid'=>$student->id));
    $DB->delete_records('local_mxschool_students', array('id'=>$student->id));
    $jAlert->create(array('type'=>'success', 'text'=>'Student was successfully deleted'));
    redirect(new moodle_url("/local/mxschool/user_management/students.php"));
}

$user_roles = get_config('local_mxschool', 'user_roles');
$user_roles = (empty($user_roles))?new stdClass():json_decode($user_roles);

$PAGE->set_url(new moodle_url("/local/mxschool/user_management/students.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('user_management', 'local_mxschool'), new moodle_url('/local/mxschool/user_management/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->add_body_class('print-table');
$PAGE->add_body_class('manage-students');

$table = new students_table('students_table', $search, $filter);
$table->is_collapsible = false;
$table->sortable(true, 'student');
$types = array("phones"=>"Phone #s", "permissions"=>"Permissions", "birthdays"=>"Birthdays", "parents"=>"Parents");

echo $OUTPUT->header();
echo $OUTPUT->heading($title);
echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'mxschool-search-form'));
echo html_writer::select(get_dorms_list(), 'filter[dorm]', array($filter->dorm), array('' => 'Select Dorm'));
echo html_writer::select($types, 'filter[type]', array($filter->type), array('default' => 'Select Type'));
echo html_writer::start_tag("label",  array());
echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search').' ...', 'value' => $search));
echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('search')));
if(isset($user_roles->student_role))
    echo html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('assign_students', 'local_mxschool'), 'onclick'=>'location="'.$CFG->wwwroot.'/admin/roles/assign.php?contextid=1&roleid='.$user_roles->student_role.'"'));
echo html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('create_user', 'local_mxschool'), 'onclick'=>'location="'.$CFG->wwwroot.'/user/editadvanced.php?id=-1"'));
if (has_capability('local/mxschool:import_students', $systemcontext)){
    echo html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('importusers', 'local_mxschool'), 'onclick'=>'location="'.$CFG->wwwroot.'/local/mxschool/user_management/importuser/index.php"'));
}
echo html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('print', 'local_mxschool'), 'id'=>'print-table'));
echo html_writer::end_tag("label");
echo html_writer::end_tag("form");

echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
$table->out(20, true);
echo html_writer::end_tag("div");

echo $OUTPUT->footer();

?>

<script>
    jQuery('.adv-select').change(function(){
        var adv = jQuery(this).val();
        var rid = jQuery(this).attr('id');
        jQuery(this).parent().find('.clone-select-box').hide();
        if (adv == '0'){
            jQuery(this).parent().find('.clone-select-box').show();
        } else {
            jQuery.ajax({
              url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/advisor_selection/ajax.php",
              type: 'POST',
              dataType: 'json',
              data: {
                        'rid' :rid,
                        'adv' :adv,
                        'action' :'set_final_advisor'
                    },
              success: function(data) {
              }
            });
        }
    });
    
    jQuery('.other-adv-select').change(function(){
        var adv = jQuery(this).val();
        var rid = jQuery(this).attr('id');
        jQuery.ajax({
          url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/advisor_selection/ajax.php",
          type: 'POST',
          dataType: 'json',
          data: {
                    'rid' :rid,
                    'adv' :adv,
                    'action' :'set_final_advisor_other'
                },
          success: function(data) {
          }
        });
    });
    
    $('#print-table').click(function (e) {
        window.print();
        return false;
    });
    
</script>

<?php
