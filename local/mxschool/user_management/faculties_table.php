<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class faculties_table extends table_sql {
    protected $page;

    
    function __construct($uniqueid, $search, $filter,$download,$page) {
		global $CFG, $USER, $DB;

        $this->is_downloading($download, 'faculties', 'Faculties');

        parent::__construct($uniqueid);

        $columns = array('firstname', 'lastname', 'type', 'dormname', 'available');
        $headers = array(
            get_string('firstname', 'local_mxschool'),
            get_string('lastname', 'local_mxschool'),
            get_string('type', 'local_mxschool'),
            get_string('dorm', 'local_mxschool'),
            get_string('available', 'local_mxschool')
        );

        if ($this->is_downloading()){
            $columns[] = 'phone1';
            $headers[] = 'Phone';
        }else{
            $columns[] = 'actions';
            $headers[] = get_string('actions', 'local_mxschool');
        }

        $this->define_columns($columns);
        $this->define_headers($headers);
        
        $sql_search = ($search) ? " AND (u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%' OR d.name)" : "";
        
        $fields = "mf.id, mf.userid, mf.type, mf.available, u.firstname, u.lastname, d.name as dormname, u.phone1, '' as actions ";
        $from = "{local_mxschool_faculty} mf
                    LEFT JOIN {user} u ON u.id = mf.userid
                    LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation = mf.dorm
                ";
        $where = 'u.id > 0 AND u.deleted = 0'.$sql_search;
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/mxschool/user_management/faculties.php?search=".$search."&page=".$page);
        $this->page = $page;
    }
    
    function col_type($values) {
      $output = '';
      $types = array(1=>'Head of House', 2=>'Lives In Dorm', 3=>'Lives Out Of Dorm');  
      
      if (isset($types[$values->type])){
          $output = $types[$values->type];
      }
        
      return $output;
    }
    
    function col_available($values) {
      $output = '';
      
      if ($values->available > 0){
          $output = get_string('available', 'local_mxschool');
      } else {
          $output = get_string('notavailable', 'local_mxschool');
      }
        
      return $output;
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
      $strdelete  = get_string('delete');
      $stredit  = get_string('edit');
      $strpreview  = get_string('preview', 'local_mxschool');
      $enable  = get_string('enable', 'local_mxschool');
      $disable  = get_string('disable', 'local_mxschool');
      
        $edit = array();
        
        if ($values->available > 0){
            $aurl = new moodle_url('/local/mxschool/user_management/faculties.php', array('action'=>'hide', 'id'=>$values->id,'page'=>$this->page));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', $disable, 'core', array('class' => 'iconsmall')));
        } else {
            $aurl = new moodle_url('/local/mxschool/user_management/faculties.php', array('action'=>'show', 'id'=>$values->id,'page'=>$this->page));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/show', $enable, 'core', array('class' => 'iconsmall')));
        }
        
        $aurl = new moodle_url('/local/mxschool/user_management/faculty_edit.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/user_management/faculties.php', array('action'=>'delete', 'id'=>$values->id,'page'=>$this->page));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));
        
      return implode('', $edit);
    }
}
