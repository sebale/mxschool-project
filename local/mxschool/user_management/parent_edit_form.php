<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $id             = $this->_customdata['id'];
        $parent         = $this->_customdata['parent'];
        $students       = $this->_customdata['students'];
        
        $systemcontext   = context_system::instance();
        
        $this->id  = $id;
        $this->context = $systemcontext;
        
        $mform->addElement('hidden', 'id', $id);
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('select', 'childid', 'Student', $students);
        $mform->addRule('childid', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('childid', PARAM_INT);
        
        $mform->addElement('text', 'name', 'Name', 'maxlength="254"  size="50"');
        $mform->addRule('name', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);
        
        $mform->addElement('text', 'email', 'Email', 'maxlength="254"  size="50"');
        $mform->addRule('email', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('email', PARAM_TEXT);
        
        $mform->addElement('text', 'address', 'Address', 'maxlength="254"  size="50"');
        $mform->setType('address', PARAM_TEXT);
        
        $mform->addElement('text', 'homephone', 'Home phone', 'maxlength="254"  size="50"');
        $mform->setType('homephone', PARAM_TEXT);
        
        $mform->addElement('text', 'cellphone', 'Cell phone', 'maxlength="254"  size="50"');
        $mform->setType('cellphone', PARAM_TEXT);
        
        $mform->addElement('text', 'workphone', 'Work phone', 'maxlength="254"  size="50"');
        $mform->setType('workphone', PARAM_TEXT);
        
        $mform->addElement('text', 'childid', 'Student', 'maxlength="254"  size="50"');
        $mform->setType('workphone', PARAM_TEXT);
        
        $this->add_action_buttons(get_string('cancel'), ($id > 0) ? get_string('update') : get_string('save', 'local_mxschool'));

        // Finally set the current form data
        $this->set_data($parent);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB, $CFG;
        
        $errors = parent::validation($data, $files);

        $data    = (object)$data;
        $user    = $DB->get_record('user', array('id'=>$data->userid));

        // validate email
        if (!validate_email($data->email)) {
            $errors['email'] = get_string('invalidemail');
        } else if (($data->email !== $user->email) and $DB->record_exists('user', array('email'=>$data->email, 'mnethostid'=>$CFG->mnet_localhost_id))) {
            $errors['email'] = get_string('emailexists');
        }

        return $errors;
    }
}

