<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $id             = $this->_customdata['id'];
        $dorm           = $this->_customdata['dorm'];
        
        $systemcontext   = context_system::instance();
        
        $this->id  = $id;
        $this->context = $systemcontext;
        
        $mform->addElement('hidden', 'id', $id);
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('text', 'name', 'Name', 'maxlength="254"  size="50"');
        $mform->addRule('name', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);
        
        $mform->addElement('text', 'abbreviation', 'Abbreviation', 'maxlength="254"  size="50"');
        $mform->addRule('abbreviation', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('abbreviation', PARAM_TEXT);
        
        $mform->addElement('select', 'gender', 'Gender', array('M'=>'M', 'F'=>'F', 'all'=>'All'));
        $mform->setType('gender', PARAM_RAW);
        
        $mform->addElement('select', 'type', 'Type', array('day'=>'Day', 'boarding'=>'Boarding', 'all'=>'All'));
        $mform->setType('type', PARAM_RAW);
        
        $this->add_action_buttons(get_string('cancel'), ($id > 0) ? get_string('update') : get_string('save', 'local_mxschool'));

        // Finally set the current form data
        $this->set_data($dorm);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB, $CFG;
        
        $errors = parent::validation($data, $files);

        $data    = (object)$data;
        if ($data->id > 0){
            $dorm = $DB->get_record('local_mxschool_dorms', array('abbreviation'=>$data->abbreviation));
            if (isset($dorm->id) and $data->id != $dorm->id) {
                $errors['abbreviation'] = get_string('dormexists', 'local_mxschool');
            }
        } else {
            if ($DB->record_exists('local_mxschool_dorms', array('abbreviation'=>$data->abbreviation))) {
                $errors['abbreviation'] = get_string('dormexists', 'local_mxschool');
            }    
        }
        
        return $errors;
    }
}

