jQuery(document).ready(function(){
   
    if (jQuery('#id_type').val() == 'course'){
        jQuery('#fitem_id_courseid').show();
    }
    
    jQuery('#id_type').change(function(e){
       if (jQuery(this).val() == 'course') {
           jQuery('#fitem_id_courseid').show();
       } else {
           jQuery('#fitem_id_courseid').hide();
       }
    });
    
    jQuery('#id_announcementdate').parent().addClass('input-append datetimepicker');
    jQuery('#id_announcementdate').attr('data-format', 'MM/dd/yyyy HH:mm PP');
    jQuery('#id_announcementdate').parent().append('<span class=\"add-on\"><span class=\"fa fa-calendar\"></span></span>');
    jQuery('#id_announcementdate').parent().datetimepicker({
      language: 'en',
      useSeconds: false,
      pick12HourFormat: true,
    });
    jQuery('#id_announcementdate').click(function(e){
        jQuery('#id_announcementdate').next().trigger('click');
    });
    
    jQuery('#id_startdate').parent().addClass('input-append datetimepicker');
    jQuery('#id_startdate').attr('data-format', 'MM/dd/yyyy HH:mm PP');
    jQuery('#id_startdate').parent().append('<span class=\"add-on\"><span class=\"fa fa-calendar\"></span></span>');
    jQuery('#id_startdate').parent().datetimepicker({
      language: 'en',
      useSeconds: false,
      pick12HourFormat: true,
    });
    jQuery('#id_startdate').click(function(e){
        jQuery('#id_startdate').next().trigger('click');
    });
    
    jQuery('#id_enddate').parent().addClass('input-append datetimepicker');
    jQuery('#id_enddate').attr('data-format', 'MM/dd/yyyy HH:mm PP');
    jQuery('#id_enddate').parent().append('<span class=\"add-on\"><span class=\"fa fa-calendar\"></span></span>');
    jQuery('#id_enddate').parent().datetimepicker({
      language: 'en',
      useSeconds: false,
      pick12HourFormat: true,
    });
    jQuery('#id_enddate').click(function(e){
        jQuery('#id_enddate').next().trigger('click');
    });
    
});

