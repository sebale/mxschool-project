<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class announcements_table extends table_sql {
    function __construct($uniqueid, $search) {
		global $CFG, $USER;

        parent::__construct($uniqueid);

        $columns = array('title', 'type', 'coursename', 'announcementdate', 'startdate', 'enddate', 'created', 'actions');
        $this->define_columns($columns);
        $headers = array(
            get_string('title', 'local_mxschool'),
            get_string('type', 'local_mxschool'),
            get_string('course', 'local_mxschool'),
            get_string('announcementdate', 'local_mxschool'),
            get_string('startdate', 'local_mxschool'),
            get_string('enddate', 'local_mxschool'),
            get_string('created', 'local_mxschool'),
            get_string('actions', 'local_mxschool'));
       
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (a.title LIKE '%$search%' OR a.description LIKE '%$search%' OR a.type LIKE '%$search%' OR u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%' OR c.fullname LIKE '%$search%')" : "";
        $fields = "a.id, a.title, a.type, a.courseid, a.announcementdate, a.startdate, a.enddate, a.timecreated, CONCAT(u.firstname, ' ', u.lastname) as created, a.state, c.fullname as coursename ";
        $from = "{local_mxschool_announcements} a 
                     LEFT JOIN {user} u ON u.id = a.userid
                     LEFT JOIN {course} c ON c.id = a.courseid";
        $where = 'a.id > 0'.$sql_search;

        if (!is_siteadmin()){
            $where .= ' AND a.userid = '.$USER->id.' ';
        }
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/mxschool/announcements/index.php");
    }
    
    function col_type($values) {
       return ucfirst($values->type);
    }
    function col_announcementdate($values) {
       return ($values->announcementdate) ? date("m/d/Y h:i", $values->announcementdate) : '-';
    }
    function col_startdate($values) {
       return ($values->startdate) ? date("m/d/Y h:i", $values->startdate) : '-';
    }
    function col_enddate($values) {
       return ($values->enddate) ? date("m/d/Y h:i", $values->enddate) : '-';
    }
    function col_timecreated($values) {
       return ($values->timecreated) ? date("m/d/Y", $values->timecreated) : '-';
    }
    function col_coursename($values) {
        global $CFG;
        $output = '';
        if ($values->courseid > 0 and $values->coursename != ''){
            $output = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$values->courseid.'">'.$values->coursename.'</a>';
        }
       return $output;
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
      $strdelete  = get_string('delete');
      $stredit  = get_string('edit');
      $strshow  = get_string('show');
      $strhide  = get_string('hide');

        $edit = array();
        $aurl = new moodle_url('/local/mxschool/announcements/edit.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/announcements/index.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));
        
        if ($values->state > 0){
            $aurl = new moodle_url('/local/mxschool/announcements/index.php', array('action'=>'hide', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', $strhide, 'core', array('class' => 'iconsmall')));
        } else {
            $aurl = new moodle_url('/local/mxschool/announcements/index.php', array('action'=>'show', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/show', $strshow, 'core', array('class' => 'iconsmall')));    
        }
        
      return implode('', $edit);
    }
}
