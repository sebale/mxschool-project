<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $type           = $this->_customdata['type'];
        $record         = $this->_customdata['record'];
        $sitetypes      = $this->_customdata['sitetypes'];
        
        $systemcontext   = context_system::instance();
        $this->context = $systemcontext;
        
        $mform->addElement('hidden', 'id');
        $mform->addElement('hidden', 'type', ((isset($record->type)) ? $record->type : $type));
        
        $mform->addElement('select', 'sitetype', get_string('sitetype', 'local_mxschool'), $sitetypes);
        $mform->setType('sitetype', PARAM_RAW);
        
        $mform->addElement('text', 'site', get_string('site', 'local_mxschool'), array('style'=>'width:40%'));
        $mform->addRule('site', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $this->add_action_buttons(get_string('cancel'), get_string('save', 'local_mxschool'));
        
        // Finally set the current form data
        $this->set_data($record);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        return $errors;
    }
}

