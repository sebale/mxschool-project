<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $settings       = $this->_customdata['settings'];
        
        $systemcontext   = context_system::instance();
        $this->context = $systemcontext;
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'vacation_form_enable', '', get_string('thanksgiving_vacation_date', 'local_mxschool').((isset($settings['thanksgiving_vacation_date']) and $settings['thanksgiving_vacation_date'] > 0) ? ' ('.date('m/d/Y', $settings['thanksgiving_vacation_date']).')' : ''), 'thanksgiving_vacation_date');
        $radioarray[] = $mform->createElement('radio', 'vacation_form_enable', '', get_string('december_vacation_date', 'local_mxschool').((isset($settings['december_vacation_date']) and $settings['december_vacation_date'] > 0) ? ' ('.date('m/d/Y', $settings['december_vacation_date']).')' : ''), 'december_vacation_date');
        $radioarray[] = $mform->createElement('radio', 'vacation_form_enable', '', get_string('march_vacation_date', 'local_mxschool').((isset($settings['march_vacation_date']) and $settings['march_vacation_date'] > 0) ? ' ('.date('m/d/Y', $settings['march_vacation_date']).')' : ''), 'march_vacation_date');
        $radioarray[] = $mform->createElement('radio', 'vacation_form_enable', '', get_string('summer_vacation_date', 'local_mxschool').((isset($settings['summer_vacation_date']) and $settings['summer_vacation_date'] > 0) ? ' ('.date('m/d/Y', $settings['summer_vacation_date']).')' : ''), 'summer_vacation_date');
        
        $mform->addGroup($radioarray, 'radioar', get_string('advisor_form_enable', 'local_mxschool'), array(''), false);
        $mform->addRule('radioar', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $options = array(''=>'----------', '-1 day'=>'1 day', '-2 days'=>'2 days', '-3 days'=>'3 days', '-4 days'=>'4 days', '-5 days'=>'5 days', '-6 days'=>'6 days', '-1 week'=>'1 week', '-2 weeks'=>'2 weeks', '-3 weeks'=>'4 weeks' );
        $mform->addElement('select', 'vacation_form_time_prior', get_string('time_prior', 'local_mxschool'), $options);
        $mform->addRule('vacation_form_time_prior', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $this->add_action_buttons(get_string('cancel'), get_string('save', 'local_mxschool'));
        
        // Finally set the current form data
        $this->set_data($settings);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        return $errors;
    }
}

