<?php
define('AJAX_SCRIPT', true);
require_once('../../../config.php');

require_login();

$id		  = optional_param('id', 0, PARAM_INT);
$type	  = optional_param('type', 0, PARAM_INT);
$field	  = optional_param('field', '', PARAM_RAW);
$data	  = optional_param('data', '', PARAM_RAW);
$action = optional_param('action', '', PARAM_RAW);
$form	= (object)clean_param_array($_POST['form'], PARAM_RAW, true);

if($action == 'get_students_dorm' and $id){
    $dorm = '';
    
    $student = $DB->get_record("local_mxschool_students", array('id'=>$id));
    if (isset($student->dorm) and $student->dorm != '') {
        $sdorm = $DB->get_record("local_mxschool_dorms", array('abbreviation'=>$student->dorm));
        if ($sdorm){
            $dorm = $sdorm->name;
        }
    }
    
    echo json_encode(array(
       'dorm' => $dorm
    ));
} elseif($action == 'set_data' and $id and $field != ''){
    
    $record = $DB->get_record('local_mxschool_transport', array('id'=>$id));
    if ($record){
        $record->$field = $data;   
        if ($field == 'pickup_time_depart_other' or $field == 'pickup_time_return_other'){
            $record->$field = strtotime($data);    
        }
        $record->timemodified = time();
        $DB->update_record('local_mxschool_transport', $record);
    }
    echo '1';
}
exit;