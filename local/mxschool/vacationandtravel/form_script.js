$(document).ready(function(){
    
    processForm();
    
    jQuery('#id_depart_date_time').parent().addClass('input-append datetimepicker');
    jQuery('#id_depart_date_time').attr('data-format', 'MM/dd/yyyy HH:mm PP');
    jQuery('#id_depart_date_time').parent().append('<span class=\"add-on\"><span class=\"fa fa-calendar\"></span></span>');
    jQuery('#id_depart_date_time').parent().datetimepicker({
      language: 'en',
      useSeconds: false,
      pick12HourFormat: true,
    });
    jQuery('#id_depart_date_time').click(function(e){
        jQuery('#id_depart_date_time').next().trigger('click');
    });
    
    jQuery('#id_return_date_time').parent().addClass('input-append datetimepicker');
    jQuery('#id_return_date_time').attr('data-format', 'MM/dd/yyyy HH:mm PP');
    jQuery('#id_return_date_time').parent().append('<span class=\"add-on\"><span class=\"fa fa-calendar\"></span></span>');
    jQuery('#id_return_date_time').parent().datetimepicker({
      language: 'en',
      useSeconds: false,
      pick12HourFormat: true,
    });
    jQuery('#id_return_date_time').click(function(e){
        jQuery('#id_return_date_time').next().trigger('click');
    });
    
    jQuery('#id_studentid').change(function(){
        var id = jQuery(this).val();
        if (id > 0){
            $.ajax({
              url: "ajax.php",
              type: 'POST',
              dataType: 'json',
              data: {
                        'id' :id,
                        'action' :'get_students_dorm'
                    },
              success: function(data) {
                var dorm = data.dorm;
                if (dorm != ''){
                    $('#fitem_id_dorm').show();
                    $('#id_dorm').val(dorm);
                } else {
                    $('#fitem_id_dorm').hide();   
                    $('#id_dorm').val('');
                }
              }
            });
        } else {
            $('#fitem_id_dorm').hide();
            $('#id_dorm').val('');
        }
        processForm();
    });
    
    jQuery('.fitem').addClass('required');
    jQuery('#id_destination').change(function(){
        processForm();
    });
    jQuery('#id_stu_cellphone').change(function(){
        processForm();
    });
    
    // derarture
    jQuery('#id_depart_transportation_no').change(function(){
        jQuery('#id_depart_transport_type_1').prop('checked', true);
        processForm();
    });
    jQuery('#id_depart_transportation_yes').change(function(){
        jQuery('#id_depart_transport_type_1').removeAttr('checked');
        processForm();
    });
    jQuery('#fgroup_id_depart_tr_type input[type="radio"]').change(function(){
        jQuery('#fgroup_id_depart_tr_type input[type="radio"]').removeAttr('checked');
        jQuery(this).prop('checked', true);
        processForm();
    });
    jQuery('#fgroup_id_depart_airport_array input[type="radio"]').change(function(){
            $('#fitem_id_depart_carrier').show();
            $('#fitem_id_depart_flight').show();
            $('#fgroup_id_depart_customs_array').show();
    });
    jQuery('#id_depart_bus_0').change(function(){
        processForm();
    });
    jQuery('#id_depart_train_0').change(function(){
        processForm();
    });
    jQuery('#id_depart_driver').change(function(){
        processForm();
    });
    jQuery('#id_depart_carrier').change(function(){
        processForm();
    });
    jQuery('#fgroup_id_depart_customs_array input[type="radio"]').change(function(){
        processForm();
    });
    jQuery('#fgroup_id_depart_train_array input[type="radio"]').change(function(){
        processForm();
    });
    jQuery('#id_depart_train_other').change(function(){
        processForm();
    });
    jQuery('#fgroup_id_depart_bus_array input[type="radio"]').change(function(){
        processForm();
    });
    jQuery('#id_depart_bus_other').change(function(){
        processForm();
    });
    jQuery('#fgroup_id_depart_nyc_stops_array input[type="radio"]').change(function(){
        processForm();
    });
    
    // return
    jQuery('#id_return_transportation_no').change(function(){
        jQuery('#id_return_transport_type_1').prop('checked', true);
        processForm();
    });
    jQuery('#id_return_transportation_yes').change(function(){
        jQuery('#id_return_transport_type_1').removeAttr('checked');
        processForm();
    });
    jQuery('#fgroup_id_return_tr_type input[type="radio"]').change(function(){
        jQuery('#fgroup_id_return_tr_type input[type="radio"]').removeAttr('checked');
        jQuery(this).prop('checked', true);
        processForm();
    });
    jQuery('#fgroup_id_return_airport_array input[type="radio"]').change(function(){
            $('#fitem_id_return_carrier').show();
            $('#fitem_id_return_flight').show();
            $('#fgroup_id_return_customs_array').show();
    });
    jQuery('#id_return_bus_0').change(function(){
        processForm();
    });
    jQuery('#id_return_train_0').change(function(){
        processForm();
    });
    jQuery('#id_return_driver').change(function(){
        processForm();
    });
    jQuery('#id_return_carrier').change(function(){
        processForm();
    });
    jQuery('#fgroup_id_return_customs_array input[type="radio"]').change(function(){
        processForm();
    });
    jQuery('#fgroup_id_return_train_array input[type="radio"]').change(function(){
        processForm();
    });
    jQuery('#id_return_train_other').change(function(){
        processForm();
    });
    jQuery('#fgroup_id_return_bus_array input[type="radio"]').change(function(){
        processForm();
    });
    jQuery('#id_return_bus_other').change(function(){
        processForm();
    });
    jQuery('#fgroup_id_return_nyc_stops_array input[type="radio"]').change(function(){
        processForm();
    });
    /*jQuery('#fitem_id_depart_date_time select, #fitem_id_return_date_time select').change(function(){
        processForm();
    });*/
    
    function processForm(){
        
        $('#fitem_id_dorm').hide();
        if ($('#id_studentid').val() > 0){
            $('#fitem_id_dorm').show();
        }

        // derarture
        if ($('#id_depart_transportation_yes').prop('checked') || $('#id_depart_transportation_no').prop('checked')){
            $('#fgroup_id_depart_tr_type').show();
            if ($('#id_depart_transportation_yes').prop('checked')){
                $('#id_depart_transport_type_1').parent().hide();
                //$('#id_depart_transport_type_2').parent().show();
                //$('#id_depart_transport_type_3').parent().show();
                //$('#id_depart_transport_type_4').parent().show();
                $('#id_depart_transport_type_5').parent().show();
                $('#id_depart_transport_type_4 + label span').hide();
            } else if ($('#id_depart_transportation_no').prop('checked')){
                $('#id_depart_transport_type_1').parent().show();
                //$('#id_depart_transport_type_2').parent().hide();
                //$('#id_depart_transport_type_3').parent().hide();
                //$('#id_depart_transport_type_4').parent().hide();
                $('#id_depart_transport_type_5').parent().hide();
                $('#id_depart_transport_type_4 + label span').show();
            }
        } else {
            $('#fgroup_id_depart_tr_type').hide();
        }

        if($('#id_depart_transport_type_1').prop('checked')){
            $('#fitem_id_depart_driver').show();
        } else {
            $('#fitem_id_depart_driver').hide();
        }

        if($('#id_depart_transport_type_2').prop('checked') && $('#id_depart_transportation_yes').prop('checked')){
            $('#fgroup_id_depart_airport_array').show();
            if ($('#fgroup_id_depart_airport_array input[type="radio"]').length == 1){
                $('#fgroup_id_depart_airport_array input[type="radio"]:first-child').trigger('click');
            }
        } else {
            $('#fgroup_id_depart_airport_array').hide();
        }

        if(($('#fgroup_id_depart_airport_array input[type="radio"]').prop('checked') && $('#id_depart_transport_type_2').prop('checked')) || ($('#id_depart_transport_type_2').prop('checked') && $('#id_depart_transportation_no').prop('checked'))){
            $('#fitem_id_depart_carrier').show();
            $('#fitem_id_depart_flight').show();
            $('#fgroup_id_depart_customs_array').show();
        } else {
            $('#fitem_id_depart_carrier').hide();
            $('#fitem_id_depart_flight').hide();
            $('#fgroup_id_depart_customs_array').hide();
        }

        if($('#id_depart_transport_type_3').prop('checked') && $('#id_depart_transportation_yes').prop('checked')){
            if ($('#fgroup_id_depart_train_array input[type="radio"]').length == 1){
                $('#fgroup_id_depart_train_array input[type="radio"]:first-child').trigger('click');
            }
            $('#fgroup_id_depart_train_array').show();
        } else {
            $('#fgroup_id_depart_train_array').hide();
        }

        if($('#id_depart_transport_type_3').prop('checked') && $('#id_depart_train_0').prop('checked')){
            $('#fitem_id_depart_train_other').show();
        } else {
            $('#fitem_id_depart_train_other').hide();
        }
        
        if($('#id_depart_transport_type_4').prop('checked') && $('#id_depart_transportation_yes').prop('checked')){
            if ($('#fgroup_id_depart_bus_array input[type="radio"]').length == 1){
                $('#fgroup_id_depart_bus_array input[type="radio"]:first-child').trigger('click');
            }
            $('#fgroup_id_depart_bus_array').show();
        } else {
            $('#fgroup_id_depart_bus_array').hide();
        }
        
        if($('#id_depart_transport_type_4').prop('checked') && $('#id_depart_bus_0').prop('checked')){
            $('#fitem_id_depart_bus_other').show();
        } else {
            $('#fitem_id_depart_bus_other').hide();
        }
        
        if($('#id_depart_transport_type_5').prop('checked')){
            if ($('#fgroup_id_depart_nyc_stops_array input[type="radio"]').length == 1){
                $('#fgroup_id_depart_nyc_stops_array input[type="radio"]:first-child').trigger('click');
            }
            $('#fgroup_id_depart_nyc_stops_array').show();
        } else {
            $('#fgroup_id_depart_nyc_stops_array').hide();
        }
        
        // return fieldset
        if ($('#id_return_transportation_yes').prop('checked') || $('#id_return_transportation_no').prop('checked')){
            $('#fgroup_id_return_tr_type').show();
            if ($('#id_return_transportation_yes').prop('checked')){
                $('#id_return_transport_type_1').parent().hide();
                //$('#id_return_transport_type_2').parent().show();
                //$('#id_return_transport_type_3').parent().show();
                //$('#id_return_transport_type_4').parent().show();
                $('#id_return_transport_type_5').parent().show();
                $('#id_return_transport_type_4 + label span').hide();
            } else if ($('#id_return_transportation_no').prop('checked')){
                $('#id_return_transport_type_1').parent().show();
                //$('#id_return_transport_type_2').parent().hide();
                //$('#id_return_transport_type_3').parent().hide();
                //$('#id_return_transport_type_4').parent().hide();
                $('#id_return_transport_type_5').parent().hide();
                $('#id_return_transport_type_4 + label span').show();
            }
        } else {
            $('#fgroup_id_return_tr_type').hide();
        }

        if($('#id_return_transport_type_1').prop('checked')){
            $('#fitem_id_return_driver').show();
        } else {
            $('#fitem_id_return_driver').hide();
        }

        if($('#id_return_transport_type_2').prop('checked') && $('#id_return_transportation_yes').prop('checked')){
            if ($('#fgroup_id_return_airport_array input[type="radio"]').length == 1){
                $('#fgroup_id_return_airport_array input[type="radio"]:first-child').trigger('click');
            }
            $('#fgroup_id_return_airport_array').show();
        } else {
            $('#fgroup_id_return_airport_array').hide();
        }

        if(($('#fgroup_id_return_airport_array input[type="radio"]').prop('checked') && $('#id_return_transport_type_2').prop('checked')) || ($('#id_return_transport_type_2').prop('checked') && $('#id_return_transportation_no').prop('checked'))){
            $('#fitem_id_return_carrier').show();
            $('#fitem_id_return_flight').show();
            $('#fgroup_id_return_customs_array').show();
        } else {
            $('#fitem_id_return_carrier').hide();
            $('#fitem_id_return_flight').hide();
            $('#fgroup_id_return_customs_array').hide();
        }

        if($('#id_return_transport_type_3').prop('checked') && $('#id_return_transportation_yes').prop('checked')){
            if ($('#fgroup_id_return_train_array input[type="radio"]').length == 1){
                $('#fgroup_id_return_train_array input[type="radio"]:first-child').trigger('click');
            }
            $('#fgroup_id_return_train_array').show();
        } else {
            $('#fgroup_id_return_train_array').hide();
        }

        if($('#id_return_transport_type_3').prop('checked') && $('#id_return_train_0').prop('checked')){
            $('#fitem_id_return_train_other').show();
        } else {
            $('#fitem_id_return_train_other').hide();
        }
        
        if($('#id_return_transport_type_4').prop('checked') && $('#id_return_transportation_yes').prop('checked')){
            if ($('#fgroup_id_return_bus_array input[type="radio"]').length == 1){
                $('#fgroup_id_return_bus_array input[type="radio"]:first-child').trigger('click');
            }
            $('#fgroup_id_return_bus_array').show();
        } else {
            $('#fgroup_id_return_bus_array').hide();
        }
        
        if($('#id_return_transport_type_4').prop('checked') && $('#id_return_bus_0').prop('checked')){
            $('#fitem_id_return_bus_other').show();
        } else {
            $('#fitem_id_return_bus_other').hide();
        }
        
        if($('#id_return_transport_type_5').prop('checked')){
            if ($('#fgroup_id_return_nyc_stops_array input[type="radio"]').length == 1){
                $('#fgroup_id_return_nyc_stops_array input[type="radio"]:first-child').trigger('click');
            }
            $('#fgroup_id_return_nyc_stops_array').show();
        } else {
            $('#fgroup_id_return_nyc_stops_array').hide();
        }
        
        validateForm();
    }
    
    function validateForm(){
        $('#id_submitbutton').prop('disabled', true);
        var error = false;
        
        if ($('#id_studentid').val() == '') error = true;
        if ($('#id_destination').val() == '') error = true;
        if ($('#id_stu_cellphone').val() == '') error = true;
        
        // departure
        if (!$('#id_depart_transportation_yes').prop('checked') && !$('#id_depart_transportation_no').prop('checked')) error = true;
        if ($('#id_depart_transportation_no').prop('checked')){
            if ($('#id_depart_transport_type_1').prop('checked')){
                if ($('#id_depart_driver').val() == ''){
                    error = true;
                }
            } else if ($('#id_depart_transport_type_2').prop('checked')){
                if ($('#id_depart_carrier').val() == '' || $('#id_depart_flight').val() == '' || (!$('#id_depart_customs_1').prop('checked') && !$('#id_depart_customs_2').prop('checked'))) error = true;
            } else if ($('#id_depart_transport_type_3').prop('checked')){
            } else if ($('#id_depart_transport_type_4').prop('checked')){
            } else if ($('#id_depart_transport_type_5').prop('checked')){
            } else {
                error = true;
            }
        }
        if ($('#id_depart_transportation_yes').prop('checked')){
            if ($('#id_depart_transport_type_2').prop('checked')){
                if ($('#id_depart_carrier').val() == '' || $('#id_depart_flight').val() == '' || (!$('#id_depart_customs_1').prop('checked') && !$('#id_depart_customs_2').prop('checked')) || !$('input[name="depart_airport"]:checked').val()) error = true;
            } else if ($('#id_depart_transport_type_3').prop('checked')){
                if (!$('input[name="depart_train"]:checked').val()){
                    error = true;
                } else if ($('#id_depart_train_0').prop('checked') && $('#id_depart_train_other').val() == ''){
                    error = true;
                }
            } else if ($('#id_depart_transport_type_4').prop('checked')){
                if (!$('input[name="depart_bus"]:checked').val()){
                    error = true;
                } else if ($('#id_depart_bus_0').prop('checked') && $('#id_depart_bus_other').val() == ''){
                    error = true;
                }
            } else if ($('#id_depart_transport_type_5').prop('checked')){
                if (!$('input[name="depart_nyc_stops"]:checked').val()){
                    error = true;
                }
            } else {
                error = true;
            }
        }
        
        // return
        if (!$('#id_return_transportation_yes').prop('checked') && !$('#id_return_transportation_no').prop('checked')) error = true;
        if ($('#id_return_transportation_no').prop('checked')){
            if ($('#id_return_transport_type_1').prop('checked')){
                if ($('#id_return_driver').val() == ''){
                    error = true;
                }
            } else if ($('#id_return_transport_type_2').prop('checked')){
                if ($('#id_return_carrier').val() == '' || $('#id_return_flight').val() == '' || (!$('#id_return_customs_1').prop('checked') && !$('#id_return_customs_2').prop('checked'))) error = true;
            } else if ($('#id_return_transport_type_3').prop('checked')){
            } else if ($('#id_return_transport_type_4').prop('checked')){
            } else if ($('#id_return_transport_type_5').prop('checked')){
            } else {
                error = true;
            }
        }
        if ($('#id_return_transportation_yes').prop('checked')){
            if ($('#id_return_transport_type_2').prop('checked')){
                if ($('#id_return_carrier').val() == '' || $('#id_return_flight').val() == '' || (!$('#id_return_customs_1').prop('checked') && !$('#id_return_customs_2').prop('checked')) || !$('input[name="return_airport"]:checked').val()) error = true;
            } else if ($('#id_return_transport_type_3').prop('checked')){
                if (!$('input[name="return_train"]:checked').val()){
                    error = true;
                } else if ($('#id_return_train_0').prop('checked') && $('#id_return_train_other').val() == ''){
                    error = true;
                }
            } else if ($('#id_return_transport_type_4').prop('checked')){
                if (!$('input[name="return_bus"]:checked').val()){
                    error = true;
                } else if ($('#id_return_bus_0').prop('checked') && $('#id_return_bus_other').val() == ''){
                    error = true;
                }
            } else if ($('#id_return_transport_type_5').prop('checked')){
                if (!$('input[name="return_nyc_stops"]:checked').val()){
                    error = true;
                }
            } else {
                error = true;
            }
        }
        
        /*var depart_date = parseInt($('#id_depart_date_time_day').val()) + parseInt($('#id_depart_date_time_month').val()) + parseInt($('#id_depart_date_time_year').val()) + parseInt($('#id_depart_date_time_hour').val()) + parseInt($('#id_depart_date_time_minute').val());
        
        var return_date = parseInt($('#id_return_date_time_day').val()) + parseInt($('#id_return_date_time_month').val()) + parseInt($('#id_return_date_time_year').val()) + parseInt($('#id_return_date_time_hour').val()) + parseInt($('#id_return_date_time_minute').val());
        
        if (depart_date >= return_date){
            error = true;
        }*/
        
        if (!error){
            $('#id_submitbutton').removeAttr('disabled');
        }
        
    }
});