<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('records_table.php');
require ('../lib.php');

$search     = optional_param('search', '', PARAM_RAW);
$filter     = optional_param_array('filter', '', PARAM_RAW);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);
$download   = optional_param('download', '', PARAM_ALPHA);

require_login();

$multiple_vacations = get_config('local_mxschool', 'multiple_vacations');
$student = $DB->get_record('local_mxschool_students', array('userid'=>$USER->id));
$records_exists = array();
if ($student){
    $records_exists = $DB->get_records('local_mxschool_transport', array('studentid'=>$student->id));
}

$title = get_string('vacationandtravel', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/vacationandtravel/records.php", array()));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new records_table('table');
$table->is_collapsible = false;

require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");
echo $OUTPUT->header();
echo $OUTPUT->heading($title);

if ($multiple_vacations > 0 or (!$multiple_vacations and count($records_exists) == 0)){
    echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'mxschool-search-form'));
        echo html_writer::start_tag("label",  array());
        echo html_writer::empty_tag('input', array('type' => 'button', 'value' => 'Create new Record', 'onclick'=>'location="'.$CFG->wwwroot.'/local/mxschool/vacationandtravel/sform.php"'));
        echo html_writer::end_tag("label");
    echo html_writer::end_tag("form");
}
echo html_writer::start_tag('div', array('class' => 'mxschool-table-box transportion-table'));

$table->out(20, true);

echo html_writer::end_tag("div");
echo $OUTPUT->footer();
