<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class records_table extends table_sql {
    
    public $_transport_types = array(''=>'', '1'=>'Car', '2'=>'Plane', '3'=>'Train', '4'=>'Bus', '5'=>'NYC Direct');
    public $_sites = array();
    
    function __construct($uniqueid) {
		global $CFG, $USER, $DB;

        parent::__construct($uniqueid);
        
        $this->set_sites();
        
        $columns = array('destination', 'stu_cellphone', 'depart_date_time', 'depart_transport_type', 'depart_driver', 'depart_details', 'return_date_time', 'return_transport_type', 'return_driver', 'return_details');
        $headers = array(
            get_string('destination', 'local_mxschool'),
            get_string('stu_cellphone', 'local_mxschool'),
            'Departure Date & Time',
            get_string('transportation_type', 'local_mxschool'),
            'Airport / Station',
            'Details',
            'Return Date & Time',
            get_string('transportation_type', 'local_mxschool'),
            'Airport / Station',
            'Details'
        );
        
        $columns[] = 'actions';
        $headers[] = 'Actions';
        $this->no_sorting('actions');
        
        $this->no_sorting('depart_driver');
        $this->no_sorting('depart_details');
        $this->no_sorting('return_driver');
        $this->no_sorting('return_details');

        $this->define_columns($columns);
        $this->define_headers($headers);
        
        $fields = "t.*, CONCAT(u.firstname, ' ', u.lastname) as student";
        $from = "{local_mxschool_transport} t
                    LEFT JOIN {local_mxschool_students} s ON s.id = t.studentid
                    LEFT JOIN {user} u ON u.id = s.userid";
        
        $where = 't.id > 0 AND u.deleted = 0 AND u.id = '.$USER->id;
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }
    
    function col_depart_date_time($values) {
        if ($this->is_downloading()){
            $date = date('m/d/Y h:i A', $values->depart_date_time);
        } else {
            $date = date('m/d/Y', $values->depart_date_time);
            $date .= '<br />'.date('h:i A', $values->depart_date_time);   
        }
      return $date;
    }
    
    function col_return_date_time($values) {
        if ($this->is_downloading()){
            $date = date('m/d/Y h:i A', $values->return_date_time);
        } else {
            $date = date('m/d/Y', $values->return_date_time);
            $date .= '<br />'.date('h:i A', $values->return_date_time);   
        }
      return $date;
    }
    
    function col_depart_transport_type($values) {
      return $this->_transport_types[$values->depart_transport_type];
    }
    
    function col_return_transport_type($values) {
      return $this->_transport_types[$values->return_transport_type];
    }
    
    function col_depart_driver($values) {
        $data = '-';
        if ($values->depart_transport_type == '2' and isset($this->_sites[$values->depart_airport])){
            $data = $this->_sites[$values->depart_airport];
        } elseif ($values->depart_transport_type == '3'){
            if ($values->depart_train != '0' and isset($this->_sites[$values->depart_train])){
                $data = $this->_sites[$values->depart_train];
            } elseif ($values->depart_train == '0'){
                $data = 'Other';
            }
        } elseif ($values->depart_transport_type == '4'){
            if ($values->depart_bus != '0' and isset($this->_sites[$values->depart_bus])){
                $data = $this->_sites[$values->depart_bus];
            } elseif ($values->depart_bus == '0'){
                $data = 'Other';
            }
        }
      return $data;
    }
    
    function col_return_driver($values) {
        $data = '-';
        if ($values->return_transport_type == '2' and isset($this->_sites[$values->return_airport])){
            $data = $this->_sites[$values->return_airport];
        } elseif ($values->return_transport_type == '3'){
            if ($values->return_train != '0' and isset($this->_sites[$values->return_train])){
                $data = $this->_sites[$values->return_train];
            } elseif ($values->return_train == '0'){
                $data = 'Other';
            }
        } elseif ($values->return_transport_type == '4'){
            if ($values->return_bus != '0' and isset($this->_sites[$values->return_bus])){
                $data = $this->_sites[$values->return_bus];
            } elseif ($values->return_bus == '0'){
                $data = 'Other';
            }
        }
      return $data;
    }
    
    function col_depart_details($values) {
        $data = '-';
        if ($values->depart_transport_type == '1'){
            if ($this->is_downloading()){
                $data = 'Driver(s): '.$values->depart_driver;   
            } else {
                $data = '<div class="tr_details"><label>Driver(s):</label>'.$values->depart_driver.'</div>';
            }
        } elseif ($values->depart_transport_type == '2'){
            if ($this->is_downloading()){
                $data = 'Carrier: '.$values->depart_carrier;   
                $data .= '; Flight #: '.$values->depart_flight;   
                $data .= '; Customs?: '.(($values->depart_customs == 1) ? 'Yes' : 'No');   
            } else {
                $data = '<div class="tr_details"><label>Carrier:</label>'.$values->depart_carrier.'</div>';
                $data .= '<div class="tr_details"><label>Flight #:</label>'.$values->depart_flight.'</div>';
                $data .= '<div class="tr_details"><label>Customs?:</label>'.(($values->depart_customs == 1) ? 'Yes' : 'No').'</div>';
            }
        } elseif ($values->depart_transport_type == '3'){
            if ($values->depart_train_other != ''){
                if ($this->is_downloading()){
                    $data = $values->depart_train_other;
                } else {
                    $data = '<div class="tr_details">'.$values->depart_train_other.'</div>';
                }
            }
        } elseif ($values->depart_transport_type == '4'){
            if ($values->depart_bus_other != ''){
                if ($this->is_downloading()){
                    $data = $values->depart_bus_other;
                } else {
                    $data = '<div class="tr_details">'.$values->depart_bus_other.'</div>';
                }
            }
        } elseif ($values->depart_transport_type == '5'){
            if (isset($this->_sites[$values->depart_nyc_stops])){
                if ($this->is_downloading()){
                    $data = 'NYC Stop: '.$this->_sites[$values->depart_nyc_stops];
                } else {
                    $data = '<div class="tr_details"><label>NYC Stop: </label>'.$this->_sites[$values->depart_nyc_stops].'</div>';
                }
            }
        }
      return $data;
    }
    
    function col_return_details($values) {
        $data = '-';
        if ($values->return_transport_type == '1'){
            if ($this->is_downloading()){
                $data = 'Driver(s): '.$values->return_driver;   
            } else {
                $data = '<div class="tr_details"><label>Driver(s):</label>'.$values->return_driver.'</div>';
            }
        } elseif ($values->return_transport_type == '2'){
            if ($this->is_downloading()){
                $data = 'Carrier: '.$values->return_carrier;   
                $data .= '; Flight #: '.$values->return_flight;   
                $data .= '; Customs?: '.(($values->return_customs == 1) ? 'Yes' : 'No');   
            } else {
                $data = '<div class="tr_details"><label>Carrier:</label>'.$values->return_carrier.'</div>';
                $data .= '<div class="tr_details"><label>Flight #:</label>'.$values->return_flight.'</div>';
                $data .= '<div class="tr_details"><label>Customs?:</label>'.(($values->return_customs == 1) ? 'Yes' : 'No').'</div>';
            }
        } elseif ($values->return_transport_type == '3'){
            if ($values->return_train_other != ''){
                if ($this->is_downloading()){
                    $data = $values->return_train_other;
                } else {
                    $data = '<div class="tr_details">'.$values->return_train_other.'</div>';
                }
            }
        } elseif ($values->return_transport_type == '4'){
            if ($values->return_bus_other != ''){
                if ($this->is_downloading()){
                    $data = $values->return_bus_other;
                } else {
                    $data = '<div class="tr_details">'.$values->return_bus_other.'</div>';
                }
            }
        } elseif ($values->return_transport_type == '5'){
            if (isset($this->_sites[$values->return_nyc_stops])){
                if ($this->is_downloading()){
                    $data = 'NYC Stop: '.$this->_sites[$values->return_nyc_stops];
                } else {
                    $data = '<div class="tr_details"><label>NYC Stop: </label>'.$this->_sites[$values->return_nyc_stops].'</div>';
                }
            }
        }
      return $data;
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
        $strdelete  = get_string('delete');
        $stredit  = get_string('edit');
      
        $edit = array();
        
        if ($values->confirmed_depart != 1 and $values->confirmed_return != 1){
            $aurl = new moodle_url('/local/mxschool/vacationandtravel/sform.php', array('id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));
        }

      return implode('', $edit);
    }
    
    function set_sites(){
        global $DB;
        $data = array();
        $sites = $DB->get_records("local_mxschool_pickup_sites");
        if (count($sites) > 0){
            foreach ($sites as $site){
                $data[$site->id] = $site->site;
            }
            $this->_sites = $data;
        }
    }
    
}
