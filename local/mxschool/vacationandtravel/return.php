<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('return_table.php');
require ('../lib.php');

$search     = optional_param('search', '', PARAM_RAW);
$filter     = optional_param_array('filter', '', PARAM_RAW);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);
$download   = optional_param('download', '', PARAM_ALPHA);

require_login();
require_capability('local/mxschool:vacation_manage', context_system::instance());

if ($action == 'delete' and $id){
    $record = $DB->get_record('local_mxschool_transport', array('id'=>$id));
    if ($record){
        $DB->delete_records('local_mxschool_transport', array('id'=>$record->id));
    }
    
    require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");
    $jAlert->create(array('type'=>'success', 'text'=>'Record was successfully deleted'));
    redirect(new moodle_url('/local/mxschool/vacationandtravel/return.php'));
} elseif ($action == 'send'){
    require('../classes/notifications.php');
    require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");
    
    $items = $DB->get_records_sql("SELECT t.*, u.email, s.userid, CONCAT(u.firstname, ' ', u.lastname) as student, pt.datetime as ptime, ps.site as pickup_site
                                    FROM {local_mxschool_transport} t
                                        LEFT JOIN {local_mxschool_students} s ON s.id = t.studentid
                                        LEFT JOIN {user} u ON u.id = s.userid
                                        LEFT JOIN {local_mxschool_pickup_times} pt ON pt.id = t.pickup_time_return 
                                        LEFT JOIN {local_mxschool_pickup_sites} ps ON ps.id = t.pickup_site_return 
                                    WHERE t.id > 0 AND u.deleted = 0 AND t.confirmed_return = 1 AND t.email_sendable_return > 0 AND (t.email_sent_return IS NULL OR t.email_sent_return = 0)");
    if (count($items) > 0){
        $msg = new mxNotifications(5);
        foreach ($items as $item){
            $params = array();
            $params['item'] = $item;
            $msg->rebuild(5, $params);
            $msg->process();
            $record = $DB->get_record('local_mxschool_transport', array('id'=>$item->id));
            if ($record){
                $record->email_sent_return = 1;
                $DB->update_record('local_mxschool_transport', $record);
            }          
        }
        $jAlert->create(array('type'=>'success', 'text'=>'Notifications was successfully sent'));
    }
    
    redirect(new moodle_url("/local/mxschool/vacationandtravel/return.php"));
}

$title = get_string('view_return', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/vacationandtravel/return.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('vacationandtravel', 'local_mxschool'), new moodle_url('/local/mxschool/vacationandtravel/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/bootstrap/bootstrap-datetimepicker.min.js', true);
$PAGE->requires->js('/local/mxschool/assets/js/bootstrap/collapse.js', true);
$PAGE->requires->js('/local/mxschool/assets/js/bootstrap/scrollspy.js', true);
$PAGE->requires->css('/local/mxschool/assets/css/bootstrap-datetimepicker.min.css', true);
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new return_table('return_table', $search, $filter, $download);
$table->is_collapsible = false;
$table->is_downloading($download, 'Return_records_'.date('m_d_Y'));

if (!$table->is_downloading()) {
    require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");
    echo $OUTPUT->header();
    echo $OUTPUT->heading($title);
    echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'mxschool-search-form'));
    echo html_writer::start_tag("label", array('style'=>'margin-bottom:-10px;display:block;'));
    echo html_writer::tag("span", 'Dates between ');
        echo html_writer::start_tag("div", array('class'=>'input-append datetimepicker'));
        echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'filter[start]', 'value' => $filter['start'], 'class' => 'date-time', 'data-format'=>'MM/dd/yyyy HH:mm PP'));
        echo html_writer::start_tag("span", array('class'=>'add-on'));
        echo html_writer::tag("span", '', array('class'=>'fa fa-calendar'));
        echo html_writer::end_tag("span");
        echo html_writer::end_tag("div");
    echo html_writer::tag("span", 'and ');
        echo html_writer::start_tag("div", array('class'=>'input-append datetimepicker'));
        echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'filter[end]', 'value' => $filter['end'], 'class' => 'date-time', 'data-format'=>'MM/dd/yyyy HH:mm PP'));
        echo html_writer::start_tag("span", array('class'=>'add-on'));
        echo html_writer::tag("span", '', array('class'=>'fa fa-calendar'));
        echo html_writer::end_tag("span");
        echo html_writer::end_tag("div");
        echo html_writer::select(array(''=>'Show all', 'pending'=>'Pending', 'confirmed'=>'Confirmed'), 'filter[status]', array($filter['status']), array('default' => ''));
    echo html_writer::end_tag("label");
    echo html_writer::empty_tag("br");
    echo html_writer::start_tag("label",  array());
    echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search').' ...', 'value' => $search));
    echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('search')));
    echo html_writer::empty_tag('input', array('type' => 'button', 'value' => 'Clear Filter', 'onclick'=>'location="'.$CFG->wwwroot.'/local/mxschool/vacationandtravel/return.php"'));
    echo html_writer::empty_tag('input', array('type' => 'button', 'value' => 'Confirm & Email', 'onclick'=>'confirmForm();'));
    echo html_writer::end_tag("label");
    echo html_writer::end_tag("form");

    echo html_writer::start_tag('div', array('class' => 'mxschool-table-box transportion-table'));
}
$table->out(20, true);

if (!$table->is_downloading()) {
    echo html_writer::end_tag("div");
    echo $OUTPUT->footer();

?>

<script>
    function removeSaved(){
        setTimeout(function () {
          $('span.saved').fadeOut(function() {
            $(this).remove();
          });
        }, 2000);   
    }
    jQuery('.trans-type-select').change(function(){
        var transtype = jQuery(this).val();
        var dataid    = jQuery(this).attr('data-id');
        var el = jQuery(this);
        jQuery.ajax({
          url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/vacationandtravel/ajax.php",
          type: 'POST',
          dataType: 'json',
          data: {
                    'id' :dataid,
                    'field' :'trans_type_return',
                    'data' :transtype,
                    'action' :'set_data'
                },
          success: function(data) {
              el.after('<span class="saved">Saved</span>');
              removeSaved();
          }
        });
    });
    jQuery('.pickup-site-return').change(function(){
        var site = jQuery(this).val();
        var dataid    = jQuery(this).attr('data-id');
        var el = jQuery(this);
        jQuery.ajax({
          url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/vacationandtravel/ajax.php",
          type: 'POST',
          dataType: 'json',
          data: {
                    'id' :dataid,
                    'field' :'pickup_site_return',
                    'data' :site,
                    'action' :'set_data'
                },
          success: function(data) {
            el.after('<span class="saved">Saved</span>');
            removeSaved();
          }
        });
    });
    
    jQuery('.pickup-time-return').change(function(){
        var times = jQuery(this).val();
        var el = jQuery(this).parent();
        var dataid    = jQuery(this).attr('data-id');
        if (times == '0'){
            jQuery('#pickup_time_other_box_'+dataid).show();
        } else{
            jQuery('#pickup_time_other_box_'+dataid).hide();
        }
        jQuery.ajax({
          url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/vacationandtravel/ajax.php",
          type: 'POST',
          dataType: 'json',
          data: {
                    'id' :dataid,
                    'field' :'pickup_time_return',
                    'data' :times,
                    'action' :'set_data'
                },
          success: function(data) {
              el.append('<span class="saved">Saved</span>');
              removeSaved();
          }
        });
    });
    
    jQuery('.pickup-time-other-box input[type="button"]').click(function(){
        var dataid  = jQuery(this).attr('data-id');
        var times   = jQuery('#pickup_time_other_'+dataid).val();
        var el = jQuery(this);
        jQuery.ajax({
          url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/vacationandtravel/ajax.php",
          type: 'POST',
          dataType: 'json',
          data: {
                    'id' :dataid,
                    'field' :'pickup_time_return_other',
                    'data' :times,
                    'action' :'set_data'
                },
          success: function(data) {
              el.after('<span class="saved">Saved</span>');
              removeSaved();
          }
        });
    });
    
    jQuery('.confirmed-return').click(function(){
        var dataid  = jQuery(this).parent().parent().attr('data-id');
        var value = jQuery(this).val();
        var el = jQuery(this).parent().parent();
        jQuery.ajax({
          url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/vacationandtravel/ajax.php",
          type: 'POST',
          dataType: 'json',
          data: {
                    'id' :dataid,
                    'field' :'confirmed_return',
                    'data' :value,
                    'action' :'set_data'
                },
          success: function(data) {
              el.append('<span class="saved">Saved</span>');
              removeSaved();
          }
        });
    });
    
    jQuery('.email-sendable-return').change(function(){
        var dataid  = jQuery(this).attr('data-id');
        var el = jQuery(this);
        if (jQuery(this).prop('checked')){
            var value = 1;    
        } else {
            var value = 0;
        }
        
        jQuery.ajax({
          url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/vacationandtravel/ajax.php",
          type: 'POST',
          dataType: 'json',
          data: {
                    'id' :dataid,
                    'field' :'email_sendable_return',
                    'data' :value,
                    'action' :'set_data'
                },
          success: function(data) {
            el.after('<span class="saved">Saved</span>');
            removeSaved();
          }
        });
    });
    
    jQuery(".datetimepicker").datetimepicker({
      language: "en",
      useSeconds: false,
      pick12HourFormat: true
    });
    jQuery(".date-time").click(function(e){
        jQuery(this).next().trigger('click');
    });
    
    function confirmForm(){
        if (confirm('Are you sure want to Confirm & Email all records?')){
            location = '<?php echo $CFG->wwwroot; ?>/local/mxschool/vacationandtravel/return.php?action=send'
        }
    }
    
</script>

<?php
}
