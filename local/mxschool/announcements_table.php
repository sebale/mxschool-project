<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Kaltura language file.
 *
 * @package    local_kaltura_announcements
 * @author     KALTURA
 * @copyright  2016 KALTURA, kaltura.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class announcements_table extends table_sql {
    function __construct($uniqueid, $search) {
		global $CFG, $USER;

        parent::__construct($uniqueid);

        $columns = array('title', 'announcementdate', 'startdate', 'enddate', 'created', 'actions');
        $this->define_columns($columns);
        $headers = array(
            get_string('title', 'local_kaltura_announcements'),
            get_string('announcementdate', 'local_kaltura_announcements'),
            get_string('startdate', 'local_kaltura_announcements'),
            get_string('enddate', 'local_kaltura_announcements'),
            get_string('created', 'local_kaltura_announcements'),
            get_string('actions', 'local_kaltura_announcements'));
       
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (a.title LIKE '%$search%' OR a.description LIKE '%$search%')" : "";
        $fields = "a.id, a.title, a.announcementdate, a.startdate, a.enddate, a.timecreated, CONCAT(u.firstname, ' ', u.lastname) as created, a.state ";
        $from = "{local_kaltura_announcements} a LEFT JOIN {user} u ON u.id = a.userid";
        $where = 'a.id > 0'.$sql_search;

        if (!is_siteadmin()){
            $where .= ' AND a.userid = '.$USER->id.' ';
        }
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/kaltura_announcements/index.php");
    }
    
    function col_announcementdate($values) {
       return ($values->announcementdate) ? date("m/d/Y h:i", $values->announcementdate) : '-';
    }
    function col_startdate($values) {
       return ($values->startdate) ? date("m/d/Y h:i", $values->startdate) : '-';
    }
    function col_enddate($values) {
       return ($values->enddate) ? date("m/d/Y h:i", $values->enddate) : '-';
    }
    function col_timecreated($values) {
       return ($values->timecreated) ? date("m/d/Y", $values->timecreated) : '-';
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
      $strdelete  = get_string('delete');
      $stredit  = get_string('edit');
      $strshow  = get_string('show');
      $strhide  = get_string('hide');

        $edit = array();
        $aurl = new moodle_url('/local/kaltura_announcements/edit.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/kaltura_announcements/index.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')));
        
        if ($values->state > 0){
            $aurl = new moodle_url('/local/kaltura_announcements/index.php', array('action'=>'hide', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', $strhide, 'core', array('class' => 'iconsmall')));
        } else {
            $aurl = new moodle_url('/local/kaltura_announcements/index.php', array('action'=>'show', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/show', $strshow, 'core', array('class' => 'iconsmall')));    
        }
        
      return implode('', $edit);
    }
}
