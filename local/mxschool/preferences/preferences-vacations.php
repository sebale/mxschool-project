<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require_once('preferences_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:settings', $systemcontext);

$title = get_string('preferences_vacations', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/preferences/preferences-vacations.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('preferences_name', 'local_mxschool'), new moodle_url('/local/mxschool/preferences/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$datesform = new vacations_dates_form(null);
if (!$datesform->is_cancelled() && $data = $datesform->get_data()) {

    set_config('thanksgiving_vacation_date', $data->thanksgiving_vacation_date, 'local_mxschool');
    set_config('december_vacation_date', $data->december_vacation_date, 'local_mxschool');
    set_config('march_vacation_date', $data->march_vacation_date, 'local_mxschool');
    set_config('summer_vacation_date', $data->summer_vacation_date, 'local_mxschool');

    $jAlert->create(array('type'=>'success', 'text'=>'Dates was successfully updated'));
    redirect(new moodle_url('/local/mxschool/preferences/index.php'));
}
echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$datesform->display();

echo $OUTPUT->footer();
