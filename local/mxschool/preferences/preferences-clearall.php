<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require_once('preferences_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:settings', $systemcontext);

$title = get_string('clear_all_records', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/preferences/preferences-clearall.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('preferences_name', 'local_mxschool'), new moodle_url('/local/mxschool/preferences/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$form = new clear_form(null);

if ($form->is_cancelled()) {
    redirect(new moodle_url('/local/mxschool/preferences/index.php'));
} else if ($data = $form->get_data()) {

    if (isset($data->clear_advisor)){
        $DB->delete_records('local_mxschool_advisors');
    }
    
     if (isset($data->clear_vacation)){
        $DB->delete_records('local_mxschool_transport');
    }
    
    if (isset($data->clear_rooming)){
        $DB->delete_records('local_mxschool_rooming');
    }
    
    if (isset($data->clear_announcements)){
        $DB->delete_records('local_mxschool_announcements');
    }

    if (isset($data->clear_peer_tutors_records)){
        $DB->delete_records('local_mxschool_tutors_sess');
    }

    if (isset($data->clear_weekend_records)){
        $DB->delete_records('local_mxschool_weekend');
    }

    if (isset($data->clear_esignout_records)){
        $DB->delete_records('local_mxschool_edriver');
        $DB->delete_records('local_mxschool_epassenger');
    }

    if (isset($data->clear_driving_records)){
        $DB->execute("DELETE FROM {local_mxschool_driving} WHERE date_time<=:time",array('time'=>time()));
    }

    if (isset($data->clear_subjects_courses_tutors_records)){
        $DB->delete_records('local_mxschool_tutors_sess');
        $DB->delete_records('local_mxschool_tutors_course');
        $DB->delete_records('local_mxschool_tutors_cat');
        $DB->execute("UPDATE {local_mxschool_tutors} set data = '[]'");
    }

    $jAlert->create(array('type'=>'success', 'text'=>'All data successfully deleted'));
    redirect(new moodle_url('/local/mxschool/preferences/index.php'));
}
echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$form->display();

echo $OUTPUT->footer();
