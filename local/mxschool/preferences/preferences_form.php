<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE, $DB;

        $mform          = $this->_form;

        $records = $DB->get_records('local_mxschool_dates');
        $dates  = array();
        foreach($records as $date){
            $dates[$date->date] = $date->weekendtype;
        }
        
        for($date = strtotime('saturday', get_config('local_mxschool', 'dorms_open')); $date <= get_config('local_mxschool', 'dorms_close'); $date = strtotime("+1 week", $date))
        {
            $str_date = date('m/d/Y',$date);
            $radioarray=array();
            $radioarray[] = $mform->createElement('radio', $str_date, '', get_string('open','local_mxschool'), 'open');
            $radioarray[] = $mform->createElement('radio', $str_date, '', get_string('closed','local_mxschool'), 'campus');
            $radioarray[] = $mform->createElement('radio', $str_date, '', get_string('free','local_mxschool'), 'free');
            $radioarray[] = $mform->createElement('radio', $str_date, '', get_string('vacation','local_mxschool'), 'vacation');
            $mform->addGroup($radioarray, 'radioar', $str_date, array(' '), false);
            if(isset($dates[$str_date]))
                $mform->setDefault($str_date, $dates[$str_date]);
        }

        $this->add_action_buttons(false, get_string('update'));

        // Finally set the current form data
        //$this->set_data($student);
    }
}

class dates_form extends moodleform {

    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;

        $mform->addElement('date_selector', 'dorms_open', get_string('dorms_open_on','local_mxschool'));
        $mform->setDefault('dorms_open', get_config('local_mxschool', 'dorms_open'));
        
        $mform->addElement('date_selector', 'second_semester_starts', get_string('second_semester_starts','local_mxschool'));
        $mform->setDefault('second_semester_starts', get_config('local_mxschool', 'second_semester_starts'));
        
        $mform->addElement('date_selector', 'dorms_close', get_string('dorms_close_on','local_mxschool'));
        $mform->setDefault('dorms_close', get_config('local_mxschool', 'dorms_close'));


        $this->add_action_buttons(false, get_string('update'));

        // Finally set the current form data
        //$this->set_data($student);
    }
}

class vacations_dates_form extends moodleform {

    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;

        $mform->addElement('date_selector', 'thanksgiving_vacation_date', get_string('thanksgiving_vacation_date','local_mxschool'));
        $mform->setDefault('thanksgiving_vacation_date', get_config('local_mxschool', 'thanksgiving_vacation_date'));
        
        $mform->addElement('date_selector', 'december_vacation_date', get_string('december_vacation_date','local_mxschool'));
        $mform->setDefault('december_vacation_date', get_config('local_mxschool', 'december_vacation_date'));
        
        $mform->addElement('date_selector', 'march_vacation_date', get_string('march_vacation_date','local_mxschool'));
        $mform->setDefault('march_vacation_date', get_config('local_mxschool', 'march_vacation_date'));
        
        $mform->addElement('date_selector', 'summer_vacation_date', get_string('summer_vacation_date','local_mxschool'));
        $mform->setDefault('summer_vacation_date', get_config('local_mxschool', 'summer_vacation_date'));
        
        $this->add_action_buttons(false, get_string('update'));

        // Finally set the current form data
        //$this->set_data($student);
    }
}


class clear_form extends moodleform {

    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;

        $mform->addElement('checkbox', 'clear_advisor', get_string('clear_advisor', 'local_mxschool'));
        $mform->addElement('checkbox', 'clear_vacation', get_string('clear_vacation', 'local_mxschool'));
        $mform->addElement('checkbox', 'clear_rooming', get_string('clear_rooming', 'local_mxschool'));
        $mform->addElement('checkbox', 'clear_announcements', get_string('clear_announcements', 'local_mxschool'));
        $mform->addElement('checkbox', 'clear_subjects_courses_tutors_records', get_string('clear_subjects_courses_tutors_records', 'local_mxschool'));
        $mform->addElement('checkbox', 'clear_peer_tutors_records', get_string('clear_peer_tutors_records', 'local_mxschool'));
        $mform->addElement('checkbox', 'clear_weekend_records', get_string('clear_weekend_records', 'local_mxschool'));
        $mform->addElement('checkbox', 'clear_driving_records', get_string('clear_driving_records', 'local_mxschool'));
        $mform->addElement('checkbox', 'clear_esignout_records', get_string('clear_esignout_records', 'local_mxschool'));

        $this->add_action_buttons(get_string('cancel'), get_string('clear'));

        // Finally set the current form data
        //$this->set_data($student);
    }
}

