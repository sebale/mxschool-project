<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Kaltura version file.
 *
 * @package    local_kaltura_announcements
 * @author     KALTURA
 * @copyright  2016 KALTURA, kaltura.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../config.php');
require_once('lib.php');
require_once('setup_roles_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$systemcontext   = context_system::instance();
require_login();
require_capability('local/mxschool:settings', $systemcontext);

$title = get_string('setup_roles', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/setup-roles.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('course');
$PAGE->set_context($systemcontext);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$form = new setup_roles_form(null);
if (!$form->is_cancelled() && $data = $form->get_data()) {
    unset($data->submitbutton);
    set_config('user_roles',json_encode($data),'local_mxschool');

    $jAlert->create(array('type'=>'success', 'text'=>'Successfully updated'));
    redirect(new moodle_url('/local/mxschool/setup-roles.php'));
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$form->display();

echo $OUTPUT->footer();
