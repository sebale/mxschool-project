<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


function print_advisors_select(){
    global $DB, $CFG;
    $output = '';
    
    $advisors = $DB->get_records_sql("SELECT f.*, CONCAT(u.firstname, ' ', u.lastname) as username, u.email 
                                    FROM {local_mxschool_faculty} f 
                                        LEFT JOIN {user} u ON u.id = f.userid 
                                        WHERE u.id > 0 AND f.available > 0
                                    ORDER BY u.firstname, u.lastname");
    
    $output = '';
    $output .= html_writer::start_tag('div', array('class'=>'clone-select-box'));
    $output .= html_writer::start_tag('select', array('name' => 'other-adv', 'class'=>'other-adv-select'));
    $output .= html_writer::tag('option', '', array('value' => ''));
        foreach ($advisors as $advisor){
            $output .= html_writer::tag('option', $advisor->username, array('value' => $advisor->id));   
        }
    $output .= html_writer::end_tag("select");
    $output .= html_writer::end_tag("div");
    
    return $output;
}

function get_dorms_list(){
    global $DB, $CFG;
    $list = array(''=>'');
    $dorms = $DB->get_records_sql("SELECT * FROM {local_mxschool_dorms} WHERE status > 0 ORDER BY name");
    if (count($dorms)){
        foreach ($dorms as $dorm){
            $list[$dorm->abbreviation] = $dorm->name;
        }
    }
    
    return $list;
}

function get_advisors_list(){
    global $DB, $CFG;
    $list = array(''=>'');
    $advisors = $DB->get_records_sql("SELECT f.*, CONCAT(u.firstname, ' ', u.lastname) as username, u.email 
                                    FROM {local_mxschool_faculty} f 
                                        LEFT JOIN {user} u ON u.id = f.userid 
                                        WHERE u.id > 0 AND f.available > 0
                                    ORDER BY u.firstname, u.lastname");
    if (count($advisors)){
        foreach ($advisors as $advisor){
            $list[$advisor->id] = $advisor->username;
        }
    }
    
    return $list;
}


function get_students_list(){
    global $DB, $CFG;
    $list = array(''=>'');
    $students = $DB->get_records_sql("SELECT s.id, CONCAT(u.firstname, ' ', u.lastname) as username 
                                    FROM {local_mxschool_students} s 
                                        LEFT JOIN {user} u ON u.id = s.userid 
                                        WHERE u.id > 0 AND u.deleted = 0
                                    ORDER BY u.firstname, u.lastname");
    if (count($students)){
        foreach ($students as $student){
            $list[$student->id] = $student->username;
        }
    }
    
    return $list;
}

function get_announcements_courseslist(){
    global $DB, $CFG;
    $list = array();
    
    if (is_siteadmin()){
        $courses = $DB->get_records_sql('SELECT * FROM {course} ORDER BY fullname');
    } else {
        $courses = enrol_get_my_courses();
    }
    
    if (count($courses)){
        foreach ($courses as $course){
            $list[$course->id] = $course->fullname;
        }
    }
    
    return $list;
}

function getSites($sitetype = ''){
    global $DB;
    $sites = array();
    
    if ($sitetype != ''){
        $db_sites = $DB->get_records_sql("SELECT * FROM {local_mxschool_pickup_sites} WHERE sitetype = '$sitetype' AND status > 0");
    } else {
        $db_sites = $DB->get_records("local_mxschool_pickup_sites", array('status'=>1));   
    }
    if (count($db_sites) > 0){
        foreach ($db_sites as $site){
            $sites[$site->type][$site->sitetype][$site->id] = $site->site;
        }
    }
    
    return $sites;
}
