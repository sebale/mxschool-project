<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class setup_roles_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB;

        $mform          = $this->_form;
        $roles_obj = role_fix_names(get_all_roles());
        $roles = array(''=>'Select role');
        foreach($roles_obj as $role){
            $roles[$role->id] = (empty($role->name))?$role->shortname:$role->name;
        }
        $data = get_config('local_mxschool', 'user_roles');
        $data = (empty($data))?new stdClass():json_decode($data);

        $mform->addElement('select', 'student_role', get_string('student_role','local_mxschool'),$roles);
        $mform->setType('student_role', PARAM_INT);
        $mform->addRule('student_role', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $mform->addElement('select', 'faculty_member_role', get_string('faculty_member_role','local_mxschool'),$roles);
        $mform->setType('faculty_member_role', PARAM_INT);
        $mform->addRule('faculty_member_role', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $mform->addElement('select', 'peer_tutors_role', get_string('peer_tutors_role','local_mxschool'),$roles);
        $mform->setType('peer_tutors_role', PARAM_INT);
        $mform->addRule('peer_tutors_role', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        foreach($data as $key=>$value){
            $mform->setDefault($key, $value);
        }

        $this->add_action_buttons(false, get_string('save', 'local_mxschool'));
    }
}


