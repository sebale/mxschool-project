$(document).ready(function() { 
 		
		var user_name = ($('#stu_name').length>0)?$('#stu_name').val():$('input[name="name"]').val();
		if (user_name != '' ) {
			$.ajax({
				url:M.cfg.wwwroot+"/local/mxschool/rooming/get_roommate_request.php",
				type: 'POST',
				dataType: 'json',
				data: {
					'stuid' :user_name,
				},
			  success: function(data) {
                  var empty_option = '<option value="">Name</option>';
                  var preferred_roommate = $("#preferred_roommate").val();

                  var old_val = [];
                  for(var i=1;i<=6;i++){
					  var val = $("#dormate"+i).find(":selected").val();
					  if(val != '')
					 	 old_val[i] = val;
                  }

                  $('#dormate1').html(empty_option + data.options_grade);
                  $('#dormate2').html(empty_option + data.options_grade);
                  $('#dormate3').html(empty_option + data.options_grade);
                  $('#dormate4').html(empty_option + data.options_all);
                  $('#dormate5').html(empty_option + data.options_all);
                  $('#dormate6').html(empty_option + data.options_all);

                  for(var i=1;i<=6;i++){
                      $("#dormate"+i+" option").removeAttr("disabled").show();
                      $("#dormate"+i+" option[value='" + user_name + "']").attr("disabled", "disabled").hide();
                      $("#dormate"+i+" option[value='" + old_val[i] + "']").attr("selected", "selected");
                  }
				  if(old_val.length>0)
                  	$('.hide-label, .req_dorms_any').show();

                  enable_disable_option();
                  $("#preferred_roommate option[value='" + preferred_roommate + "']").attr("selected", "selected");
				  $("#fitem_stu_name + .fitem .felement.fstatic").html(data.dorm);
				}
			}); //end ajax function
		}
		else {
            $(".rdormmates").hide();
            $("#preferred_roommate").html('<option value="">Name</option>');
		}

	$("#dormate1").change(function () {
            enable_disable_option();
			$(".second_dormate_requested").show();
	});

	$("#dormate2").change(function () {
            enable_disable_option();
            $('.third_dormate_requested').show();
	});

	$("#dormate3").change(function () {
            enable_disable_option();
            $('.fourth_dormate_requested').show();
            $(".req_dorms_any").show();
	});

	$("#dormate4").change(function () {
            enable_disable_option();
			$('.fifth_dormate_requested').show();
	}); 

	$("#dormate5").change(function () {
			enable_disable_option();
			$('.sixth_dormate_requested').show();
	}); // end change

	$("#dormate6").change(function () {
			enable_disable_option();
	}); // end change



	//Get gender based on Student Name Selection
	$('#stu_name').change(function(){
		var stuid = $(this).val();
		if (stuid == "") {
			$('.rdormmates').hide();
		}else {
			$.ajax({
				url: M.cfg.wwwroot + "/local/mxschool/rooming/get_roommate_request.php",
				type: 'POST',
				dataType: 'json',
				data: {
					'stuid': stuid,
				},
				success: function (data) {
					var empty_option = '<option value="">Name</option>';
					//get select value from user's name
					var stu_name = $('#stu_name').find(":selected").val();

					$('#dormate1').html(empty_option + data.options_grade);
					$('#dormate2').html(empty_option + data.options_grade);
					$('#dormate3').html(empty_option + data.options_grade);
					$('#dormate4').html(empty_option + data.options_all);
					$('#dormate5').html(empty_option + data.options_all);
					$('#dormate6').html(empty_option + data.options_all);

                    for(var i=1;i<=6;i++){
                        $("#dormate"+i+" option").removeAttr("disabled").show();
                        $("#dormate"+i+" option[value='" + stu_name + "']").attr("disabled", "disabled").hide();
                    }

					$(".rdormmates").show();

					$("#fitem_stu_name + .fitem .felement.fstatic").html(data.dorm);
				}
			}); //end ajax function
		}

	});//end stu_name change function

/********Validation on enter_rooming_form.php**********/

});//end doc ready function

function enable_disable_option(){
    $("#preferred_roommate").html('<option value="">Name</option>');
	for(var i=1;i<=6;i++){
		$("#dormate"+i+" option").removeAttr("disabled").show();
	}
	for(var i=1;i<=6;i++){
		var val = $('#dormate'+i).find(":selected").val();
        if(val != '') {
            var text = $('#dormate' + i).find(":selected").text();
            var option = '<option value="' + val + '">' + text + '</option>';
            $("#preferred_roommate").append(option);
            for (var j = 1; j <= 6; j++) {
				if(i!=j)
                	$("#dormate" + j + " option[value='" + val + "']").attr("disabled", "disabled").hide();
            }
        }
	}
}
