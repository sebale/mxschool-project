jQuery(document).ready(function(){

    if (jQuery('input.onoff-switcher').length){
        jQuery('input.onoff-switcher').each(function(event){
            jQuery(this).parent().parent().addClass('onof-switcher-box');
            jQuery(this).wrapAll( "<label />");
            jQuery(this).after('<span></span>');
        });
    }
   
    jQuery('.tabs-list li').click(function(event){
        jQuery(".tabs-list li").removeClass('active');
        jQuery(this).addClass('active');

        jQuery('.mx-jtabs-item').removeClass('visible');
        var index = jQuery(".tabs-list li").index(this);
        jQuery('.mx-jtabs-item').eq(index).addClass('visible');
    });
    
    if ($('.mask-input').length){
        $('.mask-input').mask('(999) 999-9999');
    }
    
    /*if (jQuery('.date-time').length){
        jQuery('.date-time').each(function(e){
            jQuery(this).parent().addClass('input-append datetimepicker');
            jQuery(this).attr('data-format', 'MM/dd/yyyy HH:mm PP');
            jQuery(this).parent().append('<span class="add-on"><span class="fa fa-calendar"></span></span>');
            jQuery(this).parent().datetimepicker({
              language: "en",
              useSeconds: false,
              pick12HourFormat: true
            });
            jQuery(this).click(function(e){
                jQuery(this).next().trigger('click');
            });
        });
    }*/
});