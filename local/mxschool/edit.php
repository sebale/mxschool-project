<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Kaltura version file.
 *
 * @package    local_kaltura_announcements
 * @author     KALTURA
 * @copyright  2016 KALTURA, kaltura.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../config.php');
require_once('lib.php');
require_once('edit_form.php');

$systemcontext   = context_system::instance();
require_login();
require_capability('local/kaltura_announcements:manage', $systemcontext);

$id = optional_param('id', 0, PARAM_INT); // Announcement id.

$PAGE->set_pagelayout('admin');
$pageparams = array('id' => $id);
$PAGE->set_url('/local/kaltura_announcements/edit.php', $pageparams);

$imagefilesoptions = array(
    'maxfiles' => 1,
    'maxbytes' => $CFG->maxbytes,
    'subdirs' => 0,
    'accepted_types' => 'image',
    'context' => $systemcontext
);

// Prepare course and the editor.
$editoroptions = array('maxfiles' => 1, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>$systemcontext, 'subdirs'=>0);
if ($id > 0) {
    
    $announcement = $DB->get_record('local_kaltura_announcements', array('id'=>$id));
    
} else {
    
    $announcement = new stdClass();
    $announcement->id = null;
    
}

$announcement = file_prepare_standard_editor($announcement, 'description', $editoroptions, $systemcontext, 'local_kaltura_announcements', 'descriptionfile', $announcement->id);

// First create the form.
$args = array(
    'id' => $id,
    'announcement' => $announcement,
    'editoroptions' => $editoroptions,
    'imagefilesoptions' => $imagefilesoptions
);
$editform = new edit_form(null, $args);

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/kaltura_announcements/index.php'));
} else if ($data = $editform->get_data()) {
    // Process data if submitted.
    $announcement = $data;
    $announcement->description       = '';          // updated later
    $announcement->descriptionformat = FORMAT_HTML; // updated later
    $announcement->timemodified = time();
    $announcement->userid = $USER->id;
    
    if ($id > 0) {
        // Update existing announcement.
        $DB->update_record('local_kaltura_announcements', $announcement);
    } else {
        // Add new announcement.
        $announcement->timecreated = time();
        $announcement->id = $DB->insert_record('local_kaltura_announcements', $announcement);
    }
    
    // save and relink embedded images and save attachments
    $announcement = file_postupdate_standard_editor($announcement, 'description', $editoroptions, $systemcontext, 'local_kaltura_announcements', 'descriptionfile', $announcement->id);
    
    $fs = get_file_storage();
    $fs->delete_area_files($systemcontext->id, 'local_kaltura_announcements', 'imagefile', $announcement->id);   
    file_save_draft_area_files($announcement->imagefile, $systemcontext->id, 'local_kaltura_announcements', 'imagefile', $announcement->id, array('subdirs' => 0, 'maxfiles' => 1));
    
    // store the updated value values
    $DB->update_record('local_kaltura_announcements', $announcement);
    
    redirect(new moodle_url('/local/kaltura_announcements/index.php'));
}

// Print the form.

$site = get_site();
$title = ($id > 0) ? get_string('announcementsedit', 'local_kaltura_announcements') : get_string('announcementscreate', 'local_kaltura_announcements');

$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$editform->display();

echo $OUTPUT->footer();
