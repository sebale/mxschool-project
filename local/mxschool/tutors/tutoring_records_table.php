<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class tutoring_records_table extends table_sql {

    function __construct($uniqueid, $search, $student, $tutor) {
        global $CFG,$DB;

        parent::__construct($uniqueid);

        if(isset($student->id)){
            $columns = array('date', 'time', 'tutorname', 'subject', 'course', 'topic', 'type_request', 'effectiveness', 'notes', 'actions');
            $header = array('Date', 'Time', 'Tutor Name', 'Subject Tutored', 'Course Tutored', 'Topic of Tutor Session', 'Type of Help Requested', 'Effectiveness of Session', 'Notes', 'Actions');
            $this->define_columns($columns);
            $this->define_headers($header);


            $fields = 'ts.id,
                   ts.date,
                   ts.time, 
                   CONCAT(tu.firstname," ",tu.lastname) as tutorname,
                   sub.name as subject,
                   cou.name as course,
                   ts.topic,
                   ts.type_request,
                   ts.type_requested_other,
                   ts.effectiveness,
                   ts.notes';
            $from = "{local_mxschool_tutors_sess} ts
                    LEFT JOIN {local_mxschool_tutors} t ON ts.tutor_id=t.id
                    LEFT JOIN {user} tu ON tu.id=t.userid
                    LEFT JOIN {local_mxschool_tutors_cat} sub ON sub.id=ts.subject
                    LEFT JOIN {local_mxschool_tutors_course} cou ON cou.id=ts.course";

            $where = "ts.student_id = " . $student->id;
        }elseif(isset($tutor->id)){
            $columns = array('date', 'time', 'studentname', 'subject', 'course', 'topic', 'type_request', 'effectiveness', 'notes', 'actions');
            $header = array('Date', 'Time', 'Student Name', 'Subject Tutored', 'Course Tutored', 'Topic of Tutor Session', 'Type of Help Requested', 'Effectiveness of Session', 'Notes', 'Actions');
            $this->define_columns($columns);
            $this->define_headers($header);


            $fields = 'ts.id,
                   ts.date,
                   ts.time, 
                   CONCAT(su.firstname," ",su.lastname) as studentname,
                   sub.name as subject,
                   cou.name as course,
                   ts.topic,
                   ts.type_request,
                   ts.type_requested_other,
                   ts.effectiveness,
                   ts.notes';
            $from = "{local_mxschool_tutors_sess} ts
                    LEFT JOIN {local_mxschool_students} s ON s.id=ts.student_id
                    LEFT JOIN {user} su ON su.id=s.userid
                    LEFT JOIN {local_mxschool_tutors_cat} sub ON sub.id=ts.subject
                    LEFT JOIN {local_mxschool_tutors_course} cou ON cou.id=ts.course";

            $where = "ts.tutor_id = " . $tutor->id;
        }else{
            $columns = array('date', 'time', 'tutorname', 'studentname', 'subject', 'course', 'topic', 'type_request', 'effectiveness', 'notes', 'actions');
            $header = array('Date', 'Time', 'Tutor Name', 'Student Name', 'Subject Tutored', 'Course Tutored', 'Topic of Tutor Session', 'Type of Help Requested', 'Effectiveness of Session', 'Notes', 'Actions');
            $this->define_columns($columns);
            $this->define_headers($header);


            $fields = 'ts.id,
                   ts.date,
                   ts.time, 
                   CONCAT(tu.firstname," ",tu.lastname) as tutorname, 
                   CONCAT(su.firstname," ",su.lastname) as studentname,
                   sub.name as subject,
                   cou.name as course,
                   ts.topic,
                   ts.type_request,
                   ts.type_requested_other,
                   ts.effectiveness,
                   ts.notes';
            $from = "{local_mxschool_tutors_sess} ts
                    LEFT JOIN {local_mxschool_tutors} t ON ts.tutor_id=t.id
                    LEFT JOIN {user} tu ON tu.id=t.userid
                    LEFT JOIN {local_mxschool_students} s ON s.id=ts.student_id
                    LEFT JOIN {user} su ON su.id=s.userid
                    LEFT JOIN {local_mxschool_tutors_cat} sub ON sub.id=ts.subject
                    LEFT JOIN {local_mxschool_tutors_course} cou ON cou.id=ts.course";

            $where = "ts.id>0";
        }

        if(!empty($search)){
            //LIKE '%$search%'

            $where .= " AND (ts.date LIKE '%$search%' OR ts.time LIKE '%$search%' OR CONCAT(tu.firstname,\" \",tu.lastname) LIKE '%$search%' OR CONCAT(su.firstname,\" \",su.lastname) LIKE '%$search%' OR sub.name LIKE '%$search%' OR cou.name LIKE '%$search%' OR ts.notes LIKE '%$search%' OR ts.type_requested_other LIKE '%$search%')";
        }


        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }

    function print_nothing_to_display(){
        if ($this->is_downloading()){
            return parent::print_nothing_to_display();
        }
        redirect(new moodle_url('/local/mxschool/tutors/edit-tutor-session.php'));
    }

    function col_time($row){
        $options = array(1=>'L-block', 2=>'6:30-7:30');
        return $options[$row->time];
    }

    function col_type_request($row){
        $options = array(   1=>'Homework help',
            2=>'Studying for a quiz',
            3=>'Studying for a test',
            4=>'Understanding a concept',
            5=>'Help with a project',
            6=>'Other (explain below)');
        if($row->type_request == 6)
            return $row->type_requested_other;
        else
            return $options[$row->type_request];
    }

    function col_effectiveness($row){
        $options = array(   1=>'1 - extremely helpful; issue completely resolved',
            2=>'2 - very helpful; student is better-off',
            3=>'3 - somewhat helpful; some benefit to student',
            4=>'4 - slightly helpful; student still unclear',
            5=>'5 - not effective; student received no benefit');
        return $options[$row->effectiveness];
    }

    function col_actions($values) {
        global $OUTPUT, $PAGE;

        if ($this->is_downloading()){
            return '';
        }

        $strdelete  = get_string('delete');
        $stredit  = get_string('edit');

        $edit = array();

        $aurl = new moodle_url('/local/mxschool/tutors/edit-tutor-session.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/tutors/tutoring-records.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));

        return implode('', $edit);
    }

}
