<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require_once('edit_tutor_session_form.php');

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:tutors_create', $systemcontext);

$id = optional_param('id', 0, PARAM_INT);

$title = get_string('tutor_session_form', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/tutors/tutor-session.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('tutors_name', 'local_mxschool'), new moodle_url('/local/mxschool/tutors/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/tutors.js');
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$tutor = $DB->get_record('local_mxschool_tutors',array('userid'=>$USER->id));

$form = new edit_tutor_session_form(null,array('id'=>$id,'tutor'=>$tutor));
if (!$form->is_cancelled() && $data = $form->get_data()) {
    $record = new stdClass();
    $record->date = date('m/d/Y',$data->date);
    $record->time = $data->time;
    $record->tutor_id = (isset($tutor->id))?$tutor->id:$data->tutor_id;
    $record->student_id = $data->student_id;
    $record->subject = $data->subject;
    $record->course = $data->course;
    $record->topic = $data->topic;
    $record->type_request = $data->type_request;
    $record->type_requested_other = ($data->type_request == 6)?$data->type_requested_other:'';
    $record->effectiveness = $data->effectiveness;
    $record->notes = $data->notes;

    require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");
    if(isset($data->id)){
        $record->id = $data->id;
        $DB->update_record('local_mxschool_tutors_sess',$record);
        $jAlert->create(array('type'=>'success', 'text'=>'Successfully updated'));
    }else{
        $record->timecreate = time();
        $DB->insert_record('local_mxschool_tutors_sess',$record);
        $jAlert->create(array('type'=>'success', 'text'=>'Successfully created'));
    }
    if(!has_capability('local/mxschool:tutors_settings', $systemcontext))
        redirect(new moodle_url("/my/"));
    else
        redirect(new moodle_url('/local/mxschool/tutors/tutoring-records.php'));

}elseif($form->is_cancelled()){
    if(!has_capability('local/mxschool:tutors_settings', $systemcontext))
        redirect(new moodle_url("/my/"));
    else
        redirect(new moodle_url('/local/mxschool/tutors/tutoring-records.php'));
}
echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'tutors-form'));
$form->display();
echo html_writer::end_tag("div");

echo $OUTPUT->footer();
