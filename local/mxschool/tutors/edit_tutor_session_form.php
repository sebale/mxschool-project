<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_tutor_session_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB;

        $mform          = $this->_form;
        $id             = $this->_customdata['id'];
        $tutor             = $this->_customdata['tutor'];

        if($id){
            $instant = $DB->get_record('local_mxschool_tutors_sess',array('id'=>$id));
            $instant->date = strtotime($instant->date);
        }

        if(!isset($tutor->id)){
            $tutors_obj = $DB->get_records_sql(' SELECT t.id, CONCAT(u.firstname," ",u.lastname) as name
                                         FROM {local_mxschool_tutors} t
                                          LEFT JOIN {user} u ON u.id = t.userid');
            $tutors = array();
            foreach($tutors_obj as $item){
                $tutors[$item->id] = $item->name;
            }
        }

        $students_obj = $DB->get_records_sql(' SELECT s.id, CONCAT(u.firstname," ",u.lastname) as name
                                     FROM {local_mxschool_students} s
                                      LEFT JOIN {user} u ON u.id = s.userid');
        $students = array();
        foreach($students_obj as $item){
            $students[$item->id] = $item->name;
        }

        $subjects_obj = $DB->get_records_sql(' SELECT id, name
                                         FROM {local_mxschool_tutors_cat} ');
        $subjects = array(''=>'--------');
        foreach($subjects_obj as $item){
            $subjects[$item->id] = $item->name;
        }
        
        $courses_obj = $DB->get_records_sql(' SELECT id, name
                                         FROM {local_mxschool_tutors_course} ');
        $courses = array(''=>'--------');
        foreach($courses_obj as $item){
            $courses[$item->id] = $item->name;
        }

        $mform->addElement('date_selector','date',get_string('date'));
        $mform->setType('date', PARAM_RAW);
        $mform->addRule('date', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'time', '', 'L-block', 1);
        $radioarray[] = $mform->createElement('radio', 'time', '', '6:30-7:30', 2);
        $mform->addGroup($radioarray, 'time', 'Time', array(' '), false);
        $mform->setType('time', PARAM_INT);
        $mform->addRule('time', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        if(!isset($tutor->id)){
            $mform->addElement('select', 'tutor_id', get_string('peer_tutors', 'local_mxschool'), $tutors);
            $mform->setType('tutor_id', PARAM_INT);
            $mform->addRule('tutor_id', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        }

        $mform->addElement('select', 'student_id', get_string('studentname', 'local_mxschool'), $students);
        $mform->setType('student_id', PARAM_INT);
        $mform->addRule('student_id', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $mform->addElement('select', 'subject', get_string('subject_tutored','local_mxschool'),$subjects,array('id'=>'subject_tutored'));
        $mform->setType('subject', PARAM_INT);
        $mform->addRule('subject', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('select', 'course', get_string('course_tutored','local_mxschool'),$courses,array('id'=>'course_tutored'));
        $mform->setType('course', PARAM_INT);
        $mform->addRule('course', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('text', 'topic', get_string('tutor_session_topic','local_mxschool'));
        $mform->setType('topic', PARAM_RAW);
        $mform->addRule('topic', get_string('required_field', 'local_mxschool'), 'required', null, 'client');


        $options = array(   1=>'Homework help',
                            2=>'Studying for a quiz',
                            3=>'Studying for a test',
                            4=>'Understanding a concept',
                            5=>'Help with a project',
                            6=>'Other (explain below)');
        $radioarray=array();
        foreach($options as $key=>$option){
            $radioarray[] = $mform->createElement('radio', 'type_request', '', $option, $key);
        }
        $radioarray[] = $mform->createElement('text', 'type_requested_other', '');
        $mform->addGroup($radioarray, 'type_request', get_string('type_help','local_mxschool'), array(' '), false);
        $mform->setType('type_request', PARAM_INT);
        $mform->setType('type_requested_other', PARAM_RAW);
        $mform->addRule('type_request', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $options = array(   1=>'1 - extremely helpful; issue completely resolved',
                            2=>'2 - very helpful; student is better-off',
                            3=>'3 - somewhat helpful; some benefit to student',
                            4=>'4 - slightly helpful; student still unclear',
                            5=>'5 - not effective; student received no benefit');
        $radioarray=array();
        foreach($options as $key=>$option){
            $radioarray[] = $mform->createElement('radio', 'effectiveness', '', $option, $key);
        }
        $mform->addGroup($radioarray, 'effectiveness', get_string('effectiveness','local_mxschool'), array(' '), false);
        $mform->setType('effectiveness', PARAM_INT);
        $mform->addRule('effectiveness', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $mform->addElement('textarea', 'notes', get_string('notes','local_mxschool'));
        $mform->setType('notes', PARAM_RAW);

        if(isset($instant)){
            $mform->addElement('hidden', 'id');
            $mform->setType('id', PARAM_INT);
            $this->set_data($instant);
        }

        $this->add_action_buttons(get_string('cancel'), get_string('save', 'local_mxschool'));
    }
}


