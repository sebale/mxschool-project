<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('manage_weekends_classes.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);
$student    = optional_param('student', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:weekend_settings', $systemcontext);

if ($action == 'delete' and $id) {
    $weekend = $DB->get_record('local_mxschool_weekend', array('id'=>$id));
    if ($weekend){
        $DB->delete_records('local_mxschool_weekend', array('id'=>$id));
    }
    $jAlert->create(array('type'=>'success', 'text'=>'Record was successfully deleted'));
    redirect(new moodle_url("/local/mxschool/weekend/manage-weekend.php"));
}


$title = get_string('manage_weekend_form','local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/weekend/manage-weekend.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('weekend_name', 'local_mxschool'), new moodle_url('/local/mxschool/weekend/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->add_body_class('print-table');

$form = new manage_weekends_form(null);
if (!$form->is_cancelled() && $data = $form->get_data()) {
    $table = new manage_weekends_table('manage_weekends_table',$data->student);
}elseif($student>0){
    $table = new manage_weekends_table('manage_weekends_table',$student);
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
if(isset($table))
    $table->out(200, true);
else
    $form->display();
echo html_writer::end_tag("div");

echo $OUTPUT->footer();

?>
    <script>
        jQuery(document).ready(function(){
            $('#dorm').change(function () {
                var val = $(this).val();
                if(val != ''){
                    jQuery.ajax({
                        url: M.cfg.wwwroot+"/local/mxschool/weekend/get_options_per_dorm.php",
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'dorm' :val,
                        },
                        complete: function(data) {
                            var old_val = $('#student').val();
                            var empty_option = '<option value="">------</option>';
                            $('#student').html(empty_option+data.responseJSON.options_text);
                            $('#student').val(old_val);
                            $('#instruction_bottom').html(data.responseJSON.message);
                        }
                    });
                }else{
                    $('#student').html('<option value="">------</option>');
                }
            });

            var val = $('#dorm').val();
            if(val != '') {
                $('#dorm').change();
            }else {
                $('#student').html('<option value="">------</option>');
            }
        });
    </script>
<?php
