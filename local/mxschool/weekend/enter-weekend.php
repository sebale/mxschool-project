<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('enter_weekend_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:weekend_submit', $systemcontext);

$title = get_string('enter_weekend_form','local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/weekend/enter-weekend.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('weekend_name', 'local_mxschool'), new moodle_url('/local/mxschool/weekend/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/bootstrap/bootstrap-datetimepicker.min.js', true);
$PAGE->requires->js('/local/mxschool/assets/js/bootstrap/collapse.js', true);
$PAGE->requires->js('/local/mxschool/assets/js/bootstrap/scrollspy.js', true);
$PAGE->requires->js('/local/mxschool/assets/js/script.js', true);
$PAGE->requires->css('/local/mxschool/assets/css/bootstrap-datetimepicker.min.css', true);
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->add_body_class('print-table');

$form = new enter_weekend_form(null);
if (!$form->is_cancelled() && $data = $form->get_data()) {
    require('../classes/notifications.php');
    
    $data->departure = strtotime($data->departure);
    $data->return = strtotime($data->return);
    
    $record = new stdClass();
    $record->student = $data->student;
    $record->departure_time = $data->departure;
    $record->return_time = $data->return;
    $record->destination = $data->destination;
    $record->phone = $data->phone;
    $record->transportation = $data->transportation;

    $instant = $DB->get_record_sql('SELECT * FROM {local_mxschool_weekend} WHERE student=:student AND :departure_time <= return_time  AND :return_time >= departure_time '
        ,array('student'=>$data->student,'departure_time'=>$data->departure,'return_time'=>$data->return));
    if(isset($instant->id)){
        $record->id = $instant->id;
        $DB->update_record('local_mxschool_weekend',$record);
        $data->instant = $instant;
    }else{
        $record->timecreate = time();
        $DB->insert_record('local_mxschool_weekend',$record);
    }
    $msg = new mxNotifications(6,$data);
    $msg->process();
    $jAlert->create(array('type'=>'success', 'text'=>'Successfully send'));

    if($DB->record_exists('local_mxschool_students',array('userid'=>$USER->id)))
        redirect(new moodle_url('/my/'));
    else
        redirect(new moodle_url('/local/mxschool/weekend/enter-weekend.php'));

}elseif($form->is_cancelled()){
    if($DB->record_exists('local_mxschool_students',array('userid'=>$USER->id)))
        redirect(new moodle_url('/my/'));
    else
        redirect(new moodle_url('/local/mxschool/weekend/'));
    
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
$form->display();
echo html_writer::end_tag("div");

echo $OUTPUT->footer();

?>
    <script>
        jQuery(document).ready(function(){
            
            jQuery('#id_departure').parent().addClass('input-append datetimepicker');
            jQuery('#id_departure').attr('data-format', 'MM/dd/yyyy HH:mm PP');
            jQuery('#id_departure').parent().append('<span class=\"add-on\"><span class=\"fa fa-calendar\"></span></span>');
            jQuery('#id_departure').parent().datetimepicker({
              language: 'en',
              useSeconds: false,
              pick12HourFormat: true,
            });
            jQuery('#id_departure').click(function(e){
                jQuery('#id_departure').next().trigger('click');
            });
            
            jQuery('#id_return').parent().addClass('input-append datetimepicker');
            jQuery('#id_return').attr('data-format', 'MM/dd/yyyy HH:mm PP');
            jQuery('#id_return').parent().append('<span class=\"add-on\"><span class=\"fa fa-calendar\"></span></span>');
            jQuery('#id_return').parent().datetimepicker({
              language: 'en',
              useSeconds: false,
              pick12HourFormat: true,
            });
            jQuery('#id_return').click(function(e){
                jQuery('#id_return').next().trigger('click');
            });
            
            $('#dorm').change(function () {
                var val = $(this).val();
                if(val != ''){
                    jQuery.ajax({
                        url: M.cfg.wwwroot+"/local/mxschool/weekend/get_options_per_dorm.php",
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'dorm' :val,
                        },
                        complete: function(data) {
                            var old_val = $('#student').val();
                            var empty_option = '<option value="">------</option>';
                            $('#student').html(empty_option+data.responseJSON.options_text);
                            $('#student').val(old_val);
                            $('#instruction_bottom').html(data.responseJSON.message);
                        }
                    });
                }else{
                    $('#student').html('<option value="">------</option>');
                }
            });

            var val = $('#dorm').val();
            if(val != '') {
                $('#dorm').change();
            }else {
                $('#student').html('<option value="">------</option>');
            }

            $('#mform1 input[name="update"]').val(0);
            $('#mform1').submit(function () {
                var prev = $($('#instruction').prev()).prev();
                if($(prev).hasClass('error') && !$(prev).is('#id_error_dorm')){
                    $('#mform1 input[name="update"]').val(1);
                }
            });
            
        });
    </script>
<?php
