<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require('../classes/notifications.php');

$field = optional_param('field', '', PARAM_RAW);
$action = optional_param('action', '', PARAM_ALPHA);
$value = required_param('value',PARAM_INT);
$state = optional_param('state',0,PARAM_BOOL);

require_login();
$systemcontext   = context_system::instance();

if($action == 'savechange'){
    $record = new stdClass();
    if($field == 'parent')
        $record->parent = $state;
    elseif($field == 'invite')
        $record->invite = $state;
    elseif($field == 'ok')
        $record->ok = $state;
    $record->id = $value;
    $DB->update_record('local_mxschool_weekend',$record);
    die('ok');
}elseif($action == 'sendemail'){
    $data = $DB->get_record_sql('SELECT w.*,s.dorm
                                FROM mdl_local_mxschool_weekend w
                                  LEFT JOIN mdl_local_mxschool_students s ON s.id=w.student
                                WHERE w.id=:id',array('id'=>$value));
    $data->departure = $data->departure_time;
    $data->return = $data->return_time;

    $msg = new mxNotifications(6,$data);
    $msg->process();
    die('ok');
}