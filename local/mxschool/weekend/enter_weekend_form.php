<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/formslib.php');
require_once('../lib.php');


class enter_weekend_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB,$USER;

        $mform          = $this->_form;

        $student = $DB->get_record('local_mxschool_students',array('userid'=>$USER->id));
        $faculty = new stdClass();
        $faculty->hohname = '';
        $faculty->hohphonenum = '';

        $dorms_obj = $DB->get_records_sql('SELECT * FROM {local_mxschool_dorms} WHERE status > 0 AND name NOT LIKE "%Day House%" ORDER BY name');
        $dorms = array(''=>'');
        foreach($dorms_obj as $dorm){
            $dorms[$dorm->abbreviation] = $dorm->name;
        }

        $mform->addElement('static', 'instruction','', get_string('weekend_instruction_top', 'local_mxschool'));
        if(isset($student->id)){
            $hoh = $DB->get_record_sql('SELECT u.id,u.lastname, u.firstname, u.phone1, d.name as dorm
                                        FROM {local_mxschool_faculty} f
                                          LEFT JOIN {user} u ON u.id=f.userid 
                                          LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=f.dorm
                                        WHERE f.dorm=:dorm AND f.type=1',array('dorm'=>$student->dorm));
            $faculty->hohname = $hoh->lastname.', '.$hoh->firstname;
            $faculty->hohphonenum = $hoh->phone1;
            
            $mform->addElement('hidden', 'student', $student->id);
            $mform->setType('student', PARAM_INT);
            $mform->addElement('hidden', 'dorm', $student->dorm);
            $mform->setType('dorm', PARAM_RAW);
            $mform->addElement('static', 'dorm_name', 'Dorm',$hoh->dorm);
            $mform->addElement('static', 'student_name', 'Student',fullname($USER));
        }else{
            $mform->addElement('select', 'dorm', get_string('dorm', 'local_mxschool'), $dorms, array('id' => 'dorm'));
            $mform->setType('dorm', PARAM_RAW);
            $mform->addRule('dorm', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

            $mform->addElement('select', 'student', get_string('student', 'local_mxschool'), get_students_list(), array('id' => 'student'));
            $mform->setType('student', PARAM_INT);
            $mform->addRule('student', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        }

        $times = array('startyear' => date('Y',strtotime('-6 month',get_config('local_mxschool','second_semester_starts'))),'stopyear'  => date('Y',get_config('local_mxschool','second_semester_starts')));
        
        $mform->addElement('text', 'departure', get_string('departure_date','local_mxschool'));
        $mform->addRule('departure', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('departure', PARAM_RAW);
        
        /*$mform->addElement('date_time_selector', 'departure', get_string('departure_date','local_mxschool'),$times);
        $mform->setType('departure', PARAM_INT);
        $mform->addRule('departure', get_string('required_field', 'local_mxschool'), 'required', null, 'client');*/

        $mform->addElement('text', 'return', get_string('return_date','local_mxschool'));
        $mform->addRule('return', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('return', PARAM_RAW);
        
        /*$mform->addElement('date_time_selector', 'return', get_string('return_date','local_mxschool'),$times);
        $mform->setType('return', PARAM_RAW);
        $mform->addRule('return', get_string('required_field', 'local_mxschool'), 'required', null, 'client');*/

        $mform->addElement('text', 'destination', get_string('destination','local_mxschool'));
        $mform->setType('destination', PARAM_RAW);
        $mform->addRule('destination', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $mform->addElement('text', 'transportation', get_string('transportation','local_mxschool'));
        $mform->setType('transportation', PARAM_RAW);
        $mform->addRule('transportation', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $mform->addElement('text', 'phone', get_string('phone_going_home','local_mxschool'));
        $mform->setType('phone', PARAM_RAW);
        $mform->addRule('phone', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $this->add_action_buttons(get_string('cancel'), get_string('send_weekend_form', 'local_mxschool'));

        $mform->addElement('static', 'instruction_bottom','', '<p id="instruction_bottom">'.get_string('weekend_instruction_bottom', 'local_mxschool',$faculty).'<p>');

        $mform->addElement('hidden', 'update', '0');
        $mform->setType('update', PARAM_INT);
    }
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);
        if(strtotime($data['return']) <= strtotime($data['departure'])){
            $errors['departure'] = 'Please check the dates you have entered for this weekend and submit again.';
        }

        $instant = $DB->get_record_sql('SELECT * FROM {local_mxschool_weekend} WHERE student=:student AND :departure_time <= return_time  AND :return_time >= departure_time ',array('student'=>$data['student'],'return_time'=>strtotime($data['return']),'departure_time'=>strtotime($data['departure'])));
        if(isset($instant->id) && $data['update'] == 0){
            $errors['instruction'] = 'An entry for the specified weekend already exists.  Submitting another weekend form will overwrite the previous entry.  Continue?';
        }

        $dates = $DB->get_records_sql('SELECT * FROM {local_mxschool_dates} WHERE time>:departure AND time<:return AND (weekendtype NOT LIKE "open" AND weekendtype NOT LIKE "free")',array('departure'=>strtotime($data['departure']), 'return'=>strtotime($data['return'])));
        if(count($dates)>0){
            $errors['departure'] = 'This weekend is not Open or Free';
        }

        return $errors;
    }
}
