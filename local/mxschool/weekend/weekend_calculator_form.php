<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once('../lib.php');


class weekend_calculator_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB;
        $mform          = $this->_form;
        $dates = $DB->get_records_sql('SELECT * FROM {local_mxschool_dates} ORDER BY time ASC');
        $weekends = array();
        foreach($dates as $date){
            $weekends[$date->time] = $date->date;
        }

        $mform->addElement('select', 'dorm', get_string('dorm', 'local_mxschool'), array_merge(get_dorms_list(),array('*'=>'All Houses')));
        $mform->setType('dorm', PARAM_RAW);
        $mform->addRule('dorm', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $mform->addElement('select', 'semester', get_string('semester', 'local_mxschool'), array(1=> 'First Semester',2=>'Second Semester'));
        $mform->setType('semester', PARAM_INT);
        $mform->addRule('semester', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $select = $mform->addElement('select', 'semester_dates', get_string('semester', 'local_mxschool'), $weekends);
        $select->setMultiple(true);
        $mform->setType('semester_dates', PARAM_RAW);
        $mform->addRule('semester_dates', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $this->add_action_buttons(null, get_string('generate_table', 'local_mxschool'));

    }
    function validation($data, $files) {
        $errors = parent::validation($data, $files);

        return $errors;
    }
}
