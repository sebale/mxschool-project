<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('weekend_calculator_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:weekend_calculator', $systemcontext);

$title = get_string('view_weekends_calculator','local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/weekend/weekend-calculator.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('weekend_name', 'local_mxschool'), new moodle_url('/local/mxschool/weekend/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->add_body_class('print-table');

$form = new weekend_calculator_form(null);
if (!$form->is_cancelled() && $data = $form->get_data()){
    $second_semester_starts = get_config('local_mxschool', 'second_semester_starts');
    $add_str = new stdClass();
    $times_sql = '';
    if(!empty($data->semester_dates)){
        $times_sql = ' AND time IN('.implode(',',$data->semester_dates).') ';
    }

    if($data->semester == 1){
        $weekends = $DB->get_records_sql("SELECT * FROM {local_mxschool_dates} WHERE time < :second_semester_starts $times_sql ORDER BY time ASC", array('second_semester_starts' => $second_semester_starts));
        $add_str->semester = 'First Semester';
    }else{
        $weekends = $DB->get_records_sql("SELECT * FROM {local_mxschool_dates} WHERE time >= :second_semester_starts $times_sql ORDER BY time ASC", array('second_semester_starts' => $second_semester_starts));
        $add_str->semester = 'Second Semester';
    }

    if($data->dorm == '*'){
        $students = $DB->get_records_sql('SELECT s.id, d.name as dorm, u.lastname, u.firstname, s.grade
                                          FROM {local_mxschool_students} s 
                                           LEFT JOIN {user} u ON u.id=s.userid
                                           LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=s.dorm
                                          ORDER BY s.dorm, u.lastname, u.firstname ASC');
        $add_str->dorm = '___________';
    }else{
        $students = $DB->get_records_sql('SELECT s.id, d.name as dorm, u.lastname, u.firstname, s.grade
                                              FROM {local_mxschool_students} s 
                                               LEFT JOIN {user} u ON u.id=s.userid
                                               LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation=s.dorm
                                              WHERE s.dorm=:dorm
                                              ORDER BY s.dorm, u.lastname, u.firstname ASC', array('dorm' => $data->dorm));
        $dorm = $DB->get_record('local_mxschool_dorms',array('abbreviation'=>$data->dorm));
        $add_str->dorm = $dorm->name;
    }
    
    $table = new html_table();
    $table->head = array('Name', 'Grade');
    if ($data->dorm == '*')
        $table->head[] = 'Dorm';
    foreach($weekends as $weekend){
        $table->head[] = substr($weekend->date,0,5);
    }
    $table->head[] = 'TOTAL';
    $table->head[] = 'Allowed';

    foreach($students as $student){
        $total = 0;
        $cells = array();
        $cells[] = $student->lastname.', '.$student->firstname;
        $cells[] = $student->grade;
        if ($data->dorm == '*')
            $cells[] = $student->dorm;
        
        $student_weekends = $DB->get_records_sql('SELECT * FROM {local_mxschool_weekend} w WHERE w.student=:student',array('student'=>$student->id));
        foreach($weekends as $weekend){
            $saturdaymorning = strtotime($weekend->date . " 12:00am");
            $sundaynight = $saturdaymorning + 172800;

            if($weekend->weekendtype == "free")
                $value = "free";
            elseif($weekend->weekendtype == "campus")
                $value = "camp";
            else
                $value = "";

            foreach($student_weekends as $student_weekend){
                if($student_weekend->departure_time < $sundaynight && $student_weekend->return_time > $saturdaymorning)
                    if($weekend->weekendtype != "free"){
                        $value = "X";
                        $total++;
                    }
            }
            $cells[] = $value;
        }

        if($student->grade == 10 && $data->semester == 2)
            $allowed = 5;
        elseif($student->grade == 9 || $student->grade == 10)
            $allowed = 4;
        elseif($student->grade == 11 || ($student->grade == 12 && $data->semester == 1))
            $allowed = 6;
        elseif($student->grade == 12)
            $allowed = "ALL";
        else
            $allowed = 0;

        $class = '';
        if(($student->grade < 12 || ($student->grade == 12 && $data->semester == 1)) && $allowed - $total <= 2)
        {
            if($allowed - $total == 2)
               $class = 'green';
            else if($allowed - $total == 1)
                $class = 'yellow';
            else if($allowed - $total <= 0)
                $class = 'red';
        }
        $cell = new html_table_cell($total);
        $cell->attributes['class'] = $class;
        $cells[] = $cell;

        $cells[] = $allowed;

        $table->data[] = $cells;
    }
}

echo $OUTPUT->header();

if(isset($table)){
    echo html_writer::tag('div',html_writer::link(new moodle_url('#'), get_string('print', 'local_mxschool'),array('class' => 'btn','id'=>'print-table')),array('class' => 'before-table'));
    echo $OUTPUT->heading(get_string('weekend_calculator_table_title', 'local_mxschool', $add_str), 2, 'table_title_center');
    echo html_writer::start_tag('div', array('class' => 'mxschool-table-box small_table weekend_calculator_table'));
    echo html_writer::table($table);

    echo html_writer::start_tag('table', array('cellpadding' => 0,'border'=>1, 'class'=>'legend-table'));
    echo html_writer::start_tag('tbody');

    echo html_writer::start_tag('tr');
    echo html_writer::tag('th', 'Legend:',array('colspan' => 2));
    echo html_writer::end_tag('tr');

    echo html_writer::start_tag('tr');
    echo html_writer::tag('td', '',array('width' => 50));
    echo html_writer::tag('td', '3+ weekends left');
    echo html_writer::end_tag('tr');

    echo html_writer::start_tag('tr');
    echo html_writer::tag('td', '',array('width' => 50,'class'=>'green'));
    echo html_writer::tag('td', '2 weekends left');
    echo html_writer::end_tag('tr');

    echo html_writer::start_tag('tr');
    echo html_writer::tag('td', '',array('width' => 50,'class'=>'yellow'));
    echo html_writer::tag('td', '1 weekends left');
    echo html_writer::end_tag('tr');

    echo html_writer::start_tag('tr');
    echo html_writer::tag('td', '',array('width' => 50,'class'=>'red'));
    echo html_writer::tag('td', 'No weekends left');
    echo html_writer::end_tag('tr');

    echo html_writer::start_tag('tr');
    echo html_writer::tag('td', 'free',array('width' => 50));
    echo html_writer::tag('td', 'Free weekend');
    echo html_writer::end_tag('tr');

    echo html_writer::start_tag('tr');
    echo html_writer::tag('td', 'camp',array('width' => 50));
    echo html_writer::tag('td', 'Campus weekend');
    echo html_writer::end_tag('tr');

    echo html_writer::end_tag('tbody');
    echo html_writer::end_tag('table');

    echo html_writer::end_tag("div");
}else{
    echo $OUTPUT->heading($title);
    echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
    $form->display();
    echo html_writer::end_tag("div");
}

echo $OUTPUT->footer();

if($table):?>
    <script>
        $(window).ready(function () {
            $('#print-table').click(function (e) {
                window.print();
                return false;
            });
        });
    </script>
<?php else:?>
    <script>
        $(window).ready(function () {
            var second_semester_starts = <?php echo get_config('local_mxschool', 'second_semester_starts');?>;
            $('#id_semester').change(function () {
                var semester = $(this).val();
                $('#id_semester_dates option').each(function () {
                    $(this).removeAttr('selected');
                    var time = $(this).attr('value');
                    if(semester == 1 && time < second_semester_starts){
                        $(this).attr('selected','selected');
                    }else if(semester == 2 && time >= second_semester_starts){
                        $(this).attr('selected','selected');
                    }
                });
            });
            $('#id_semester').change();
        });
    </script>
<?php endif;
