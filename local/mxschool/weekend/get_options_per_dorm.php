<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');

$dorm = required_param('dorm',PARAM_RAW);

$students = $DB->get_records_sql("SELECT s.id, CONCAT(u.lastname,' ',u.firstname) as name 
                                    FROM {local_mxschool_students} s 
                                        LEFT JOIN {user} u ON u.id = s.userid 
                                        WHERE u.id > 0 AND u.deleted = 0 AND s.dorm LIKE :dorm
                                    ORDER BY u.lastname,u.firstname",array('dorm'=>$dorm));

$options = '';
foreach ($students as $item) {
    $options .= '<option value="'.$item->id.'">'.$item->name.'</option>';
}

$hoh = $DB->get_record_sql('SELECT u.id,u.lastname, u.firstname, u.phone1
                                        FROM {local_mxschool_faculty} f
                                          LEFT JOIN {user} u ON u.id=f.userid 
                                        WHERE f.dorm=:dorm AND f.type=1',array('dorm'=>$dorm));

$faculty = new stdClass();
if(isset($hoh->id)){
    $faculty->hohname = $hoh->lastname . ', ' . $hoh->firstname;
    $faculty->hohphonenum = $hoh->phone1;
}else{
    $faculty->hohname = '';
    $faculty->hohphonenum = '';
}

die(json_encode(array('options_text' => $options, 'message'=>get_string('weekend_instruction_bottom', 'local_mxschool',$faculty))));

