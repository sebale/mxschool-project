<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/formslib.php');
require_once('../lib.php');

class manage_weekends_table extends table_sql {

    function __construct($uniqueid, $student) {
        global $CFG;

        parent::__construct($uniqueid);

        $columns = array('departure','return','destination', 'actions');
        $header = array('Departure Date','Return Date','Destination', 'Delete');

        $this->define_columns($columns);
        $this->define_headers($header);


        $fields = "w.id, w.destination, w.timecreate, w.departure_time, w.return_time, '' as actions";
        $from = "{local_mxschool_weekend} w";

        $where = " w.student='$student'";

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI'].'?student='.$student);
    }
    
    function col_departure($values){
        return date('m/d/Y',$values->departure_time);
    }

    function col_return($values){
        return date('m/d/Y',$values->return_time);
    }

    function col_actions($values) {
        global $OUTPUT, $PAGE;

        if ($this->is_downloading()){
            return '';
        }

        $strdelete  = get_string('delete');

        $edit = array();
        
        $aurl = new moodle_url('/local/mxschool/weekend/manage-weekend.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));

        return implode('', $edit);
    }
}

class manage_weekends_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB;

        $mform          = $this->_form;

        $mform->addElement('select', 'dorm', get_string('dorm', 'local_mxschool'), get_dorms_list(), array('id' => 'dorm'));
        $mform->setType('dorm', PARAM_RAW);
        $mform->addRule('dorm', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $mform->addElement('select', 'student', get_string('student', 'local_mxschool'), get_students_list(), array('id' => 'student'));
        $mform->setType('student', PARAM_INT);
        $mform->addRule('student', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $this->add_action_buttons(null, get_string('generate_table', 'local_mxschool'));
    }
}
