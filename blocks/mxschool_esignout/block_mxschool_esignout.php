<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * News items block mxschool deans office.
 *
 * @package    block
 * @subpackage mxschool_esignout
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

class block_mxschool_esignout extends block_base {
    
    function init() {
        $this->title = get_string('blockname', 'block_mxschool_esignout');
    }
    
	/*function hide_header() {
        return true;
    }*/
    
	function html_attributes() {
		$attributes = parent::html_attributes();
		$attributes['class'] .= ' no_border_block block_' . $this->name();
		if ($this->instance_can_be_docked() && get_user_preferences('docked_block_instance_'.$this->instance->id, 0)) {
            $attributes['class'] .= ' dock_on_load';
        }
        return $attributes;
    }
    
    function get_content() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
        
        $courseid = $this->page->course->id;
		$issite = ($courseid == SITEID);

        if (empty($this->instance)) {
            return $this->content;
        }
        $esignout_link = $this->print_esignout_link();

        if ($esignout_link != ''){
            $this->content->text .= html_writer::start_tag('ul');
            $this->content->text .= $esignout_link;
            $this->content->text .= html_writer::end_tag('ul');
        }
        
        return $this->content;
    }

    function print_esignout_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = ''; $where = '';
        $driver_grades  = get_config('local_mxschool', 'driver_grades');
        
        if (!empty($driver_grades)){
            $where .= "s.grade IN($driver_grades) AND";
        }
        $driver = $DB->get_record_sql("SELECT s.id
                                            FROM {local_mxschool_students} s
                                              LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation = s.dorm
                                            WHERE $where d.type='day' AND s.userid=:userid AND (s.maydrive = 1 OR s.maygiverides = 1)
                                              ",array('userid'=>$USER->id));
        

        if(isset($driver->id) || has_capability('local/mxschool:edriving_settings', context_system::instance())){
            $url = new moodle_url('/local/mxschool/esignout/enter-driving.php');
            $str = get_string('driver', 'local_mxschool');
            $str .= html_writer::link($url, get_string('signout', 'local_mxschool'));

            $today_start  = strtotime("today 00:01");
            $today_end  = strtotime("today 23:59");
            $exist_records = 0;
            if(isset($driver->id)){
                $exist_records = $DB->record_exists_sql("SELECT d.id FROM {local_mxschool_edriver} d WHERE d.driver=$driver->id AND d.timectreate > $today_start AND d.timectreate < $today_end");
            }
            if($exist_records || has_capability('local/mxschool:edriving_settings', context_system::instance())){
                $url = new moodle_url('/local/mxschool/esignout/drivings.php');
                $str .= ', ' . html_writer::link($url, get_string('signin', 'local_mxschool'));
            }

            $output .= html_writer::tag('li', $str);
        }

        if (!empty($driver_grades)){
            $student = $DB->get_record_sql("SELECT s.id
                                            FROM {local_mxschool_students} s
                                            WHERE $where s.userid=:userid
                                              ",array('userid'=>$USER->id));
            if(isset($student->id) || has_capability('local/mxschool:edriving_settings', context_system::instance())){
                $url = new moodle_url('/local/mxschool/esignout/enter-passenger.php');
                $str = get_string('passenger', 'local_mxschool');
                $str .= html_writer::link($url, get_string('signout', 'local_mxschool'));

                $today_start  = strtotime("today 00:01");
                $today_end  = strtotime("today 23:59");
                $exist_records = 0;
                if(isset($student->id)){
                    $exist_records = $DB->record_exists_sql("SELECT p.id FROM {local_mxschool_epassenger} p WHERE p.passenger=$student->id AND p.timecreate > $today_start AND p.timecreate < $today_end");
                }
                if($exist_records || has_capability('local/mxschool:edriving_settings', context_system::instance())){
                    $url = new moodle_url('/local/mxschool/esignout/passengers.php');
                    $str .= ', ' . html_writer::link($url, get_string('signin', 'local_mxschool'));
                }

                $output .= html_writer::tag('li', $str);
            }

        }else{
            $url = new moodle_url('/local/mxschool/esignout/passengers.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('create_passenger', 'local_mxschool')));
        }



        return $output;
    }
    

}


