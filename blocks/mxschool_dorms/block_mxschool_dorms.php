<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * News items block mxschool deans office.
 *
 * @package    block
 * @subpackage mxschool_deans_office
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

class block_mxschool_dorms extends block_base {

    function init() {
        global $CFG;
        require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");
        $this->title = get_string('blockname', 'block_mxschool_dorms');
    }

    /*function hide_header() {
        return true;
    }*/

    function html_attributes() {
        $attributes = parent::html_attributes();
        $attributes['class'] .= ' no_border_block block_' . $this->name();
        if ($this->instance_can_be_docked() && get_user_preferences('docked_block_instance_'.$this->instance->id, 0)) {
            $attributes['class'] .= ' dock_on_load';
        }
        return $attributes;
    }

    function get_content() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        $courseid = $this->page->course->id;
        $issite = ($courseid == SITEID);

        if (empty($this->instance)) {
            return $this->content;
        }

        $weekend_link = $this->print_weekend_link();
        $vacation_link = $this->print_vacation_link();
        $checkin_link = $this->print_checkin_link();

        if ($weekend_link != '' or $vacation_link != '' or $checkin_link != ''){
            $this->content->text .= html_writer::start_tag('ul');
            $this->content->text .= $weekend_link;
            $this->content->text .= $vacation_link;
            $this->content->text .= $checkin_link;
            $this->content->text .= html_writer::end_tag('ul');
        }

        return $this->content;
    }

    function print_weekend_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';

        if (has_capability('local/mxschool:weekend_submit', context_system::instance())){
            $url = new moodle_url('/local/mxschool/weekend/enter-weekend.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('enter_weekend', 'local_mxschool')));
        }

        if (has_capability('local/mxschool:weekend_calculator', context_system::instance())){
            $url = new moodle_url('/local/mxschool/weekend/weekend-calculator.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('view_weekends_calculator', 'local_mxschool')));
        }

        if (has_capability('local/mxschool:weekend_settings', context_system::instance())){
            $url = new moodle_url('/local/mxschool/weekend/manage-weekend.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('manage_weekend_form','local_mxschool')));
        }

        return $output;
    }

    function get_vacation_permissions() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;

        $result = new stdClass();
        $result->is_student = false;
        $result->records = array();

        $student = $DB->get_record('local_mxschool_students', array('userid'=>$USER->id));
        if ($student){
            $result->is_student = true;
        }

        return $result;
    }

    function print_vacation_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';

        $vacation_permissions = $this->get_vacation_permissions();
        $settings = $this->get_dates_settings();

        if ($vacation_permissions->is_student){

            if ($settings['vacation_form_enable'] != '') {
                $date_open = $settings[$settings['vacation_form_enable']];
            } else {
                $date_open = NULL;
            }

            $date_open_minus = strtotime($settings['vacation_form_time_prior'], $date_open);

            if (time() < $date_open_minus || time() > $date_open){
            } else {
                if (has_capability('local/mxschool:vacation_view', context_system::instance())){
                    $url = new moodle_url('/local/mxschool/vacationandtravel/sform.php');
                    $output .= html_writer::tag('li', html_writer::link($url, get_string('vacationandtravel_form', 'local_mxschool')));
                }
            }
        } elseif (has_capability('local/mxschool:vacation_manage', context_system::instance())){
            $url = new moodle_url('/local/mxschool/vacationandtravel/index.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('vacationandtravel_form', 'local_mxschool')));
        }

        return $output;
    }
    
    function print_checkin_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';

        if (has_capability('local/mxschool:checkin_settings', context_system::instance())){
            $url = new moodle_url('/local/mxschool/check-in/index.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('checkin_name', 'local_mxschool')));
        }

        return $output;
    }

    function get_dates_settings() {
        $settings = array();
        $settings['second_semester_starts'] = get_config('local_mxschool', 'second_semester_starts');
        $settings['dorms_close'] = get_config('local_mxschool', 'dorms_close');
        $settings['advisor_form_enable'] = get_config('local_mxschool', 'advisor_form_enable');
        $settings['advisor_form_time_prior'] = get_config('local_mxschool', 'advisor_form_time_prior');
        $settings['thanksgiving_vacation_date'] = get_config('local_mxschool', 'thanksgiving_vacation_date');
        $settings['december_vacation_date'] = get_config('local_mxschool', 'december_vacation_date');
        $settings['march_vacation_date'] = get_config('local_mxschool', 'march_vacation_date');
        $settings['summer_vacation_date'] = get_config('local_mxschool', 'summer_vacation_date');
        $settings['vacation_form_enable'] = get_config('local_mxschool', 'vacation_form_enable');
        $settings['vacation_form_time_prior'] = get_config('local_mxschool', 'vacation_form_time_prior');
        $settings['rooming_form_time_prior'] = get_config('local_mxschool', 'rooming_form_time_prior');

        return $settings;
    }

}


