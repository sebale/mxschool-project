<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * News items block mxschool deans office.
 *
 * @package    block
 * @subpackage block_mxschool_deans
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

class block_mxschool_deans extends block_base {
    
    function init() {
        $this->title = get_string('blockname', 'block_mxschool_deans');
    }
    
	/*function hide_header() {
        return true;
    }*/
    
	function html_attributes() {
		$attributes = parent::html_attributes();
		$attributes['class'] .= ' no_border_block block_' . $this->name();
		if ($this->instance_can_be_docked() && get_user_preferences('docked_block_instance_'.$this->instance->id, 0)) {
            $attributes['class'] .= ' dock_on_load';
        }
        return $attributes;
    }
    
    function get_content() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
        
        $courseid = $this->page->course->id;
		$issite = ($courseid == SITEID);

        if (empty($this->instance)) {
            return $this->content;
        }
        
        $select_advisor_link = $this->print_select_advisor_link();
        $driving_link = $this->print_driving_link();
        $esignout_link = $this->print_esignout_link();
        $rooming_link = $this->print_rooming_link();
        $vacation_link = $this->print_vacation_link();        
        $manage_students = $this->print_manage_students();

        if ($select_advisor_link != '' or $vacation_link != '' or $rooming_link != '' or $driving_link != '' or $esignout_link != '' or $manage_students != ''){
            $this->content->text .= html_writer::start_tag('ul');
            $this->content->text .= $select_advisor_link;
            $this->content->text .= $driving_link;
            $this->content->text .= $esignout_link;
            $this->content->text .= $rooming_link;
            $this->content->text .= $vacation_link;
            $this->content->text .= $manage_students;
            $this->content->text .= html_writer::end_tag('ul');
        }
        
        return $this->content;
    }
    
    function print_select_advisor_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';
        
        if (has_capability('local/mxschool:advisor_selection_manage', context_system::instance())){
            $url = new moodle_url('/local/mxschool/advisor_selection/index.php');
            $output .= html_writer::tag('li', html_writer::link($url, 'Advisor Selection'));
        }
        
        return $output;
    }
    
    function get_dates_settings() {            
        $settings = array();
        $settings['second_semester_starts'] = get_config('local_mxschool', 'second_semester_starts');
        $settings['dorms_close'] = get_config('local_mxschool', 'dorms_close');
        $settings['advisor_form_enable'] = get_config('local_mxschool', 'advisor_form_enable');
        $settings['advisor_form_time_prior'] = get_config('local_mxschool', 'advisor_form_time_prior');
        $settings['thanksgiving_vacation_date'] = get_config('local_mxschool', 'thanksgiving_vacation_date');
        $settings['december_vacation_date'] = get_config('local_mxschool', 'december_vacation_date');
        $settings['march_vacation_date'] = get_config('local_mxschool', 'march_vacation_date');
        $settings['summer_vacation_date'] = get_config('local_mxschool', 'summer_vacation_date');
        $settings['vacation_form_enable'] = get_config('local_mxschool', 'vacation_form_enable');
        $settings['vacation_form_time_prior'] = get_config('local_mxschool', 'vacation_form_time_prior');
        $settings['rooming_form_time_prior'] = get_config('local_mxschool', 'rooming_form_time_prior');

        return $settings;
    }
    
    function print_vacation_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';
        
        if (has_capability('local/mxschool:vacation_manage', context_system::instance())){
            $url = new moodle_url('/local/mxschool/vacationandtravel/index.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('vacationandtravel', 'local_mxschool')));
        }
        
        return $output;
    }

    function print_driving_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';

        if (has_capability('local/mxschool:driving_settings', context_system::instance())){
            $url = new moodle_url('/local/mxschool/driving/index.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('driving_name', 'local_mxschool')));
        }

        return $output;
    }

    function print_rooming_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';

        if (has_capability('local/mxschool:rooming_manage', context_system::instance())){
            $url = new moodle_url('/local/mxschool/rooming/index.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('rooming_name', 'local_mxschool')));
        }

        return $output;
    }
    
    function print_manage_students() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';

        if (has_capability('local/mxschool:manage_users', context_system::instance())){
            $url = new moodle_url('/local/mxschool/user_management/students.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('manage_students', 'local_mxschool')));
        }

        return $output;
    }

    function print_weekend_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';

        $url = new moodle_url('/local/mxschool/weekend/enter-weekend.php');
        $output .= html_writer::tag('li', html_writer::link($url, get_string('enter_weekend', 'local_mxschool')));

        return $output;
    }

    function print_esignout_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = ''; $where = '';
        
        if(has_capability('local/mxschool:edriving_settings', context_system::instance())){
            $url = new moodle_url('/local/mxschool/esignout/index.php');
            $str = html_writer::link($url, get_string('signout', 'local_mxschool'));
            $output .= html_writer::tag('li', $str);
        }

        return $output;
    }
    

}


