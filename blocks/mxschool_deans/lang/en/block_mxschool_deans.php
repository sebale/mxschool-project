<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * News items block mxschool deans office.
 *
 * @package    block
 * @subpackage mxschool_deans
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['mxschool_deans_office:addinstance'] = 'Add a Mxschool Deans\' block';
$string['mxschool_deans_office:myaddinstance'] = 'Add my Mxschool Deans\' block';
$string['pluginname'] = 'Mxschool Deans\' Block';
$string['blockname'] = 'Deans\' Block';
