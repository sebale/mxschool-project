<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * News items block mxschool peer tutoring.
 *
 * @package    block
 * @subpackage mxschool_peer_tutoring
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

class block_mxschool_peer_tutoring extends block_base {

    function init() {
        $this->title = get_string('blockname', 'block_mxschool_peer_tutoring');
    }

	/*function hide_header() {
        return true;
    }*/

	function html_attributes() {
		$attributes = parent::html_attributes();
		$attributes['class'] .= ' no_border_block block_' . $this->name();
		if ($this->instance_can_be_docked() && get_user_preferences('docked_block_instance_'.$this->instance->id, 0)) {
            $attributes['class'] .= ' dock_on_load';
        }
        return $attributes;
    }

    function get_content() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        $courseid = $this->page->course->id;
		$issite = ($courseid == SITEID);

        if (empty($this->instance)) {
            return $this->content;
        }
        $tutor_link = $this->print_tutors_link();

        if ($tutor_link != ''){
            $this->content->text .= html_writer::start_tag('ul');
            $this->content->text .= $tutor_link;
            $this->content->text .= html_writer::end_tag('ul');
        }

        return $this->content;
    }

    function get_dates_settings() {
        $settings = array();
        $settings['tutors_form_start_date'] = get_config('local_mxschool', 'tutors_form_start_date');
        $settings['tutors_form_end_date'] = get_config('local_mxschool', 'tutors_form_end_date');

        return $settings;
    }

    function print_tutors_link() {
        global $DB, $USER;
        $output = '';
        $settings = $this->get_dates_settings();
        $tutor = $DB->get_record('local_mxschool_tutors',array('userid'=>$USER->id));
        if ($settings['tutors_form_start_date'] <= time() && $settings['tutors_form_end_date'] >= time() && has_capability('local/mxschool:tutors_create', context_system::instance())){
            $url = new moodle_url('/local/mxschool/tutors/edit-tutor-session.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('peer_tutoring_records', 'local_mxschool')));
        }
        if ( has_capability('local/mxschool:tutors_settings', context_system::instance())){
            $url = new moodle_url('/local/mxschool/tutors/tutoring-records.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('peer_tutoring_records_manage', 'local_mxschool')));
        }
        return $output;
    }


}


