<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * News items block mxschool announcements.
 *
 * @package    block
 * @subpackage mxschool_announcements
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

class block_mxschool_announcements extends block_base {
    
    function init() {
        $this->title = get_string('blockname', 'block_mxschool_announcements');
    }
    
	/*function hide_header() {
        return true;
    }*/
    
	function html_attributes() {
		$attributes = parent::html_attributes();
		$attributes['class'] .= ' no_border_block block_' . $this->name();
		if ($this->instance_can_be_docked() && get_user_preferences('docked_block_instance_'.$this->instance->id, 0)) {
            $attributes['class'] .= ' dock_on_load';
        }
        return $attributes;
    }
    
    function get_content() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
        
        $courseid = $this->page->course->id;
		$issite = ($courseid == SITEID);

        if (empty($this->instance)) {
            return $this->content;
        }
        
        $this->content->text = $this->print_announcements();
        
        return $this->content;
    }
    
    function get_announcements(){
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        
        $courseid = $this->page->course->id;
		$issite = ($courseid == SITEID);
        
        if ($issite){
            $where = " AND a.type = 'system'";
            if (is_siteadmin()){
                $where = " AND (a.type = 'system' OR a.type = 'course')";
            } else {
                $my_courses = enrol_get_my_courses();
                if (count($my_courses) > 0){
                    $courses = array();
                    foreach ($my_courses as $my_course){
                        $courses[$my_course->id] = $my_course->id;
                    }
                    $where = " AND (a.type = 'system' OR (a.type = 'course' AND a.courseid IN (".implode($courses).")))";
                }
            }
            $announcements = $DB->get_records_sql("SELECT a.*, CONCAT(u.firstname, ' ', u.lastname) as user, c.fullname as coursename FROM {local_mxschool_announcements} a
                                                    LEFT JOIN {user} u ON u.id = a.userid
                                                    LEFT JOIN {course} c ON c.id = a.courseid
                                                        WHERE a.startdate <= ".time()." AND (a.enddate = 0 OR a.enddate >= ".time().") AND a.state > 0 $where
                                                    ORDER BY a.startdate");
        } else {
            $announcements = $DB->get_records_sql("SELECT a.*, CONCAT(u.firstname, ' ', u.lastname) as user, c.fullname as coursename FROM {local_mxschool_announcements} a
                                                    LEFT JOIN {user} u ON u.id = a.userid
                                                    LEFT JOIN {course} c ON c.id = a.courseid
                                                        WHERE a.startdate <= ".time()." AND (a.enddate = 0 OR a.enddate >= ".time().") AND a.state > 0 AND a.type = 'course' AND a.courseid = $courseid
                                                    ORDER BY a.startdate");
        }
        return $announcements;
    }
    
    function print_announcements(){
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        
        $courseid = $this->page->course->id;
		$issite = ($courseid == SITEID);
        $output = '';
        $announcements = $this->get_announcements();
        
        if (count($announcements) > 0 ){
            $output .= html_writer::start_tag('div', array('class' => 'announcements-box'));
            
            foreach ($announcements as $announcement){
                $output .= html_writer::start_tag('div', array('class' => 'announcement-item clearfix', 'data-item-id'=>$announcement->id));
                    $output .= html_writer::start_tag('div', array('class' => 'announcement-item-body'));
                        $output .= html_writer::tag('div', $announcement->title, array('class' => 'announcements-title'));
                        if ($issite and $announcement->courseid > 0){
                            $output .= html_writer::tag('div', 'Course: <a href="'.$CFG->wwwroot.'/course/view.php?id='.$announcement->courseid.'">'.$announcement->coursename.'</a>', array('class' => 'announcements-course'));
                        }
                        $output .= html_writer::tag('div', $announcement->description, array('class' => 'announcements-description'));
                
                        $output .= html_writer::start_tag('div', array('class' => 'announcement-item-footer clearfix', 'data-item-id'=>$announcement->id));
                            if ($announcement->user != ''){
                                $output .= html_writer::tag('div', $announcement->user, array('class' => 'announcements-user'));   
                            }
                            if ($announcement->announcementdate){
                                $output .= html_writer::tag('div', date('d F Y, ga', $announcement->announcementdate), array('class' => 'announcements-date'));   
                            }
                        $output .= html_writer::end_tag('div');
                    $output .= html_writer::end_tag('div');

                $output .= html_writer::end_tag('div');    
            }

            $output .= html_writer::end_tag('div');

        }
        
        return $output;
        
    }
    
}


