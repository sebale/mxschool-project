<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * News items block mxschool deans office.
 *
 * @package    block
 * @subpackage mxschool_deans_office
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

class block_mxschool_deans_office extends block_base {
    
    function init() {
        $this->title = get_string('blockname', 'block_mxschool_deans_office');
    }
    
	/*function hide_header() {
        return true;
    }*/
    
	function html_attributes() {
		$attributes = parent::html_attributes();
		$attributes['class'] .= ' no_border_block block_' . $this->name();
		if ($this->instance_can_be_docked() && get_user_preferences('docked_block_instance_'.$this->instance->id, 0)) {
            $attributes['class'] .= ' dock_on_load';
        }
        return $attributes;
    }
    
    function get_content() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
        
        $courseid = $this->page->course->id;
		$issite = ($courseid == SITEID);

        if (empty($this->instance)) {
            return $this->content;
        }
        
        $select_advisor_link = $this->print_select_advisor_link();
        //$vacation_link = $this->print_vacation_link();
        $rooming_link = $this->print_rooming_link();
        //$weekend_link = $this->print_weekend_link();
        //$esignout_link = $this->print_esignout_link();
        $driving_link = $this->print_driving_link();

        if ($select_advisor_link != '' or $vacation_link != '' or $rooming_link != '' or $driving_link != ''){
            $this->content->text .= html_writer::start_tag('ul');
            $this->content->text .= $select_advisor_link;
            $this->content->text .= $rooming_link;
            $this->content->text .= $driving_link;
            $this->content->text .= html_writer::end_tag('ul');
        }
        
        return $this->content;
    }
    
    function get_advisor_permissions() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        
        $result = new stdClass();
        $result->is_student = false;
        $result->advisor_submitted = false;
        $result->is_record = false;
        
        $student = $DB->get_record('local_mxschool_students', array('userid'=>$USER->id));
        if ($student){
            $result->is_student = true;
            
            $record = $DB->get_record('local_mxschool_advisors', array('studentid'=>$student->id));
            if ($record and $record->status > 0){
                $result->advisor_submitted = true;
            }
            if ($record){
                $result->is_record = true;
            }
        }
        
        return $result;
    }
    
    function print_select_advisor_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';
        
        $advisor_permissions = $this->get_advisor_permissions();
        $settings = $this->get_dates_settings();
        
        if ($settings['advisor_form_enable'] == 'second_semester_starts') {
            $date_open = $settings['second_semester_starts'];
        } elseif ($settings['advisor_form_enable'] == 'dorms_close') {
            $date_open = $settings['dorms_close']; 
        } else {
            $date_open = NULL;
        }
        
        $date_open_minus = strtotime($settings['advisor_form_time_prior'], $date_open);
        
        if ($advisor_permissions->is_student){
            if (time() < $date_open_minus || time() > $date_open || $advisor_permissions->advisor_submitted){
            } else {
                $url = new moodle_url('/local/mxschool/advisor_selection/select_advisor.php');
                $output .= html_writer::tag('li', html_writer::link($url, (($advisor_permissions->is_record) ? 'View and Edit Advisor Selection Form' : 'Select advisor')));
            }
        } elseif (has_capability('local/mxschool:advisor_selection_manage', context_system::instance())){
            $url = new moodle_url('/local/mxschool/advisor_selection/index.php');
            $output .= html_writer::tag('li', html_writer::link($url, 'Select advisor'));
        }
        
        return $output;
    }
    
    function get_dates_settings() {            
        $settings = array();
        $settings['second_semester_starts'] = get_config('local_mxschool', 'second_semester_starts');
        $settings['dorms_close'] = get_config('local_mxschool', 'dorms_close');
        $settings['advisor_form_enable'] = get_config('local_mxschool', 'advisor_form_enable');
        $settings['advisor_form_time_prior'] = get_config('local_mxschool', 'advisor_form_time_prior');
        $settings['thanksgiving_vacation_date'] = get_config('local_mxschool', 'thanksgiving_vacation_date');
        $settings['december_vacation_date'] = get_config('local_mxschool', 'december_vacation_date');
        $settings['march_vacation_date'] = get_config('local_mxschool', 'march_vacation_date');
        $settings['summer_vacation_date'] = get_config('local_mxschool', 'summer_vacation_date');
        $settings['vacation_form_enable'] = get_config('local_mxschool', 'vacation_form_enable');
        $settings['vacation_form_time_prior'] = get_config('local_mxschool', 'vacation_form_time_prior');
        $settings['rooming_form_time_prior'] = get_config('local_mxschool', 'rooming_form_time_prior');

        return $settings;
    }
    
    function get_vacation_permissions() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        
        $result = new stdClass();
        $result->is_student = false;
        $result->records = array();
        
        $student = $DB->get_record('local_mxschool_students', array('userid'=>$USER->id));
        if ($student){
            $result->is_student = true;
        }
        
        return $result;
    }
    
    function print_vacation_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';
        
        $vacation_permissions = $this->get_vacation_permissions();
        $settings = $this->get_dates_settings();
        
        if ($vacation_permissions->is_student){
            
            if ($settings['vacation_form_enable'] != '') {
                $date_open = $settings[$settings['vacation_form_enable']];
            } else {
                $date_open = NULL;
            }

            $date_open_minus = strtotime($settings['vacation_form_time_prior'], $date_open);
            
            if (time() < $date_open_minus || time() > $date_open){
            } else {
                $url = new moodle_url('/local/mxschool/vacationandtravel/records.php');
                $output .= html_writer::tag('li', html_writer::link($url, get_string('vacationandtravel_form', 'local_mxschool')));
            }
        } elseif (has_capability('local/mxschool:vacation_manage', context_system::instance())){
            $url = new moodle_url('/local/mxschool/vacationandtravel/index.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('vacationandtravel_form', 'local_mxschool')));
        }
        
        return $output;
    }

    function print_driving_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';

        if (has_capability('local/mxschool:driving_settings', context_system::instance())){
            $url = new moodle_url('/local/mxschool/driving/enter-driving.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('enter_driving_form', 'local_mxschool')));
        }

        return $output;
    }

    function get_rooming_permissions() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;

        $result = new stdClass();
        $result->is_student = false;
        $result->records = array();

        $student = $DB->get_record('local_mxschool_students', array('userid'=>$USER->id));
        if ($student){
            $result->is_student = true;
        }

        return $result;
    }

    function print_rooming_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';

        $vacation_permissions = $this->get_rooming_permissions();
        $settings = $this->get_dates_settings();

        if ($vacation_permissions->is_student && $settings['rooming_form_time_prior'] != ''){
            $date_open_minus = strtotime($settings['rooming_form_time_prior'], $settings['dorms_close']);

            if (time() > $date_open_minus){
                $url = new moodle_url('/local/mxschool/rooming/rooming-form.php');
                $output .= html_writer::tag('li', html_writer::link($url, get_string('rooming_form', 'local_mxschool')));
            }
        } elseif (has_capability('local/mxschool:rooming_manage', context_system::instance())){
            $url = new moodle_url('/local/mxschool/rooming/rooming-form.php');
            $output .= html_writer::tag('li', html_writer::link($url, get_string('rooming_form', 'local_mxschool')));
        }

        return $output;
    }

    function print_weekend_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';

        $url = new moodle_url('/local/mxschool/weekend/enter-weekend.php');
        $output .= html_writer::tag('li', html_writer::link($url, get_string('enter_weekend', 'local_mxschool')));

        return $output;
    }

    function print_esignout_link() {
        global $CFG, $USER, $OUTPUT, $DB, $PAGE;
        $output = '';

        $url = new moodle_url('/local/mxschool/esignout/enter-driving.php');
        $output .= html_writer::tag('li', html_writer::link($url, get_string('create_driving', 'local_mxschool')));

        $url = new moodle_url('/local/mxschool/esignout/enter-passenger.php');
        $output .= html_writer::tag('li', html_writer::link($url, get_string('create_passenger', 'local_mxschool')));

        return $output;
    }
    

}


